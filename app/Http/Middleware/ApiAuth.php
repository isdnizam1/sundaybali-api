<?php

namespace App\Http\Middleware;

use App\Libs\Firebase;
use Closure;
use Illuminate\Http\Request;
use App\Models\User;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();
        $user = User::with('driver','vendor', 'wallet_only')->where('token', $token)->where('account_status', 'active')->first();
        if (!$user) {
            $firebase = new Firebase();
            $auth = $firebase->verifyIdToken($token);
            if(!empty($auth->email)){
                $user = User::with('driver', 'wallet_only')->whereEmail($auth->email)->where('account_status', 'active')->first();
            }elseif(!empty($auth->phoneNumber)){
                $user = User::with('driver', 'wallet_only')->wherePhone($auth->phoneNumber)->where('account_status', 'active')->first();
            }
           
        }
        if ($user) {
            if($user->access=='vendor'){
                $user->rating = 3;
            }
            $request->merge(['auth' => $user]);
            return $next($request);
        }
        return response([
            'code' => 500,
            'status' => 'failed',
            'message' => 'Invalid Token'
        ], 200);
        // return $next($request);
    }
}
