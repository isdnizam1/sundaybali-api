<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\ExpoToken;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\UpgradeAccount;
use App\Models\Booking;
use App\Models\BankAccount;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Models\Vendor;
use App\Models\FirebaseToken;
use App\Models\Notification;
use App\Models\Document;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;

    public function index(Request $request)
    {
        $user = User::with('wallet')->with('bankAccount')->with('driver')->with('vendor')->orderByDesc('user_id');
        $access = $request->input('access');
        if ($request->input('user_id')) {
            $user->whereUser_id($request->input('user_id'));
        }
        if ($request->input('account_status')) {
            $user->whereAccount_status($request->input('account_status'));
        }
        if ($access) {
            $user->whereAccess($access);
        }
        $data = $user->get();
        if (!empty($data)) {
            if ($request->input('user_id')) {
            $data[0]->document = Document::whereUser_id($request->input('user_id'))->first();
            }

            return $this->success($data);
        } else {
            return $this->error('User is empty');
        }
    }

    public function driver(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $user = User::with('driver')->orderBy('user_id');
        $access = $request->input('access');
        if ($request->input('user_id')) {
            $user->whereUser_id($request->input('user_id'));
        }
        if ($request->input('account_status')) {
            $user->whereAccount_status($request->input('account_status'));
        }
        $user->whereAccess('driver');
        $user->whereHas('driver', function ($query) use ($request) {
            if ($request->status) {
                $query->whereStatus($request->status);
            }
        });
        $data = $user->get();
        $i = 0;
        foreach ($data as &$key) {
            if (!empty($key->driver)) {
                if ($request->available_status) {
                    $checkAvailable =   Booking::whereDriver_id($key->driver->driver_id)->whereIn('status', ['pending', 'waiting driver', 'waiting pickup', 'picked up'])->wherePickup_date(date('Y-m-d'))->first();
                    if ($checkAvailable) {
                        unset($data[$i]);
                    }
                }
                $key->driver->vehicle = Vehicle::find($key->driver->vehicle_id);
            }
            $i++;
        }
        $data = $data->values();
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Driver is not available');
        }
    }

    public function add_driver(Request $request)
    {
        if (!empty($request->created_by)) {
            if (!empty($request->user_id)) {
                $user = User::with('bankAccount')->with('driver')->find($request->user_id);
                $additional_data = json_decode($user->additional_data, true);
                $additional_data['updated_by'] = $request->created_by;
            } else {
                // Jika insert data
                $checkEmail = User::whereEmail($request->email)->first();
                if (!empty($checkEmail)) {
                    return $this->error('Email is already exist');
                }
                $checkPhone = User::wherePhone($request->phone)->first();
                if (!empty($checkPhone)) {
                    return $this->error('Phone is already exist');
                }
                $additional_data['created_by'] = $request->created_by;
                $input['token'] = sha1(uniqid());
                $input['access'] = 'driver';
                $input['registered_via'] = 'email';
                $input['phone_status'] = 'unverified';
                $input['account_status'] = 'active';
            }
            if($request->password){
                $input['password'] = Hash::make($request->password);
            }
            $input['first_name'] = $request->first_name;
            $input['last_name'] = $request->last_name;
            $input['prefered_language'] = $request->language;
            $input['email'] = $request->email;
            $input['phone'] = $request->phone;
            $input['additional_data'] = json_encode($additional_data);
            if (!empty($user)) {
                User::find($user->user_id)->update($input);
                $user_id = $request->user_id;
            } else {
                $user_id = User::create($input)->user_id;
            }

            $driver['vehicle_id'] = $request->vehicle_id;
            $driver['plate_number'] = $request->plate_number;
            $driver['user_id'] = $user_id;
            if (!empty($user->driver)) {
                Driver::find($user->driver->driver_id)->update($driver);
            } else {
                Driver::create($driver);
            }

            if(!empty($request->bank_id)){
                $bank['bank_id'] = $request->bank_id;
                $bank['account_name'] = $request->account_name;
                $bank['account_number'] = $request->account_number;
                $bank['user_id'] = $user_id;
                if (!empty($user->bankAccount)) {
                    BankAccount::find($user->bankAccount->bank_account_id)->update($bank);
                } else {
                    BankAccount::create($bank);
                }
            }

            return $this->success($input);
        } else {
            return $this->error('Created by is not found');
        }
    }

    public function set_status_user(Request $request)
    {
        $checkUser = User::find($request->user_id);
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            $input['account_status'] = $request->status;
            if (!empty($request->banned_reason)) {
                $additional_data['banned_reason'] = $request->banned_reason;
                $input['additional_data'] = json_encode($additional_data);
            }
            $update = User::find($checkUser->user_id)->update($input);
            return $this->success($input);
        } else {
            return $this->error('User is not found');
        }
    }


    public function detail(Request $request)
    {
        if (!empty($request->auth)) {
            $dataUser = $request->auth;
        }
        
        if (!empty($dataUser)) {
            $dataUser->document = Document::whereUser_id($request->auth->user_id)->first();
            return $this->success($dataUser);
        } else {
            return $this->error('User is not found');
        }
    }


    public function update_profile(Request $request)
    {
        $checkUser = $request->auth;;
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            $input['first_name'] = $request->first_name;
            $input['last_name'] = $request->last_name;
            $input['phone'] = $request->phone;
            $checkPhone = User::wherePhone($request->phone)->first();
            if($checkPhone){
                if($checkPhone->user_id!=$checkUser->user_id){
                    return $this->error('Phone is already exist');
                }
            }
            if ($request->avatar) $input['profile_picture'] = $request->avatar;
            if ($request->about) $input['about_me'] = $request->about;
            $input['additional_data'] = json_encode($additional_data);
            $update = User::find($checkUser->user_id)->update($input);
            if($checkUser->access=='vendor'){
                if(!empty($request->vendor_name)){
                    $inputVendor['vendor_name'] = $request->vendor_name;
                }
                $inputVendor['is_auto_confirm'] = $request->is_auto_confirm;
                $update = Vendor::whereUser_id($checkUser->user_id)->update($inputVendor);
            }
            // if ($checkUser->phone != $request->phone) {
            //     $this->sendMessage('Your verification code  is : ' . $additional_data['phone_verification_code'], $input['phone']);
            // }
            $dataUser = User::whereUser_id($checkUser->user_id)->first();

            return $this->success($dataUser);
        } else {
            return $this->error('User is not found');
        }
    }

    public function save_expo_token(Request $request)
    {
        $checkUser = User::whereToken($request->token)->first();
        $code = 500;
        if (!empty($checkUser)) {
            $input['expo_token'] = $request->expo_token;
            $input['user_id'] = $checkUser->user_id;
            $exists = ExpoToken::where($request->only('expo_token'))->first();
            if ($exists) {
                $exists->update($input);
                $exists->touch();
            } else ExpoToken::create($input);
            return $this->success($input);
        } else {
            return $this->error('User is not found');
        }
    }

    public function upgrade_account_level(Request $request)
    {
        $code = 500;
        $checkUser = User::whereToken($request->token)->first();
        if (!empty($checkUser)) {
            if (!empty($request->electric_bill)) {
                $file_name = 'ElectricBill_' . uniqid() . '_' . time();
                $file_name = $this->uploadImage($request->electric_bill, '/public/assets/images/document/', $file_name);
                $additional_data['electric_bill'] = '/assets/images/document/' . $file_name;
            } else {
                return $this->error('Electric Bill is Empty');
            }

            if (!empty($request->salary_receipt)) {
                $file_name = 'SalaryReceipt_' . uniqid() . '_' . time();
                $file_name = $this->uploadImage($request->salary_receipt, '/public/assets/images/document/', $file_name);
                $additional_data['salary_receipt'] = '/assets/images/document/' . $file_name;
            } else {
                return $this->error('Salary Receipt is Empty');
            }
            if (!empty($request->other_receipt)) {
                $file_name = 'OtherReceipt_' . uniqid() . '_' . time();
                $file_name = $this->uploadImage($request->other_receipt, '/public/assets/images/document/', $file_name);
                $additional_data['other_receipt'] = '/assets/images/document/' . $file_name;
            }
            $input['user_id'] = $checkUser->user_id;
            $input['account_level'] = 'gold';
            $input['status'] = 'pending';
            $input['additional_data'] = json_encode($additional_data);
            UpgradeAccount::create($input);
            $code = 200;
        } else {
            $message = 'User is not found';
        }
        if ($code == 200) {
            return $this->success($input);
        } else {
            return $this->error($message);
        }
    }

    public function delete_account(Request $request)
    {
        $code = 500;
        $where['user_id'] = $request->user_id;
        $checkUser = User::where($where)->first();
        $firebase_token = FirebaseToken::whereUser_id($request->user_id)->delete();
        $notification = Notification::whereUser_id($request->user_id)->delete();

        if (!empty($checkUser)) {
            $checkUser->delete();
            $code = 200;
        } else {
            $message = 'User is not found';
        }
        if ($code == 200) {
            return $this->success($checkUser);
        } else {
            return $this->error($message);
        }
    }
}
