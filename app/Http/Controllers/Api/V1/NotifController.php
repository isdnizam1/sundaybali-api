<?php

namespace App\Http\Controllers\Api\V1;


use App\Models\Notif;
use App\Models\Booking;
use App\Traits\GlobalFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Notification;
use App\Models\BookingDriver;
use Illuminate\Support\Facades\Artisan;


class NotifController extends Controller
{
    use GlobalFunction;

  
    public function index(Request $request){
        $limit=7;
        $offset=0;
        if(!empty($_GET['limit'])){
            $limit= $_GET['limit'];
        }
        if(!empty($_GET['offset'])){
            $offset= $_GET['offset'];
        }
        $data =Notif::orderByDesc('created_at');
        if(!empty($request->user_id)){
            $data->whereUser_id($request->user_id);
        }
        $response['list'] =$data->offset($offset)->limit($limit)->get();

        $dataUnread =Notif::whereStatus('sent');
        if(!empty($request->user_id)){
            $dataUnread->whereUser_id($request->user_id);
        }
        $response['unread'] =$dataUnread->count();

        if(!empty($response)){ 
            return $this->success($response);
        }else{
            return $this->error('Notif is empty');
        }
    }


    public function set_status_notif(Request $request)
    {
        $checkData = Notif::find($request->id);
        if (!empty($checkData)) {
            $input['status'] = $request->status;
            $update = Notif::find($checkData->id)->update($input);
            return $this->success($checkData);
        } else {
            return $this->error('User is not found');
        }
    }


    public function send_notif(Request $request)
    {
        Notification::create([
            'user_id' => $request->user_id,
            'title' => $request->title,
            'message' => $request->message,
            'payload' => json_encode(["booking_id" => $request->booking_id]),
        ]);
        Artisan::call('push:sender');
        return $this->success($request->user_id);
    }


}
