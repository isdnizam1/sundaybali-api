<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;

class DriverController extends Controller
{

    use GlobalFunction;

    public function switch_status(Request $request)
    {
        $driver = Driver::whereUserId($request->auth->user_id)->firstOrFail();
        $driver->update($request->only('status'));
        return $this->success($driver);
    }

    public function last_location(Request $request)
    {
        $data = Driver::with('last_location')->with('user')->get();
        $i=0;
        foreach($data as $key){
            if($key->last_location==null){
                unset($data[$i]);
            }
            $i++;
        }
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Driver is empty');
        }
    }

    public function location(Request $request)
    {
        $data = Driver::with('last_location')->with('user')->get();
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Driver is empty');
        }
    }
}
