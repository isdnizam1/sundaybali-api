<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\PositionLog;
use Illuminate\Http\Request;

class PositionController extends Controller
{

    public function push_log(Request $request)
    {
        $data = $request->only([
            "latitude",
            "longitude",
            "street_name",
            "country",
            "province",
            "district",
            "subdistrict",
            "village",
        ]);
        $data['driver_id'] = $request->auth->driver->driver_id;
        $log = PositionLog::create($data);
        return response()->json(compact('log'));
    }
}
