<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\Attraction;




class AttractionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;


    public function index()
    {
        if (!empty($_GET['attraction_id'])) {
            $attraction_id = $_GET['attraction_id'];
            $data = Attraction::find($attraction_id);
        } else {
            $limit = 5;
            $offset = 0;
            if (!empty($_GET['limit'])) {
                $limit = $_GET['limit'];
            }
            if (!empty($_GET['offset'])) {
                $offset = $_GET['offset'];
            }
            $data = Attraction::offset($offset)->limit($limit)->get()->toArray();
            foreach ($data as $index => $val) {
                $data[$index]['additional'] = json_decode($val['additional_data']);
                unset($data[$index]['additional_data']);
                unset($data[$index]['overview']);
            }
        }
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Attraction is not found');
        }
    }

    public function submit(Request $request)
    {
        $user_id = $request->auth->user_id;
        $booking_id = 'SB-' . $user_id . '' . strtotime(date('Y-m-d H:i:s'));
        $additional_data['date'] = $request->date;
        $additional_data['pickup_time'] = $request->pickup_time;
        $additional_data['pickup_location'] = $request->location;
        $additional_data['note'] = $request->note;
        $additional_data['pax'] = $request->pax;
        $additional_data['duration'] = $request->duration;
        $input['status'] = 'pending';
        $input['user_id'] = $user_id;
        $input['booking_id'] = $booking_id;
        $input['additional_data'] = json_encode($additional_data);
        Booking::create($input);
        if (!empty($additional_data)) {
            return $this->success($additional_data);
        } else {
            return $this->error('User is not found');
        }
    }

    public function store(Request $request)
    {
        $user_id = $request->auth->user_id;
        $form['attraction_name'] = $request->attraction_name;
        $form['overview'] = $request->overview;
        $form['address'] = $request->address;
        $form['latitude'] = $request->latitude;
        $form['longitude'] = $request->longitude;
        $form['user_id'] = $user_id;
        $image = $request->file('image');
        if (!empty($image)) {
            $path_upload = 'assets/images/attraction';
            $form['image'] = '/' . $path_upload . '/' . $image->getClientOriginalName();
            $image->move($path_upload, $image->getClientOriginalName());
        }
        $additional_data['inclussion'] = $request->inclussion;
        $additional_data['entrance_fee'][0]['type'] = 'child';
        $additional_data['entrance_fee'][0]['price'] = $request->entrance_fee_child;
        $additional_data['entrance_fee'][1]['type'] = 'adult';
        $additional_data['entrance_fee'][1]['price'] = $request->entrance_fee_adult;
        if (empty($request->attraction_id)) {
            Attraction::create($form);
        } else {
            Attraction::find($request->attraction_id)->update($form);
        }
        return $this->success($form, 'Suceess Updated Attraction');
    }
}
