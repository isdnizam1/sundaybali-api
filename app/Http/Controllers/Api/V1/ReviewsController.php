<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\User;
use App\Models\Reviews;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;


    public function index(Request $request){
        $limit=100;
        $offset=0;
        if(!empty($_GET['limit'])){
            $limit= $_GET['limit'];
        }
        if(!empty($_GET['offset'])){
            $offset= $_GET['offset'];
        }
        $data =Reviews::orderBy('review_id')->offset($offset)->limit($limit);
        if($request->booking_id){
            $data->whereBooking_id($request->booking_id);
        }
        $response = $data->get();
        if(!empty($response)){ 
            return $this->success($response);
        }else{
            return $this->error('Review is not found');
        }
    }

    public function addReview(Request $request){
        $user_id =$request->auth->user_id;
        $booking = Booking::find($request->booking_id);
        if($booking){
            $form['star']=$request->star;
            if($request->services){
                $form['services']=json_encode($request->services);
            }
            $form['comment']=$request->comment;
            $form['booking_id']=$request->booking_id;
            $form['user_id']=$user_id;
            Reviews::create($form);
            return $this->success($form);
        }else{
            return $this->error('Booking ID is not found');
        }
    }
}
