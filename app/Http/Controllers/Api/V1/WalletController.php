<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Mutation;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;

class WalletController extends Controller
{
    use GlobalFunction;
    
    public function mutations(Request $request)
    {
        $auth = $request->auth;
        $mutations = Mutation::whereWalletId($auth->wallet_only->wallet_id)->orderBy('created_at', 'desc')->get();
        return $this->success($mutations);
    }
}
