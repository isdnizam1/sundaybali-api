<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\User;
use App\Models\Report;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;


    public function index(Request $request){
        $limit=100;
        $offset=0;
        if(!empty($_GET['limit'])){
            $limit= $_GET['limit'];
        }
        if(!empty($_GET['offset'])){
            $offset= $_GET['offset'];
        }
        $data =Report::with('booking')->orderByDesc('report_id')->offset($offset)->limit($limit);
        if($request->booking_id){
            $data->whereBooking_id($request->booking_id);
        }
        $response = $data->get();
        if(!empty($response)){ 
            return $this->success($response);
        }else{
            return $this->error('Report is not found');
        }
    }

    public function addReport(Request $request){
        $user_id =$request->auth->user_id;
        $booking = Booking::whereStatus('completed')->find($request->booking_id);
        if($booking){
            $dataReport =Report::whereBooking_id($booking->booking_id)->first();
            if($dataReport){
                return $this->error('Kamu telah memberi report untuk bookingan ini');
            }else{
                $form['comment']=$request->comment;
                $form['booking_id']=$request->booking_id;
                Report::create($form);
                return $this->success($form);
            }
        }else{
            return $this->error('Booking  is not found');
        }
    }
}
