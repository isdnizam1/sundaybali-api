<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\VehicleType;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;


    public function index(){
        if(!empty($_GET['vehicle_id'])){
            $vehicle_id = $_GET['vehicle_id'];
            $data = Vehicle::find($vehicle_id);
        }else{
            $limit=100;
            $offset=0;
            if(!empty($_GET['limit'])){
                $limit= $_GET['limit'];
            }
            if(!empty($_GET['offset'])){
                $offset= $_GET['offset'];
            }
            $data =Vehicle::with('vehicleType')->orderBy('name')->offset($offset)->limit($limit)->get();
        }
        if(!empty($data)){ 
            return $this->success($data);
        }else{
            return $this->error('Vehicle is not found');
        }
    }


    public function type(Request $request){
        if(!empty($request->vehicle_type_id)){
            $vehicle_type_id = $request->vehicle_type_id;
            $data = VehicleType::find($request->vehicle_type_id);
        }else{
            $limit=5;
            $offset=0;
            if(!empty($_GET['limit'])){
                $limit= $_GET['limit'];
            }
            if(!empty($_GET['offset'])){
                $offset= $_GET['offset'];
            }
            $data =VehicleType::offset($offset)->limit($limit)->get();
        }
        if(!empty($data)){ 
            return $this->success($data);
        }else{
            return $this->error('Vehicle is not found');
        }
    }


    public function add_vehicle(Request $request){
        $user_id =$request->auth->user_id;
        $form['vehicle_type_id']=$request->vehicle_type_id;
        $form['name']=$request->name;
        $form['seats']=$request->seats;
        if(empty($request->vehicle_id)){
            $form['price']=0;
            Vehicle::create($form);
        }else{
            Vehicle::find($request->vehicle_id)->update($form);
        }
        return $this->success($form);
    }
}
