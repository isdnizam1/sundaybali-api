<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\User;
use App\Models\Withdrawal;
use App\Models\BankAccount;
use App\Models\Wallet;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;


    public function index(Request $request)
    {
        $limit = 100;
        $offset = 0;
        if (!empty($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        }
        if(!empty($request->user_id)){
            $wallet = Wallet::whereUser_id($request->user_id)->first();
        }
        $data = Withdrawal::with('wallet')->orderByDesc('withdrawal_id')->offset($offset)->limit($limit);
        if(!empty($wallet->wallet_id)){
            $data->whereWallet_id($wallet->wallet_id);
        }
        $data = $data->get();
        foreach ($data as &$key) {
            $key->bank_account = BankAccount::whereUser_id($key->wallet->user_id)->with('bank')->first();
        }
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Withdrawal is not found');
        }
    }

    public function set_status_withdraw(Request $request)
    {
        $checkData = Withdrawal::find($request->withdrawal_id);
        if (!empty($checkData)) {
            $additional_data = json_decode($checkData->additional_data, true);
            $input['status'] = $request->status;
            if (!empty($request->note)) {
                $additional_data['note'] = $request->note;
            }
            $input['additional_data'] = json_encode($additional_data);
            $update = Withdrawal::find($checkData->withdrawal_id)->update($input);
            return $this->success($input);
        } else {
            return $this->error('User is not found');
        }
    }

    public function inquiry(Request $request)
    {
        $withdrawal = DB::transaction(function () use ($request) {
            if ($request->auth->wallet_only->balance_withdraw >= $request->amount){
                if($request->amount > 500000){
                    return ["formatted_status" => "Limit withdrawal is 500.000"];
                }else{
                    return Withdrawal::create([
                        'amount' => $request->amount,
                        'wallet_id' => $request->auth->wallet_only->wallet_id,
                        'status' => 'pending',
                        'additional_data' => '{}'
                    ]);
                }
            }else{ return ["formatted_status" => "Insuficient Balance"]; }
        });
        return response()->json(compact('withdrawal'));
    }

    public function personal_withdrawal_history(Request $request)
    {
        return $this->success(
            Withdrawal::whereWalletId($request->auth->wallet_only->wallet_id)->orderBy('created_at', 'desc')->get()
        );
    }
}
