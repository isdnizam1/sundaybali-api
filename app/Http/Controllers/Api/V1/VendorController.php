<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\ExpoToken;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\User;
use App\Models\Car;
use App\Models\Vehicle;
use App\Models\Vendor;
use App\Models\UpgradeAccount;
use App\Models\Booking;
use App\Models\BankAccount;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Mail\VerifyEmail;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;

    public function index(Request $request)
    {
        $user = Vendor::with('user')->orderByDesc('created_at');
        if ($request->input('vendor_id')) {
            $user->whereVendor_id($request->input('vendor_id'));
        }
        if ($request->input('status')) {
            $user->whereStatus($request->input('status'));
        }
        $data = $user->get();
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Vendor is empty');
        }
    }

    public function add_vendor(Request $request)
    {  
        if (!empty($request->created_by)) {
            if (!empty($request->user_id)) {
                $user = User::with('bankAccount')->with('vendor')->find($request->user_id);
                $additional_data = json_decode($user->additional_data, true);
                $additional_data['updated_by'] = $request->created_by;
            } else {
                // Jika insert data
                $checkEmail = User::whereEmail($request->email)->first();
                if (!empty($checkEmail)) {
                    return $this->error('Email is already exist');
                }
                $checkPhone = User::wherePhone($request->phone)->first();
                if (!empty($checkPhone)) {
                    return $this->error('Phone is already exist');
                }
                $additional_data['created_by'] = $request->created_by;
                $input['token'] = sha1(uniqid());
                $input['access'] = 'vendor';
                $input['registered_via'] = 'email';
                $input['phone_status'] = 'unverified';
                $input['account_status'] = 'active';
            }
            $input['first_name'] = $request->first_name;
            $input['last_name'] = $request->last_name;
            $input['email'] = $request->email;
            $input['phone'] = $request->phone;
            $input['additional_data'] = json_encode($additional_data);
            if (!empty($user)) {
                User::find($user->user_id)->update($input);
                $user_id = $request->user_id;
            } else {
                $input['password'] = Hash::make($request->password);
                $user_id = User::create($input)->user_id;
            }

            $vendor['vendor_name'] = $request->vendor_name;
            $vendor['service_fee'] = $request->service_fee;
            $vendor['service_fee_type'] = $request->service_fee_type;
            $vendor['user_id'] = $user_id;
            
            if (!empty($user->vendor)) {
                Vendor::find($user->vendor->vendor_id)->update($vendor);
            } else {
                // Generate token for verification
                $additional_data_vendor['token_verify'] = Str::random(40);
                $vendor['additional_data'] = json_encode($additional_data_vendor);
                $vendor['status'] = 'Non Active';

                $vendor = Vendor::create($vendor);
                
                // Send email verification
                $res['name'] = $vendor->vendor_name;
                $res['url'] = URL::to('/') . "/vendor/verify?vendor_id=" . $vendor->vendor_id . "&token=" . $additional_data_vendor['token_verify'];
                Mail::to($request->email)->send(new VerifyEmail($res));
            }

            if(!empty($request->bank_id)){
                $bank['bank_id'] = $request->bank_id;
                $bank['account_name'] = $request->account_name;
                $bank['account_number'] = $request->account_number;
                $bank['user_id'] = $user_id;
                if (!empty($user->bankAccount)) {
                    BankAccount::find($user->bankAccount->bank_account_id)->update($bank);
                } else {
                    BankAccount::create($bank);
                }
            }

            return $this->success($input);
        } else {
            return $this->error('Created by is not found');
        }
    }

    public function set_status_user(Request $request)
    {
        $checkUser = User::find($request->user_id);
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            $input['account_status'] = $request->status;
            if (!empty($request->banned_reason)) {
                $additional_data['banned_reason'] = $request->banned_reason;
                $input['additional_data'] = json_encode($additional_data);
            }
            $update = User::find($checkUser->user_id)->update($input);
            return $this->success($input);
        } else {
            return $this->error('User is not found');
        }
    }


    public function update_location(Request $request)
    {
        $checkVendor = Vendor::find($request->vendor_id);
        if (!empty($checkVendor)) {
            // $input['latitude'] = $request->latitude;
            // $input['longitude'] = $request->longitude;
            // $input['location_name'] = $request->location_name;
            $additional_data = json_decode($checkVendor->additional_data, true);
            $additional_data['latitude'] = $request->latitude;
            $additional_data['longitude'] = $request->longitude;
            $additional_data['location_name'] = $request->location_name;
            $input['additional_data'] = json_encode($additional_data);

            $update = Vendor::find($checkVendor->vendor_id)->update($input);
            // $input['vendor_id'] = $request->vendor_id;
            $additional_data['vendor_id'] = $request->vendor_id;

            $inputCar['latitude'] = $request->latitude;
            $inputCar['longitude'] = $request->longitude;
            Car::whereVendor_id($checkVendor->vendor_id)->update($inputCar);
            return $this->success($additional_data);
        } else {
            return $this->error('Vendor is not found');
        }
    }


    public function update_profile(Request $request)
    {
        $checkUser = $request->auth;;
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            $input['first_name'] = $request->first_name;
            $input['last_name'] = $request->last_name;
            if($request->phone){
                $input['phone'] = $request->phone;
            }
            if($request->email){
                $input['email'] = $request->email;
            }
            if($request->profile_picture){
            $file_name = 'profile_picture-'. time();
            $file_name = $this->uploadImage($request->profile_picture, 'public/images/', $file_name);
            $input['profile_picture'] = URL::to('images/' . $file_name . '');
            }

            $input['additional_data'] = json_encode($additional_data);
            $update = User::find($checkUser->user_id)->update($input);
            $inputVendor['vendor_name'] = $request->vendor_name;
            $update = Vendor::whereUser_id($checkUser->user_id)->update($inputVendor);
            // if ($checkUser->phone != $request->phone) {
            //     $this->sendMessage('Your verification code  is : ' . $additional_data['phone_verification_code'], $input['phone']);
            // }
            return $this->success($input);
        } else {
            return $this->error('User is not found');
        }
    }


    public function delete_account(Request $request)
    {
        $code = 500;
        $where['user_id'] = $request->user_id;
        $where['phone_status'] = 'unverified';
        $where['access'] = 'member';
        $checkUser = User::where($where)->first();
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data);
            if (!empty($additional_data->id_card_selfie)) {
                File::delete(public_path() . '' . $additional_data->id_card_selfie);
            }
            if (!empty($additional_data->id_card)) {
                File::delete(public_path() . '' . $additional_data->id_card);
            }
            $checkUser->delete();
            $code = 200;
        } else {
            $message = 'User is not found';
        }
        if ($code == 200) {
            return $this->success($checkUser);
        } else {
            return $this->error($message);
        }
    }


    public function update_free_delivery(Request $request)
    {
        $checkVendor = Vendor::find($request->vendor_id);
        if (!empty($checkVendor)) {
            $additional_data = json_decode($checkVendor->additional_data);
            $additional_data['free_delivery'] = $request->free_delivery;
            $input['additional_data'] = json_encode($additional_data);
            $update = Vendor::find($checkVendor->vendor_id)->update($input);
            $input['vendor_id'] = $request->vendor_id;
            return $this->success($input);
        } else {
            return $this->error('Vendor is not found');
        }
    }

    public function verify_vendor(Request $request) {
        $vendor = Vendor::where('vendor_id', $request->vendor_id)->first();
        $message = 'Sorry your email cannot be identified.';

        if ($vendor) {
            $additional_data = json_decode($vendor->additional_data, true);
            if ($additional_data['token_verify'] == $request->token) {
                $vendor->status = 'Active';
                $vendor->save();
                $message = "Your vendor is verified. You can now login.";
            }
        }
        
        return view('message', compact('message'));
    }

}
