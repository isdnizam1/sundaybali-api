<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\User;
use App\Models\Car;
use App\Models\CarImage;
use App\Models\Vendor;
use App\Models\VehicleType;
use Illuminate\Support\Facades\Auth;
use Validator;
use DateTime;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Reviews;
use App\Libs\Distance;
use Carbon\CarbonPeriod;
use Carbon\Carbon;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;


    public function index(Request $request){
        $limit=100;
        $offset=0;
        if(!empty($_GET['limit'])){
            $limit= $_GET['limit'];
        }
        if(!empty($_GET['offset'])){
            $offset= $_GET['offset'];
        }
        $data =Car::with('vehicle')->orderByDesc('car_id')->offset($offset)->limit($limit);
        if(!empty($request->vendor_id)){
            $data->whereVendor_id($request->vendor_id);
        }
        if(!empty($request->vehicle_id)){
            $data->whereVehicle_id($request->vehicle_id);
        }
        if(!empty($request->status)){
            $data->whereStatus($request->status);
        }
        $data = $data->get();
        foreach($data as &$key){
            // Masih hardcode
            $key->rating = 3;
            $key->total_reviews =1;
        }
        if(!empty($data)){ 
            return $this->success($data);
        }else{
            return $this->error('Car is not found');
        }
    }

    public function detail(Request $request){
        if($request->car_id){
            $data = Car::with('vehicle')->with('vendor')->whereCar_id($request->car_id)->first();
            $data->rating = 3;
            $data->reviews = Reviews::orderBy('review_id')->with('user:user_id,first_name,last_name,profile_picture')->get();
            $data->image_gallery = CarImage::whereCar_id($request->car_id)->get();
            $data->total_reviews =count($data->reviews);

            // Get existed date
            $dateList = [];
            $bookings = Booking::whereCarId($request->car_id)->get();
            foreach ($bookings as $booking) {
                // GET range datetime
                $pickup = $booking->arr_data['additional_data']['pickup_location']['date'];
                $return = $booking->arr_data['additional_data']['return_location']['date'];
                $pickupTime = $booking->arr_data['additional_data']['pickup_location']['time'];
                $returnTime = $booking->arr_data['additional_data']['return_location']['time'];

                // Eliminate past return booking
                if (Carbon::parse($return . " " . $returnTime)->isPast()) continue;

                $periods = CarbonPeriod::create($pickup, $return)->toArray();
                foreach($periods as $period) {
                    if ($period->format('Y-m-d') == $return){
                        $pushed = Carbon::parse($return . " " . $returnTime)->addHour(1)->format('Y-m-d H:i:s');
                        // If after adding hour change date
                        if(substr($pushed,0,10) != $return) {
                            array_push($dateList, $return . " 23:59:00");
                        }
                    } else {
                        $pushed = $period->format('Y-m-d') . " 23:59:00";
                    }
                    array_push($dateList, $pushed);
                }
                
                // Push and parsed date to array
                // array_push($dateList, $pickup . " " . $pickupTime);
                // if (count($periods) > 2) {
                //     for ($i = 1; $i < count($periods) - 1; $i++) {
                //         array_push($dateList, $periods[$i]->format('Y-m-d') . " 23:59:00");
                //     }
                // }
                // // Add 3 hours from return;
                // $returnPlus3Hours = Carbon::parse($return . " " . $returnTime)->addHour(3)->format('Y-m-d H:i:s');
                // array_push($dateList, $returnPlus3Hours);
            }
            
            // Filter unique and sort array
            $existedDates = array_values(array_unique($dateList));
            sort($existedDates);

            // Unset data with same date but time is lower
            $existedDatesLength = count($existedDates) - 1;
            $i = 0;
            while($i < $existedDatesLength) {
                if (substr($existedDates[$i],0,10) == substr($existedDates[$i+1],0,10)) {
                    if ($existedDates[$i] > $existedDates[$i+1]) {
                        array_splice($existedDates, $i+1, 1);
                        $existedDatesLength--;
                    } else {
                        array_splice($existedDates, $i, 1);
                        $existedDatesLength--;
                    }
                } else {
                    $i++;
                }
            }
            $data->existed_dates = $existedDates;
        }
        if(!empty($data)){ 
            return $this->success($data);
        }else{
            return $this->error('Car is not found');
        }
    }
    
    public function add_car(Request $request){
        $user_id =$request->auth->user_id;
        $vendor = Vendor::whereUser_id($user_id)->first();
        $form['vehicle_id']=$request->vehicle_id;
        $form['vendor_id']=$vendor->vendor_id;
        $form['transimission']=$request->transimission;
        $form['plate_number']=$request->plate_number;
        $form['rental_price']=$request->rental_price;
        $form['year']=$request->year;
        $form['seats']=$request->seats;
        $form['fuel']=$request->fuel;
        $form['class']=$request->class;
        $form['description']=$request->description;
        $form['status']='Active';

        // Get Vendor Location
        $additional_data = json_decode($vendor->additional_data, true);
        if (array_key_exists('latitude', $additional_data)) 
            $form['latitude'] = $additional_data['latitude'] ;
        if (array_key_exists('longitude', $additional_data)) 
            $form['longitude'] = $additional_data['longitude'] ;
        
        $image = $request->image;
     
        if(empty($request->car_id)){
            $car_id = Car::create($form)->car_id;
            $form['car_id']= $car_id;
        }else{
            $car_id = $request->car_id;
            Car::find($car_id)->update($form);
            $form['car_id']= $car_id;
        }

        if ($image) {
            $i=0;
            if(!is_array($image)){
                $image[0] = $image;
            }
            foreach($image as $key=>$val){
                if($image[$i]){
                    $file_name = 'car-'.$i.'-'. time();
                    $file_name = $this->uploadImage($image[$i], 'public/images/cars/', $file_name);
                    $formImage['image'] = URL::to('images/cars/' . $file_name . '');
                    $formImage['car_id'] = $car_id;
                    CarImage::create($formImage);
                }
                $i++;
            }
            if(!empty($formImage['image'])){
                $img['image'] = $formImage['image'];
                Car::find($car_id)->update($img);
            }

        }
        return $this->success($form);
    }


    public function add_image(Request $request){
        $user_id =$request->auth->user_id;
        $dataCar = Car::find($request->car_id);
        if($dataCar){
            if($request->image){
                $file_name = 'car-'. time();
                $file_name = $this->uploadImage($request->image, 'public/images/cars/', $file_name);
                $formImage['image'] = URL::to('images/cars/' . $file_name . '');
                $formImage['car_id'] = $request->car_id;
                CarImage::create($formImage);

                if(empty($dataCar->image)){
                    $img['image'] = $formImage['image'];
                    Car::find($car_id)->update($img);
                }
            }
            return $this->success($formImage);
        }else{
            return $this->error('Car is not found');
        }
    }


    public function remove_image(Request $request){
        $user_id =$request->auth->user_id;
        $dataCar = CarImage::find($request->car_image_id);
        if($dataCar){
            $delete = $dataCar->delete();

                $filename =str_replace(['https://allsunday.dev21.my.id','https://featour.com'],'',$dataCar->image);
                $image_path =  public_path($filename);

                if (file_exists($image_path)) {
                    @unlink($image_path);
                }
                $delete = $dataCar->delete();
                $car = Car::find($dataCar->car_id);
                if($dataCar->image==$car->image){
                    $form['image'] ='';
                    $car->update($form);
                }

                return $this->success($dataCar);
        }else{
            return $this->error('Image is not found');
        }
    }


    public function search(Request $request){
        $pickup_date = $request->pickup_date;
        $return_date = $request->drop_off_date;
        $pickup_coordinate = $request->pickup_coordinate;
        $return_coordinate = $request->drop_off_coordinate;
        
        // Check Existed Booking
        $existedBooking = Booking::whereNotIn('status', ['cancel'])->get();
        $i=0;
        foreach($existedBooking as &$key){
            // Clean noise data (date not found)
            if (!array_key_exists('date', $key->arr_data['additional_data']['pickup_location'])) {
                unset($existedBooking[$i]); 
                $i++;
                continue;
            }

            // Clean not intersection date
            $data_pickup_date = $key->arr_data['additional_data']['pickup_location']['date'] . " " .$key->arr_data['additional_data']['pickup_location']['time'];
            $data_return_date = $key->arr_data['additional_data']['return_location']['date'] . " " .$key->arr_data['additional_data']['return_location']['time'];
            if (($data_pickup_date < $pickup_date and $data_return_date < $pickup_date) or ($data_pickup_date > $return_date and $data_return_date > $return_date)) {
                
                // Car will available after 3 hours from previous return
                $previous_return = new DateTime($data_return_date);
                $new_pickup = new DateTime($pickup_date);
                $interval = $previous_return->diff($new_pickup);
                $hours = $interval->h;
                $hours = $hours + ($interval->days*24);

                if ($hours >= 3) unset($existedBooking[$i]); //TODO: check if booking status log last completed > 3
            }
            $i++;
        }
        $notAvailableCarId = $existedBooking->pluck('car_id')->unique();

        $limit=100;
        $offset=0;
        if(!empty($_GET['limit'])){
            $limit= $_GET['limit'];
        }
        if(!empty($_GET['offset'])){
            $offset= $_GET['offset'];
        }
        $data = Car::select('car_id','vehicle_id','image','rental_price','latitude','longitude','location_name','seats','fuel','class')
            ->with('vehicle:vehicle_id,vehicle_type_id,name')
            ->whereStatus('Active')
            ->whereNotIn('car_id', $notAvailableCarId)
            ->orderByDesc('car_id')
            ->offset($offset)
            ->limit($limit);
        // if(!empty($vendor->vendor_id)){
        //     $data->whereVendor_id($vendor->vendor_id);
        // }
        $data = $data->get();
        $i=0;
        foreach($data as &$key){
            // Masih hardcode
            $key->rating = 3;
            $key->total_reviews =1;

            // Calculate Distance
            $distance = new Distance();
            $carLocation = $key->latitude.','.$key->longitude;
            $distance_object = $distance->getDistance($pickup_coordinate, $carLocation);
            if (array_key_exists('distance', $distance_object)) {
                $distance_value = $distance_object['distance']['value'];
                $key->distance = $distance_value;
            } else {
                $key->distance = PHP_INT_MAX;
            }

            if(!empty($request->vehicle_type_id)){
                if($request->vehicle_type_id!=$key->vehicle->vehicle_type_id){
                    unset($data[$i]);
                }
            }
            $i++;
        }

        // Sort By Distance
        $data = $data->sortBy('distance')->values();

        if(!empty($data)){ 
            return $this->success($data);
        }else{
            return $this->error('Car is not found');
        }
    }



    public function set_status(Request $request){
        $car = Car::find($request->car_id);
        if($car){
            $form['car_id']=$request->car_id;
            $form['status']=$request->status;
            Car::find($request->car_id)->update($form);
        }
        return $this->success($form);
    }
}