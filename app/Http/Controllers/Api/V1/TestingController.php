<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestEmail;

class TestingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use GlobalFunction;
    public function send_email(Request $request) {
        Mail::to($request->email)->send(new TestEmail(['content' => $request->content]));
        return $this->success(['message' => 'Email has sent']);
    }
}
