<?php

namespace App\Http\Controllers\Api\V1;


use App\Models\News;
use App\Traits\GlobalFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class NewsController extends Controller
{
    use GlobalFunction;

  
    public function index(Request $request){
        if(!empty($_GET['news_id'])){
            $news_id = $_GET['news_id'];
            $data = News::find($news_id);
        }else{
            $limit=205;
            $offset=0;
            if(!empty($_GET['limit'])){
                $limit= $_GET['limit'];
            }
            if(!empty($_GET['offset'])){
                $offset= $_GET['offset'];
            }
            $data =News::orderBy('created_at');
            if(empty($request->status)){
                $data->whereStatus('active');
            }else{
                if($request->status!='all'){
                $data->whereStatus($request->status);
                }
            }
            $data =$data->offset($offset)->limit($limit)->get();
        }
        if(!empty($data)){ 
            return $this->success($data);
        }else{
            return $this->error('News is not found');
        }
    }


    public function add(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta'); 
		$created_at= date("Y-m-d H:i:s");
        if (!empty($request->user_id)) {
            $input['title'] = $request->title;
            $input['description'] = $request->description;
            $input['user_id'] = $request->user_id;
            $input['status'] = $request->status;
            if ($request->image) {
                $file_name = 'news-' . time();
                $file_name = $this->uploadImage($request->image, '/public/images/news/', $file_name);
                $input['image'] = url('/images/news/' . $file_name.'');
            }
            // if ($request->hasFile('image')) {
            //     $image = $request->file('image');
            //     $name = time().'.'.$image->getClientOriginalExtension();
            //     $destinationPath = public_path('/images/news');
            //     $image->move($destinationPath, $name);
            //     $input['image'] = url('images/news/'.$name);
            // }else{
            //     $input['image'] = 'images';
            // }
            if(empty($request->news_id)){
                $input['created_at'] = $created_at;
                News::create($input);
            }else{
                News::find($request->news_id)->update($input);
            }
            return $this->success($input);
        } else {
            return $this->error('Failed  add news');
        }
    }
}



