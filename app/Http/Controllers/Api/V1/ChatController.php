<?php

namespace App\Http\Controllers\Api\V1;


use App\Models\Chat;
use App\Models\Booking;
use App\Models\Driver;
use App\Models\Vendor;
use App\Models\Notification;
use App\Traits\GlobalFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan;

class ChatController extends Controller
{
    use GlobalFunction;

    public function index()
    {
        $where = array();
        if (!empty($_GET['booking_id'])) {
            $where['booking_id'] = $_GET['booking_id'];
        }
        $data = Chat::where($where)->with('participant:user_id,first_name,last_name,profile_picture')->with('sender:user_id,first_name,last_name,profile_picture')->orderBy('chat_id')->get();
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Chat is not found');
        }
    }

    public function send(Request $request){
        $dataBooking = Booking::find($request->booking_id);
        $user_id =$request->auth->user_id;
        if($dataBooking){
            if($dataBooking->driver_id){
                $dataVendor = Driver::find($dataBooking->driver_id);
            }elseif($dataBooking->vendor_id){
                $dataVendor = Vendor::find($dataBooking->vendor_id);
            }
            if($user_id==$dataVendor->user_id){
                $form['participant_id']=$dataBooking->user_id;
            }else{
                $form['participant_id']=$dataVendor->user_id;
            }
            $form['sender_id']=$user_id;
            $form['booking_id']=$request->booking_id;
            $form['message']=$request->message;
            Chat::create($form);
            Notification::create([
                'user_id' => $form['participant_id'],
                'title' => 'Yo got a new message',
                'message' => $request->message,
                'payload' => json_encode(['booking_id' => $request->booking_id]),
                'type' => 'push'
            ]);
            Artisan::call('push:sender');
            return $this->success($form);
        }else{
            return $this->error('Booking ID is not found');
        }
    }

    public function delete(Request $request){
        $dataChat = Chat::find($request->chat_id);
        if($dataChat){
            $delete = $dataChat->delete();
            return $this->success($dataChat);
        }else{
            return $this->error('Chat is not found');
        }
    }
}
