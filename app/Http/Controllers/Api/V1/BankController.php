<?php

namespace App\Http\Controllers\Api\V1;


use App\Models\Bank;
use App\Models\BankAccount;
use App\Traits\GlobalFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class BankController extends Controller
{
    use GlobalFunction;

    public function index()
    {
        $where = array();
        if (!empty($_GET['is_visible'])) {
            $where['is_visible'] = $_GET['is_visible'];
        }
        $data = Bank::where($where)->orderBy('bank_id')->get();
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Bank is not found');
        }
    }

    public function account(Request $request)
    {
        $account = BankAccount::with('bank')->has('bank')->whereUserId($request->auth->user_id)->first();
        return $this->success($account);
    }

    public function save(Request $request)
    {
        $account = BankAccount::whereUserId($request->auth->user_id)->first() ?? new BankAccount();
        $account->fill([
            'user_id' => $request->auth->user_id,
            'bank_id' => $request->input('bank_id'),
            'account_name' => $request->input('account_name'),
            'account_number' => $request->input('account_number'),
        ]);
        $account->save();
        return $this->success($account);
    }
}
