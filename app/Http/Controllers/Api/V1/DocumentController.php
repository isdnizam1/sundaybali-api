<?php

namespace App\Http\Controllers\Api\v1;
use App\Traits\GlobalFunction;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Document;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Reviews;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;

    public function insert_document(Request $request)
    {
        $code = 500;
        $checkUser = User::whereToken($request->token)->first();
        if (!empty($request->auth->user_id)) {
            if (!empty($request->card_id)) {
                $file_name = 'CardID_' . uniqid() . '_' . time();
                $file_name = $this->uploadImage($request->card_id, '/public/images/document/', $file_name);
                $input['card_id'] = URL::to('images/document/' . $file_name . '');
            } else {
                return $this->error('Card ID is Empty');
            }


            if (!empty($request->sim_card)) {
                $file_name = 'SIMCARD_' . uniqid() . '_' . time();
                $file_name = $this->uploadImage($request->sim_card, '/public/images/document/', $file_name);
                $input['sim_card'] = URL::to('images/document/' . $file_name . '');
            } else {
                return $this->error('SIM ID is Empty');
            }

            if (!empty($request->card_id_selfie)) {
                $file_name = 'CardIDSelfie_' . uniqid() . '_' . time();
                $file_name = $this->uploadImage($request->card_id_selfie, '/public/images/document/', $file_name);
                $input['card_id_selfie'] = URL::to('images/document/' . $file_name . '');
            } else {
                return $this->error('Card ID Selfie is Empty');
            }
            if (!empty($request->another_document)) {
                $file_name = 'AnotherDocument' . uniqid() . '_' . time();
                $file_name = $this->uploadImage($request->another_document, '/public/images/document/', $file_name);
                $input['another_document'] = URL::to('images/document/' . $file_name . '');
            }
            $input['user_id'] = $request->auth->user_id;
            $input['status'] = 'pending';
            $checkUser=  Document::whereUser_id($request->auth->user_id)->first();
            if($checkUser){
                $input['note'] = '';
                Document::find($checkUser->document_id)->update($input);
            }else{
                Document::create($input);
            }
            $formUser['is_document_verified']='pending';
            $dataUser=  User::find($request->auth->user_id)->update($formUser);
        
            $this->set_notif('admin', 'Upload Profile Document', $request->auth->first_name.' Upload profile document', ["user_id" => $request->auth->user_id, 'redirect' => '/admin/user/detail/' . $request->auth->user_id]);
            $code = 200;
        } else {
            $message = 'User is not found';
        }
        if ($code == 200) {
            return $this->success($input);
        } else {
            return $this->error($message);
        }
    }


    public function set_status_document(Request $request)
    {
        $code = 500;
        $checkData=  Document::find($request->document_id);
        if (!empty($checkData)) {
            $input['status'] = $request->status;
            $input['note'] = $request->note;
            $checkData->update($input);
            if($request->status=='approved'){
                $title= 'Document Approved';
                $subtitle= 'Your document has been approved';
                $documentStatus='verified';
            }elseif($request->status=='decline'){
                $title= 'Document Declined';
                $subtitle= 'Your document has been declined with reason '.$request->note;
                $documentStatus='unverified';
            }
            $formUser['is_document_verified']=$documentStatus;
            $dataUser=  User::find($checkData->user_id)->update($formUser);

            $this->set_notif($checkData->user_id, $title, $subtitle, ["user_id" => $request->auth->user_id]);
            $code = 200;
        } else {
            $message = 'Document is not found';
        }
        if ($code == 200) {
            return $this->success($input);
        } else {
            return $this->error($message);
        }
    }
}