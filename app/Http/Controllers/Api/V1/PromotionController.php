<?php

namespace App\Http\Controllers\Api\V1;


use App\Models\Promotion;
use App\Traits\GlobalFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class PromotionController extends Controller
{
    use GlobalFunction;

  
    public function index(Request $request){
        if(!empty($_GET['promotion_id'])){
            $promotion_id = $_GET['promotion_id'];
            $data = Promotion::find($promotion_id);
        }else{
            $where['status'] = 'active';
            $limit=5;
            $offset=0;
            if(!empty($_GET['limit'])){
                $limit= $_GET['limit'];
            }
            if(!empty($_GET['offset'])){
                $offset= $_GET['offset'];
            }
            $data =Promotion::orderBy('created_at');
                if(!empty($request->category)){
                    $data->whereCategory($request->category);
                }
                if(!empty($request->status)){
                    if($request->status!='all'){
                        $data->whereStatus($request->status);
                    }
                }else{
                    $data->whereStatus('active');
                }
            $data =$data->offset($offset)->limit($limit)->get();
        }
        if(!empty($data)){ 
            return $this->success($data);
        }else{
            return $this->error('Promotion is not found');
        }
    }


    public function add(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta'); 
		$created_at= date("Y-m-d H:i:s");
        if (!empty($request->user_id)) {
            $input['promotion_name'] = $request->promotion_name;
            $input['description'] = $request->description;
            $input['status'] = $request->status;
            $input['voucher_code'] = $request->voucher_code;
            $input['discount'] = $request->discount;
            if ($request->image) {
                if (filter_var($request->image, FILTER_VALIDATE_URL) === FALSE) {
                    $file_name = 'promotion-' . time();
                    $file_name = $this->uploadImage($request->image, '/public/images/promotion/', $file_name);
                    $input['image'] = url('/images/promotion/' . $file_name.'');
                }
            }
            // if ($request->hasFile('image')) {
            //     $image = $request->file('image');
            //     $name = time().'.'.$image->getClientOriginalExtension();
            //     $destinationPath = public_path('/images/promotion');
            //     $image->move($destinationPath, $name);
            //     $input['image'] = url('images/promotion/'.$name);
            // }
            if(empty($request->promotion_id)){
                $input['created_at'] = $created_at;
                $input['user_id'] = $request->user_id;
                Promotion::insert($input);
            }else{
                $checkData = Promotion::find($request->promotion_id)->first();
                if($checkData){
                    Promotion::find($request->promotion_id)->update($input);
                }
            }
            return $this->success($input);
        } else {
            return $this->error('Failed is add promotion');
        }
    }

    public function edit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta'); 
		$updated_at= date("Y-m-d H:i:s");
        if (!empty($checkData)) {
            $input['promotion_name'] = $request->promotion_name;
            $input['description'] = $request->description;
            $input['status'] = $request->status;
            $input['voucher_code'] = $request->voucher_code;
            $input['discount'] = $request->discount;
            if ($request->image) {
                $file_name = 'promotion-' . time();
                $file_name = $this->uploadImage($request->image, '/public/images/promotion/', $file_name);
                $input['image'] = url('/images/promotion/' . $file_name.'');
            }
            // if ($request->hasFile('image')) {
            //     $image = $request->file('image');
            //     $name = time().'.'.$image->getClientOriginalExtension();
            //     $destinationPath = public_path('/images/promotion');
            //     $image->move($destinationPath, $name);
            //     $input['image'] = 'images/promotion/'.$name;
            //     //remove old image
            //     if(!empty($checkData->image)){
            //     $old_file_path = public_path($checkData->image);
            //     File::delete($old_file_path);
            //     }
            // }
            $input['updated_at'] = $updated_at;
            Promotion::wherePromotion_id($request->id)->update($input);
            return $this->success($input);
        } else {
            return $this->error('Promotion is not found');
        }
    }

  

}
