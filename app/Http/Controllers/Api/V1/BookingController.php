<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Libs\Distance;
use App\Libs\Slack;
use App\Models\Booking;
use App\Models\BookingDriver;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\User;
use App\Models\Attraction;
use App\Models\Driver;
use App\Models\Mutation;
use App\Models\Report;
use App\Models\Settings;
use App\Models\Wallet;
use App\Models\Notification;
use App\Models\BookingStatusLog;
use App\Models\BookingImage;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Royryando\Duitku\Facades\Duitku;
use Illuminate\Support\Facades\Artisan;
use App\Models\Car;
use App\Models\Vendor;
use Carbon\Carbon;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;

    public function index(Request $request)
    {
        date_default_timezone_set("UTC");
        $limit = 100;
        $offset = 0;
        if (!empty($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        }
        $upcoming= false;
        if($request->status[0]=='upcoming'){
            $upcoming= true;
            $request->status==["waiting pickup"];
        }
        $booking = Booking::orderByDesc('pickup_date')->offset($offset)->limit($limit);
        if (!empty($request->auth->access != 'admin')) {
            $booking->whereUser_id($request->auth->user_id);
        }
        if (!empty($request->user_id)) {
            $booking->whereUser_id($request->user_id);
        }
        if ($request->driver_id) {
            $booking->whereDriver_id($request->driver_id);
        }
        if ($request->status) {
            $booking->whereIn('status', $request->status);
        }

        if ($upcoming) {
            $booking->whereBetween('pickup_date', [date('Y-m-d H:i:s', strtotime('-1days')), date('Y-m-d H:i:s')]);
        }
        $data = $booking->get();


        foreach($data as &$key){
            if($key->status=='completed'){
                $remark = 'Fee transaction '.$key->booking_id;
                $key->fee_mutation = Mutation::whereRemark($remark)->first();
            }
        }
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('User is empty');
        }
    }



    public function self_drive(Request $request)
    {
        date_default_timezone_set("UTC");
        $limit = 100;
        $offset = 0;
        if (!empty($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        }
        $booking = Booking::whereType('SELF DRIVE')->with('car')->orderByDesc('created_at')->offset($offset)->limit($limit);
        if (!empty($request->user_id)) {
            $booking->whereUser_id($request->user_id);
        }
        if ($request->vendor_id) {
            $booking->whereVendor_id($request->vendor_id);
        }
        if ($request->status) {
            $booking->whereIn('status', $request->status);
        }
        $data = $booking->get();
        foreach($data as &$key){
            // if($key->status=='completed'){
            //     $remark = 'Fee transaction '.$key->booking_id;
            //     $key->fee_mutation = Mutation::whereRemark($remark)->first();
            // }
        }
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('User is empty');
        }
    }

    public function detail(Request $request)
    {
        date_default_timezone_set("UTC");
        $data = Booking::with('user')->with('car')->with('report')->with(['picked_up_images', 'completed_images'])->whereBooking_id($request->booking_id)->first();
        if (!empty($data)) {
            $data->driver = Driver::with('user:user_id,first_name,last_name,profile_picture')->with('vehicle')->find($data->driver_id);
            if ($data->type == 'TOUR') {
                $destination = json_decode($data->destination);
                foreach ($destination as $key => $value) {
                    $data->destination = Attraction::select('attraction_id', 'attraction_name')->find($value)->get(['attraction_id', 'attraction_name', 'address', 'image']);
                }
            }
            $data['additional'] = json_decode($data->additional_data);

            if($data->status=='completed'){
                $remark = 'Fee transaction '.$request->booking_id;
                $data->fee_mutation = Mutation::whereRemark($remark)->first();
            }

            $data->status_logs = BookingStatusLog::whereBooking_id($request->booking_id)->orderByDesc('created_at')->get();
            return $this->success($data);
        } else {
            return $this->error('Booking ID is not found');
        }
    }


    public function report(Request $request)
    {
        date_default_timezone_set("UTC");
        $data = Report::with('booking')->get();
        if (!empty($data)) {
            $data->driver = Driver::with('user:user_id,first_name,last_name,profile_picture')->with('vehicle')->find($data->driver_id);
            if ($data->type == 'TOUR') {
                $destination = json_decode($data->destination);
                foreach ($destination as $key => $value) {
                    $data->destination = Attraction::select('attraction_id', 'attraction_name')->find($value)->get(['attraction_id', 'attraction_name', 'address', 'image']);
                }
            }
            $data['additional'] = json_decode($data->additional_data);

            if($data->status=='completed'){
                $remark = 'Fee transaction '.$request->booking_id;
                $data->fee_mutation = Mutation::whereRemark($remark)->first();
            }
            return $this->success($data);
        } else {
            return $this->error('Booking ID is not found');
        }
    }



    public function submit(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $user_id = $request->auth->user_id;
            if (!empty($user_id)) {
                $booking_id = 'SB-' . $user_id . '' . strtotime(date('Y-m-d H:i:s'));
                $additional_data['pickup_date'] = $request->pickup_date;
                $additional_data['pickup_time'] = $request->pickup_time;
                $additional_data['pickup_location'] = $request->pickup_location;
                $additional_data['note'] = $request->note;
                $additional_data['pax'] = $request->pax;
                // $additional_data['duration'] = $request->duration;
                $input['status'] = 'pending';
                $input['user_id'] = $user_id;
                $input['car_type'] = $request->car_type;
                $input['destination'] = json_encode($request->destination);
                $input['booking_id'] = $booking_id;
                $input['additional_data'] = json_encode($additional_data);
                Booking::create($input);
                return $this->success($additional_data);
            } else {
                return $this->error('User is not found');
            }
        });
    }


    public function selectDriver(Request $request)
    {
        // date_default_timezone_set('Asia/Jakarta');
        $dateTime = date("Y-m-d H:i:s");
        $deadline_confirmation = $this->add_datetime($dateTime, "+2 minute");
        $user_id = $request->auth->user_id;
        $booking_id = $request->booking_id;
        $driver_id = $request->driver_id;
        $dataBooking = Booking::with('user')->whereBooking_id($booking_id)->whereStatus('pending')->wherePayment_status('paid')->whereNull('driver_id')->first();
        $dataDriver = Driver::find($driver_id);
        if (!empty($dataBooking) and !empty($dataDriver)) {
            if($dataDriver->status=='Offline') {
                return $this->error('This driver is not online');
            } 
            $additional_data = json_decode($dataBooking->additional_data, true);
            $additional_data['driver_choosen_by'] = $user_id;
            $input['status'] = 'waiting driver';
            $input['driver_id'] = $driver_id;
            $input['additional_data'] = json_encode($additional_data);
            Booking::whereBooking_id($booking_id)->update($input);

            $bookingDriver['booking_id'] = $booking_id;
            $bookingDriver['driver_id'] = $driver_id;
            $bookingDriver['deadline_confirmation'] = $deadline_confirmation;
            $bookingDriver['status'] = 'pending';
            BookingDriver::create($bookingDriver);

            // $this->set_notif($dataDriver->user_id, 'Orderan baru !', 'Anda mendapatkan orderan baru, silahkan untuk segera mengkonfirmasi pesanan', ["booking_id" => $booking_id]);
            $this->set_notif($dataBooking->user_id, 'We found a guide', 'We found a guide to accompany a pleasant trip', ["booking_id" => $booking_id]);
            return $this->success($dataDriver);
        } else {
            return $this->error('This booking already has a driver');
        }
    }

    public function get_price(Request $request)
    {
        if ($request->input('type') == 'CAR CHARTER') {
            $pax = intval($request->input('pax'));
            $duration = $request->input('duration');
            $duration_key = str_replace(' ', '_', "price_" . strtolower($request->input('duration')));
            $pricing = Settings::getValueByName('car_charter_price');
            $price = 0;
            $fee_driver = 0;
            foreach ($pricing['user'] as $key => $value) {
                if ($pax >= $value['person'][0] && $pax <= $value['person'][1]) {
                    $price = $value[$duration_key];
                    break;
                }
            }
            foreach ($pricing['driver'] as $key => $value) {
                if ($pax >= $value['person'][0] && $pax <= $value['person'][1]) {
                    $fee_driver = $value[$duration_key];
                    break;
                }
            }
            return response()->json(compact('pax', 'duration', 'pricing', 'price', 'fee_driver'));
        }

        if ($request->input('type') == 'CITY TRANSFER') {
            $distance = new Distance();
            $pax = intval($request->input('pax'));
            $matrix = $distance->getDistance($request->origin, $request->destination);
            $km = ceil($matrix['distance']['value'] / 1000);
            $time = ceil($matrix['duration']['value']);
            $pricing = Settings::getValueByName('city_transfer_price');
            $fee_per_km = 0;
            $fee_driver_per_km = 0;
            $minimum_charge = 0;
            $fee_driver = 0;
            $sisa_km = 1;
            $sisa_km_driver = 1;
            foreach ($pricing['user'] as $key => $value) {
                if ($pax >= $value['person'][0] && $pax <= $value['person'][1]) {
                    $fee_per_km = $value['fee_per_km'];
                    $minimum_charge = $value['minimum_charge'];
                    $minimum_km =  $value['minimum_km'];
                    if (intval($request->is_airport_transfer) == 1) {
                        $fee_per_km = $value['airport_transfer']['fee_per_km'];
                        $minimum_charge = $value['airport_transfer']['minimum_charge'];
                    }
                    if($km > $minimum_km){
                        $sisa_km =$km-$minimum_km;
                    }
                    break;
                }
            }
            foreach ($pricing['driver'] as $key => $value) {
                if ($pax >= $value['person'][0] && $pax <= $value['person'][1]) {
                    $fee_driver_per_km = $value['fee_per_km'];
                    $minimum_driver_charge = $value['minimum_charge'];
                    $minimum_km_driver = $value['minimum_km'];
                    if (intval($request->is_airport_transfer) == 1) {
                        $fee_driver_per_km = $value['airport_transfer']['fee_per_km'];
                        $minimum_driver_charge = $value['airport_transfer']['minimum_charge'];
                    }
                    if($km > $minimum_km_driver){
                        $sisa_km_driver =$km-$minimum_km_driver;
                    }
                    break;
                }
            }

            if($km <= $minimum_km){
                $price = $minimum_charge;
            }else{
                $price = $minimum_charge+$fee_per_km * $sisa_km;
            }
            // if (intval($request->is_airport_transfer) == 1) {
            // $price = $minimum_charge+$fee_per_km * $sisa_km;
            // }else{
            // $price = $fee_per_km * $km;
            // }
            // $fee_driver = $fee_driver_per_km * $km;
            if($km <= $minimum_km_driver){
                $fee_driver = $minimum_driver_charge;
            }else{
                $fee_driver = $minimum_driver_charge + $fee_driver_per_km*$sisa_km_driver;
            }
            if ($price < $minimum_charge) $price = $minimum_charge;
            if ($fee_driver < $minimum_driver_charge) $fee_driver = $minimum_driver_charge;
            return response()->json(compact(
                'matrix',
                'fee_per_km',
                'minimum_charge',
                'pax',
                'pricing',
                'km',
                'time',
                'price',
                'fee_driver'
            ));
        }


        if ($request->input('type') == 'SELF DRIVE') {
            $car_id = intval($request->input('car_id'));
            $dataCar = Car::find($car_id);
            $dataVendor = Vendor::find($dataCar->vendor_id);

            $dataSetting = Settings::whereSetting_name('self_drive_price')->first();
            $feeSelfDrive = json_decode($dataSetting->value);
            foreach($feeSelfDrive as $fee) {
                if($fee->type=='PICKUP') $pickupFee = $fee;
                if($fee->type=='RETURN') $returnFee = $fee;
            }

            // $additional_data = json_decode($dataVendor->additional_data);
            // $free_delivery_meter=0;
            // if(!empty($additional_data->free_delivery)){
            //     $free_delivery_meter = $additional_data->free_delivery*1000;
            // }
            
            $pickup_time = $request->input('pickup_time');
            $dropoff_time = $request->input('dropoff_time');
            $pickup_location = $request->input('pickup_location');
            $dropoff_location = $request->input('dropoff_location');
            
            // $duration = $this->get_between_date($pickup_time,$dropoff_time);
            $orderPeriod = Carbon::parse($pickup_time)->diff(Carbon::parse($dropoff_time));
            /* Duration Formula:
            0 < x <= 2 h = +0
            2 < x <= 7 h = +0.5
            x > 7 h = +1
            **/
            $duration = $orderPeriod->d;
            if ($orderPeriod->h >= 2 && $orderPeriod->h < 7) {
                if ($orderPeriod->h == 2 && $orderPeriod->i == 0) $duration += 0;
                else $duration += 0.5;
            } else if($orderPeriod->h >= 7 ) {
                if ($orderPeriod->h == 7 && $orderPeriod->i == 0) $duration += 0.5;
                else $duration += 1;
            }

            $distance = new Distance();
            $carLocation = $dataCar->latitude.','.$dataCar->longitude;

            $pickup = $distance->getDistance($pickup_location, $carLocation);
            $pickup_meter = $pickup['distance']['value'];
            // if($pickup_meter >= $free_delivery_meter){ //pickup meter lebih besar dari free delivery meter
            //     $is_pickup_free =false;
            //     $pickup_meter = $pickup_meter-$free_delivery_meter;
            // }else{
            //     $is_pickup_free =true;
            // }
            if($pickup_meter > ($pickupFee->min_km*1000)){ //pickup meter lebih besar dari minimum km
                $is_pickup_free =false;
                $pickup_meter = $pickup_meter-($pickupFee->min_km*1000);
            }else{
                $is_pickup_free =true;
            }

            $return = $distance->getDistance($dropoff_location, $carLocation);
            $return_meter = $return['distance']['value'];
            // if($return_meter >= $free_delivery_meter){ //pickup meter lebih besar dari free delivery meter
            //     $is_return_free =false;
            //     $return_meter = $return_meter-$free_delivery_meter;
            // }else{
            //     $is_return_free =true;
            // }
            if($return_meter > ($returnFee->min_km*1000)){ //pickup meter lebih besar dari free delivery meter
                $is_return_free =false;
                $return_meter = $return_meter-($returnFee->min_km*1000);
            }else{
                $is_return_free =true;
            }
            
            $delivery_fee = Settings::whereSetting_name('delivery_fee_self_drive')->first();
            $delivery_fee = json_decode($delivery_fee->value);
            
            $pickup_fee = 0;
            $return_fee = 0;

            if($is_pickup_free==false){
                // if($pickup_meter <=$delivery_fee->meter){
                //     $pickup_fee = $delivery_fee->price;
                // }else{
                //     $pickup_fee = $pickup_meter/$delivery_fee->meter*$delivery_fee->price;
                // }
                $pickup_fee = ceil($pickup_meter/1000) * $pickupFee->price_per_km;
            }
            if($is_return_free==false){
                // if($return_meter <=$delivery_fee->meter){
                //     $return_fee = $delivery_fee->price;
                // }else{
                //     $return_fee = $return_meter/$delivery_fee->meter*$delivery_fee->price;
                // }
                $return_fee = ceil($return_meter/1000) * $returnFee->price_per_km;
            }
            $car_price = $dataCar->rental_price;
            $subtotal_car_price = $car_price*$duration;
            $total_price = $pickup_fee+$return_fee+$subtotal_car_price;
            return response()->json(compact('pickup_fee', 'return_fee','car_price', 'duration', 'total_price'));
        }

    }

    public function book_city_transfer(Request $request)
    {
        $user_id = $request->auth->user_id;
        if (!empty($user_id)) {
            $booking = DB::transaction(function () use ($request, $user_id) {
                if(empty($request->booking_id)){
                    for ($i = 1; $i <= 9999; $i++) {
                        $booking_id = 'CT' . date('Ymd') . str_pad($i, 4, '0', STR_PAD_LEFT).'0';
                        if (!Booking::find($booking_id)) break;
                    }
                }else{
                    $booking_id =$request->booking_id;
                }
                $input['type'] = 'CITY TRANSFER';
                // $input['pickup_date'] = date('Y-m-d', strtotime($request->timing['date']));
                // $input['pickup_time'] = date('H:i', strtotime($request->timing['time']));

                $later = date('Y-m-d H:i:s', strtotime('+1days'));
                $input['pickup_date'] = date('Y-m-d', strtotime($request->timing['date']));
                $input['pickup_time'] = date('H:i', strtotime($request->timing['time']));
                if($later <= $input['pickup_date'].' '.$input['pickup_time']){
                    $book_type ='later';
                }else{
                    $book_type ='now';
                }
                $payment_provider='DUITKU';
                $input['book_type'] = $book_type;
                $input['status'] = 'pending';
                $input['user_id'] = $user_id;
                $input['booking_id'] = $booking_id;
                $input['adult'] = $request->pax['adult'];
                $input['child'] = $request->pax['child'];
                $input['infant'] = $request->pax['infant'];
                $input['rate_per_km'] = $request->rate_per_km;
                $input['car_price'] = $request->subtotal;
                $input['distance'] = $request->distance;
                $input['subtotal'] = $request->subtotal + $request->channel['fee'];
                $input['driver_fee'] = $request->driver_fee;
                $input['is_airport_transfer'] = $request->is_airport_transfer;
                $input['payment_provider'] = $payment_provider;
                $input['payment_fee'] = $request->channel['fee'];
                $input['platform_fee'] = $request->subtotal - $request->driver_fee;
                $additional_data['pickup_location'] = $request->pickup;
                $additional_data['destination'] = $request->destination;
                $additional_data['note'] = $request->note;
                $additional_data['preferred_language'] = $request->preferred_language;
                $additional_data['flight_number'] = $request->flight_number;
                $input['additional_data'] = json_encode($additional_data);
                if(empty($request->booking_id)){
                    $booking = Booking::create($input);
                }else{
                    $booking = Booking::whereBooking_id($request->booking_id)->update($input);
                }
                error_reporting(0);
                    $inquiry = Duitku::createInvoice(
                        $booking_id,
                        $input['subtotal'],
                        $request->channel['code'],
                        "City Transfer",
                        implode(" ", [$request->auth->first_name, $request->auth->last_name]),
                        $request->auth->email,
                        120
                    );
                    $dataBooking = Booking::whereBooking_id($booking_id)->update([
                        'payment_data' => json_encode(
                            [
                                'channel' => $request->channel,
                                'inquiry' => $inquiry
                            ]
                        )
                    ]);
                Slack::bookUpdate($booking_id);
                return Booking::find($booking_id);
            });
            return $this->success($booking);
        } else {
            return $this->error('User is not found');
        }
    }


    public function book_car_charter(Request $request)
    {
        $user_id = $request->auth->user_id;
        if (!empty($user_id)) {
            $booking = DB::transaction(function () use ($request, $user_id) {
                if(empty($request->booking_id)){
                    for ($i = 1; $i <= 9999; $i++) {
                        $booking_id = 'CC' . date('Ymd') . str_pad($i, 4, '0', STR_PAD_LEFT).'0';
                        if (!Booking::find($booking_id)) break;
                    }
                }else{
                    $booking_id =$request->booking_id;
                }
                $input['type'] = 'CAR CHARTER';
                $later = date('Y-m-d H:i:s', strtotime('+1days'));
                $input['pickup_date'] = date('Y-m-d', strtotime($request->timing['date']));
                $input['pickup_time'] = date('H:i', strtotime($request->timing['time']));
                if($later <= $input['pickup_date'].' '.$input['pickup_time']){
                    $book_type ='later';
                }else{
                    $book_type ='now';
                }

                if(!empty($request->payment_provider)){
                    $payment_provider=$request->payment_provider;
                }else{
                    $payment_provider='DUITKU';
                }
                $input['book_type'] = $book_type;
                $input['status'] = 'pending';
                $input['user_id'] = $user_id;
                $input['booking_id'] = $booking_id;
                $input['adult'] = $request->pax['adult'];
                $input['child'] = $request->pax['child'];
                $input['infant'] = $request->pax['infant'];
                $input['rate_per_km'] = 0;
                $input['car_price'] = $request->subtotal;
                $input['distance'] = 0;
                $input['duration'] = $request->input('duration');
                $input['subtotal'] = $request->subtotal + $request->channel['fee'];
                $input['driver_fee'] = $request->driver_fee;
                $input['payment_provider'] = $payment_provider;
                if($payment_provider=='DUITKU'){
                    $input['payment_fee'] = $request->channel['fee'];
                }elseif($payment_provider=='CASH'){
                    $input['payment_status'] = 'paid';
                    $input['payment_fee'] = 0;
                }
                $input['platform_fee'] = $request->subtotal - $request->driver_fee;
                $additional_data['note'] = $request->note;
                $additional_data['pickup_location'] = $request->pickup;
                $additional_data['preferred_language'] = $request->preferred_language;
                // $additional_data['destination'] = $request->destination;
                $input['additional_data'] = json_encode($additional_data);
                if(empty($request->booking_id)){
                    $booking = Booking::create($input);
                }else{
                    $booking = Booking::whereBooking_id($request->booking_id)->update($input);
                }
                error_reporting(0);
                if($payment_provider=='DUITKU'){
                    $inquiry = Duitku::createInvoice(
                        $booking_id,
                        $input['subtotal'],
                        $request->channel['code'],
                        "City Transfer",
                        implode(" ", [$request->auth->first_name, $request->auth->last_name]),
                        $request->auth->email,
                        120
                    );
                    $dataBooking = Booking::whereBooking_id($booking_id)->update([
                        'payment_data' => json_encode(
                            [
                                'channel' => $request->channel,
                                'inquiry' => $inquiry
                            ]
                        )
                    ]);
                }

                Slack::bookUpdate($booking_id);
                return Booking::find($booking_id);
            });
            return $this->success($booking);
        } else {
            return $this->error('User is not found');
        }
    }


    
    public function detail_new(Request $request)
    {
        date_default_timezone_set("UTC");
        $booking = Booking::with('driver')->findOrFail($request->input('booking_id'));
        return response()->json(compact('booking'));
    }

    public function declineBooking(Request $request)
    {
        $dataBooking = Booking::find($request->booking_id);
        if($dataBooking){
            if($dataBooking->type!='SELF DRIVE'){
                $bookingDriver = BookingDriver::whereBooking_id($request->booking_id)->update(['status' => 'expired', 'reason' => 'Customer cancelled']);
            }
            $additional_data = json_decode($dataBooking->additional_data,true);
            $additional_data['reason'] = $request->reason;
            $booking = Booking::find($request->booking_id);
            $booking->update([
                'status' => 'cancel',
                'driver_id' => null,
                'additional_data' => json_encode($additional_data),
            ]);
            return $this->success($booking);
        }else{
            return $this->error('Booking is not found');
        }
    }


    public function confirmBooking(Request $request)
    {
        $dataBooking = Booking::find($request->booking_id);
        if($dataBooking){
            if($dataBooking->type=='SELF DRIVE'){
            $booking = Booking::find($request->booking_id);
            $booking->update([
                'status' => 'waiting pickup',
            ]);
            return $this->success($booking);
            }

        }else{
            return $this->error('Booking is not found');
        }
    }

    public function sendFeeTranscation(Request $request)
    {
        $remark = 'Fee transaction '.$request->booking_id;
        $mutation = Mutation::whereRemark($remark)->first();
        if($mutation){
            $dataBooking = Booking::whereBooking_id($request->booking_id)->first();
            $checkReport = Report::whereBooking_id($request->booking_id)->first();
            if($request->action=='driver'){
                if($checkReport){
                    $updateReport = $checkReport->update(['status' => 'driver-win','reason'=>$request->note]);
                }
                $updateMutation = Mutation::find($mutation->mutation_id)->update(['status' => $request->status]);
                $wallet = Wallet::findOrFail($mutation->wallet_id);
                $mutations = Mutation::whereWalletId($wallet->wallet_id)->get();
                $completedMutations = Mutation::whereWalletId($wallet->wallet_id)->whereStatus('completed')->get();
                // $wallet->fill([
                //     'balance' => $mutations->sum('amount_in') - $mutations->sum('amount_out'),
                //     'balance_withdraw' =>  $completedMutations->sum('amount_in') - $completedMutations->sum('amount_out')
                // ]);  
                // $wallet->save();
                Notification::create([
                    'user_id' => $wallet->user_id,
                    'title' => "Featour e-Wallet",
                    // 'title' => ($mutation->amount_in ?? 0) > 0 ? "Your wallet balance increases" : "Your wallet balance is reduced",
                    'message' => sprintf('(%s%s) %s', ($mutation->amount_in ?? 0) > 0 ? '+' : '-', number_format(($mutation->amount_in ?? 0) > 0 ? $mutation->amount_in : $mutation->amount_out), $mutation->remark),
                    'payload' => json_encode(["mutation_id" => $mutation->mutation_id]),
                ]);


                Notification::create([
                    'user_id' => $dataBooking->user_id,
                    'title' => "Report Update",
                    // 'title' => ($mutation->amount_in ?? 0) > 0 ? "Your wallet balance increases" : "Your wallet balance is reduced",
                    'message' => 'Your report with order ID #'.$request->booking_id.' has been declined with reason '.$request->note,
                    'payload' => json_encode(["mutation_id" => $request->booking_id]),
                ]);

                Artisan::call('push:sender');
            }else{
                if($checkReport){
                    $updateReport = $checkReport->update(['status' => 'customer-win','reason'=>$request->note]);
                }
                $updateMutation = Mutation::find($mutation->mutation_id)->update(['status' => 'decline']);
                $wallet = Wallet::findOrFail($mutation->wallet_id);
                Notification::create([
                    'user_id' => $wallet->user_id,
                    'title' => "Order Reported",
                    // 'title' => ($mutation->amount_in ?? 0) > 0 ? "Your wallet balance increases" : "Your wallet balance is reduced",
                    'message' => 'Your order with ID #'.$request->booking_id.' has been reported',
                    'payload' => json_encode(["booking_id" => $request->booking_id]),
                ]);

                Notification::create([
                    'user_id' => $dataBooking->user_id,
                    'title' => "Report Update",
                    // 'title' => ($mutation->amount_in ?? 0) > 0 ? "Your wallet balance increases" : "Your wallet balance is reduced",
                    'message' => 'Your report with order ID #'.$request->booking_id.' has been accepted. your money will be refund',
                    'payload' => json_encode(["mutation_id" => $request->booking_id]),
                ]);
                Artisan::call('push:sender');
            }
        }

        return $this->success($mutation);
    }

    public function refundBooking(Request $request)
    {
        $dataBooking = Booking::find($request->booking_id);
        $checkUser = $request->auth;
        if($dataBooking){
            $additional_data = json_decode($dataBooking->additional_data,true);
            $additional_data['refunded_by']['user_id'] = $checkUser->user_id;
            $additional_data['refunded_by']['full_name'] = $checkUser->first_name.' '.$checkUser->last_name;
            $dataBooking->update([
                'payment_status' => 'refunded',
                'additional_data' => json_encode($additional_data),
            ]);
            return $this->success($dataBooking);
        }else{
            return $this->error('Booking is not found');
        }
    }


    public function paidBooking(Request $request)
    {
        $dataBooking = Booking::find($request->booking_id);
        $checkUser = $request->auth;
        if($dataBooking){
            $additional_data = json_decode($dataBooking->additional_data,true);
            $additional_data['action_paid_by']['user_id'] = $checkUser->user_id;
            // Move from observer
            if ($dataBooking->type=='SELF DRIVE') {
                $dataBooking->update([
                    'payment_status' => 'paid',
                    'status' => 'waiting pickup',
                    'additional_data' => json_encode($additional_data),
                ]);
            } else {
                $dataBooking->update([
                    'payment_status' => 'paid',
                    'status' => 'pending',
                    'additional_data' => json_encode($additional_data),
                ]);
            }
            BookingStatusLog::create([
                'booking_id' => $dataBooking->booking_id,
                'status' => 'pending',
                'description' =>  'Payment Received',
                'notes' => ''
            ]);
            return $this->success($dataBooking);
        }else{
            return $this->error('Booking is not found');
        }
    }



    public function book_self_drive(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pickup.date' => 'required|date|after_or_equal:today',
            'return_location.date' => 'required|date|after_or_equal:today|after_or_equal:pickup.date',
        ]);

        if($validator->fails()){
            return response()->json([
                'code' => 400,
                'status' => 'failed',
                'message' => json_decode($validator->messages(), true)
            ], 400);
        }

        $user_id = $request->auth->user_id;
        if (!empty($user_id)) {
            $booking = DB::transaction(function () use ($request, $user_id) {
                if(empty($request->booking_id)){ 
                    for ($i = 1; $i <= 9999; $i++) {
                        $booking_id = 'SD' . date('Ymd') . str_pad($i, 4, '0', STR_PAD_LEFT);
                        if (!Booking::find($booking_id)) break;
                    }
                } else {
                    $booking_id = $request->booking_id;
                }

                $dataCar = Car::with('vehicle')->with('vendor')->whereCar_id($request->car_id)->first();
                if($dataCar){
                    $dataVendor = Vendor::find($dataCar->vendor_id);
                    $input['type'] = 'SELF DRIVE';
                    $later = date('Y-m-d H:i:s', strtotime('+1days'));
                    $input['pickup_date'] = date('Y-m-d', strtotime($request->pickup['date']));
                    $input['pickup_time'] = date('H:i', strtotime($request->pickup['time']));
                    $returnDate = date('Y-m-d', strtotime($request->return_location['date']));
                    $returnTime = date('H:i', strtotime($request->return_location['time']));
                    $pickup_datetime = $input['pickup_date'].' '.$input['pickup_time'];
                    $return_datetime = $returnDate.' '.$returnTime;

                    $duration = $this->get_between_date($pickup_datetime,$return_datetime);
                    $totalCarPrice = $dataCar->rental_price*$duration;
                    if($dataVendor->service_fee_type=='percentase'){
                        $platform_fee = ceil($totalCarPrice*$dataVendor->service_fee/100);
                    }else{
                        $platform_fee = $dataVendor->service_fee;
                    }
                    if($later <= $input['pickup_date'].' '.$input['pickup_time']){
                        $book_type ='later';
                    }else{
                        $book_type ='now';
                    }

                    $input['book_type'] = $book_type;
                    $input['status'] = 'pending';
                    $input['user_id'] = $user_id;
                    $input['booking_id'] = $booking_id;
                    $input['rate_per_km'] = 0;
                    $input['car_id'] = $request->car_id;
                    $input['car_price'] = $dataCar->rental_price;
                    $input['distance'] = 0;
                    $input['duration'] = $duration.' Days';
                    $input['subtotal'] = $totalCarPrice+$request->pickup['fee']+$request->return_location['fee'];
                    // $input['subtotal'] = $request->subtotal + $request->channel['fee'];
                    $input['driver_fee'] = 0;
                    $input['vendor_id'] = $dataCar->vendor_id;
                    $input['payment_provider'] = 'DUITKU';
                    $input['payment_fee'] = 0;
                    $input['platform_fee'] = $platform_fee;
                    // $input['platform_fee'] = $request->subtotal - $request->driver_fee;
                    $additional_data['note'] = $request->note;
                    $additional_data['pickup_location'] = $request->pickup;
                    $additional_data['return_location'] = $request->return_location;
                    // $additional_data['destination'] = $request->destination;
                    $input['additional_data'] = json_encode($additional_data);
                    if(empty($request->booking_id)){
                        $booking = Booking::create($input);
                    }else{
                        // $booking = Booking::whereBooking_id($request->booking_id)->update($input);
                        $booking = Booking::whereBooking_id($request->booking_id)->first();
                        if (empty($booking)) {
                            return $this->error('Booking not found');
                        }

                        $booking->fill($input);
                        $booking->save();
                    }
                    error_reporting(0);
                    $inquiry = Duitku::createInvoice(
                        $booking_id,
                        $input['subtotal'],
                        $request->channel['code'],
                        "Self Drive",
                        implode(" ", [$request->auth->first_name, $request->auth->last_name]),
                        $request->auth->email,
                        15 //minutes
                    );
                    $booking->update([
                        'payment_data' => json_encode(
                            [
                                'channel' => $request->channel,
                                'inquiry' => $inquiry
                            ]
                        )
                    ]);
                    Slack::bookUpdate($booking_id);
                    BookingStatusLog::create([
                        'booking_id' => $booking_id,
                        'status' => 'pending',
                        'description' =>  $booking->booking_status,
                        'notes' => ''
                    ]);
                    return Booking::find($booking_id);
                }else{
                    return $this->error('Car is not found');
                }
            });
            return $this->success($booking);
        } else {
            return $this->error('User is not found');
        }
    }



    public function on_rent(Request $request)
    {
        $limit = 100;
        $offset = 0;
        if (!empty($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        }
        $status=["waiting pickup","picked up"];
        $booking = Booking::select('booking_id','status','car_id','user_id','vendor_id','pickup_date','pickup_time','additional_data')->with('car')->with('user')->orderByDesc('created_at')->offset($offset)->limit($limit);
        if (!empty($request->user_id)) {
            $booking->whereUser_id($request->user_id);
        }
        if (!empty($request->vendor_id)) {
            $booking->whereVendor_id($request->vendor_id);
        }
        $booking->whereIn('status', $status);
        $data = $booking->get();
        // dd($data);
        foreach($data as &$key){
            $additional_data = json_decode($key->additional_data);
            $key->vehicle_name = $key->car->vehicle->name;
            $key->vehicle_year = $key->car->year;
            $key->rented_by =  $key->user ? $key->user->first_name.' '.$key->user->last_name : '';
            $key->start_date =  $key->pickup_date.' '.$key->pickup_time;
            $key->end_date = isset($additional_data->return_location) ? $additional_data->return_location->date.' '.$additional_data->return_location->time : '';
            unset($key->user,$key->booking_status,$key->arr_data,$key->pax_info,$key->name,$key->pickup_date,$key->booking_status,$key->pickup_time);
        }
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Data is not found');
        }
    }

    public function my_rent(Request $request)
    {
        $limit = 100;
        $offset = 0;
        if (!empty($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        }
        // $status=["waiting pickup","picked up"];
        $booking = Booking::select('booking_id','car_id','user_id','vendor_id','pickup_date','pickup_time','additional_data')->with('car')->with('user')->orderByDesc('created_at')->offset($offset)->limit($limit);
        if (!empty($request->user_id)) {
            $booking->whereUser_id($request->user_id);
        }
        if (!empty($request->vendor_id)) {
            $booking->whereVendor_id($request->vendor_id);
        }
        // $booking->whereIn('status', $status);
        $data = $booking->get();

        foreach($data as &$key){
            $additional_data = json_decode($key->additional_data);
            $key->vehicle_name = $key->car->vehicle->name;
            $key->vehicle_year = $key->car->year;
            $key->rented_by = $key->user->first_name.' '.$key->user->last_name;
            $key->start_date =  $key->pickup_date.' '.$key->pickup_time;
            $key->end_date =  $additional_data->return_location->date.' '.$additional_data->return_location->time;
            unset($key->car,$key->user,$key->booking_status,$key->arr_data,$key->pax_info,$key->name,$key->pickup_date,$key->booking_status,$key->pickup_time,$key->additional_data);
        }
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Data is not found');
        }
    }

    public function confirmCleaningCar(Request $request)
    {
        $dataBooking = Booking::find($request->booking_id);
        if($dataBooking){
            if($dataBooking->type=='SELF DRIVE'){
                $additional_data = json_decode($dataBooking->additional_data,true);
                $is_cleaning_car =$request->is_cleaning_car;
                if($request->is_cleaning_car=='true'){
                    $is_cleaning_car = true;
                }elseif($request->is_cleaning_car=='false'){
                    $is_cleaning_car = false;
                }
                $additional_data['is_cleaning_car'] = $is_cleaning_car;
                $dataBooking->update([
                    'additional_data' => json_encode($additional_data),
                ]);
                if($is_cleaning_car==true){
                    $this->set_notif($dataBooking->user_id,'Booking Updates','Yeay.. your car has been cleaned, ready for delivery', ["booking_id" => $dataBooking->booking_id, 'redirect' => '/admin/bookingDetail/' . $dataBooking->booking_id],'push');
                }
                return $this->success($dataBooking);
            }

        }else{
            return $this->error('Booking is not found');
        }
    }


    // public function uploadPhoto(Request $request)
    // {
    //     $dataBooking = Booking::find($request->booking_id);
    //     $type = 'carImage' . $request->type;
    //     if($dataBooking){
    //         if($dataBooking->type=='SELF DRIVE'){
    //             $additional_data = json_decode($dataBooking->additional_data,true);
    //             if($request->image){
    //                 $totalCarImage = 0;
    //                 if(!empty($additional_data[$type])){
    //                     $totalCarImage = count($additional_data[$type]);
    //                 }
    //                 $file_name = 'carDetail-'. time();
    //                 $file_name = $this->uploadImage($request->image, 'public/images/cars/', $file_name);
    //                 $image= URL::to('images/cars/' . $file_name . '');
    //                 $additional_data[$type][$totalCarImage] = $image;
    //                 // $dataBooking->update([
    //                 // 'additional_data' => json_encode($additional_data),
    //                 // ]);
    //                 $dataBooking->additional_data = json_encode($additional_data);
    //                 $dataBooking->save();
    //                 return $this->success($dataBooking);
    //             }else{
    //                 return $this->error('Image is not found');
    //             }
    //         } else {
    //             return $this->error('Booking type is not SELF DRIVE');
    //         }
    //     }else{
    //         return $this->error('Booking is not found');
    //     }
    // }

    public function uploadPhoto(Request $request)
    {
        $dataBooking = Booking::find($request->booking_id);
        if($dataBooking){
            if($dataBooking->type=='SELF DRIVE'){
                if($request->image){
                    $file_name = 'carDetail-'. time();
                    $file_name = $this->uploadImage($request->image, 'public/images/cars/', $file_name);
                    $image= URL::to('images/cars/' . $file_name . '');
                    
                    $bookingImage = BookingImage::create([
                        'image' =>  $image,
                        'booking_id' => $request->booking_id,
                        'type' => $request->type,
                    ]);

                    return $this->success($bookingImage);
                }else{
                    return $this->error('Image is not found');
                }
            } else {
                return $this->error('Booking type is not SELF DRIVE');
            }
        }else{
            return $this->error('Booking is not found');
        }
    }

    public function statusLogs(Request $request)
    {
        $limit = 100;
        $offset = 0;
        if (!empty($_GET['limit'])) {
            $limit = $_GET['limit'];
        }
        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        }
        $logs = BookingStatusLog::orderByDesc('created_at')->offset($offset)->limit($limit);
        if (!empty($request->booking_id)) {
            $logs->whereBooking_id($request->booking_id);
        }
        $data = $logs->get();
        // dd($data);
        if (!empty($data)) {
            return $this->success($data);
        } else {
            return $this->error('Data is not found');
        }
    }

    // public function deletePhoto(Request $request)
    // {
    //     $dataBooking = Booking::find($request->booking_id);
    //     $type = 'carImage' . $request->type;
    //     $file_name = $request->file_name;

    //     if($dataBooking){
    //         if($dataBooking->type=='SELF DRIVE'){
    //             $additional_data = json_decode($dataBooking->additional_data,true);
    //             if($request->file_name){

    //                 if (isset($additional_data[$type])) {
                        
    //                     // Delete data image
    //                     $index = array_search($file_name, $additional_data[$type]);
    //                     if($index !== FALSE){
    //                         array_splice($additional_data[$type], $index, 1); 
    //                     }
    //                     $dataBooking->update([
    //                         'additional_data' => json_encode($additional_data),
    //                     ]);

    //                     // Delete Physical Image
    //                     $parse = substr(parse_url($file_name, PHP_URL_PATH), 1);
    //                     if(file_exists($parse)) {
    //                         unlink($parse);
    //                     }

    //                     return $this->success($dataBooking);
    //                 } else {
    //                     return $this->error('Type is not found');
    //                 }

    //             }else{
    //                 return $this->error('Image is not found');
    //             }
    //         } else {
    //             return $this->error('Booking type is not SELF DRIVE');
    //         }
    //     }else{
    //         return $this->error('Booking is not found');
    //     }
    // }

    public function deletePhoto(Request $request)
    {
        $dataBookingImage = BookingImage::find($request->id);
        $file_name = $dataBookingImage->image;

        if($dataBookingImage){
            // Delete data image
            $dataBookingImage->delete();
         
            // Delete Physical Image
            $parse = substr(parse_url($file_name, PHP_URL_PATH), 1);
            if(file_exists($parse)) {
                unlink($parse);
            }

            return $this->success($dataBookingImage);
        }else{
            return $this->error('Booking Image is not found');
        }
    }
}
