<?php

namespace App\Http\Controllers\Api\V1;


use App\Models\User;
use App\Traits\GlobalFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libs\Firebase;
use App\Models\BookingDriver;
use App\Models\Driver;
use App\Models\FirebaseToken;
use App\Models\Mutation;
use App\Models\Vendor;
use App\Models\BankAccount;
use App\Models\Document;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Mail\SendOtp;

class AuthController extends Controller
{
    use GlobalFunction;



    public function firebase_signin(Request $request)
    {
        $firebase = new Firebase();
        $auth = $firebase->verifyIdToken($request->input('id_token'));
        if(!empty($auth->uid)){
            if(!empty($auth->phoneNumber)){
                $user = User::wherePhone($auth->phoneNumber)->first();
            }elseif(!empty($auth->providerData[0]->providerId=='facebook.com')){
                $user = User::whereEmail($auth->providerData[0]->email)->first();
            }elseif(!empty($auth->providerData[0]->providerId=='google.com')){
                $user = User::whereEmail($auth->providerData[0]->email)->first();
            }elseif(!empty($auth->providerData[0]->providerId=='password')){
                $user = User::whereEmail($auth->providerData[0]->email)->first();
            }elseif(!empty($auth->phoneNumber)){
                $user = User::wherePhone($auth->phoneNumber)->first();
            }else{
                $user = User::whereEmail($auth->email)->first();
            }

            if(!$user){
                if(!empty($auth->phoneNumber)){
                    $checkPhone = User::wherePhone($auth->phoneNumber)->first();
                    if($checkPhone){
                        return $this->error('Phone is already exist');
                    }
                }
                $user = new User();
                $full_name = explode(' ', $auth->displayName);
                $first_name='User';
                $last_name='';
                if(!empty($full_name[0])) $first_name = $full_name[0];
                if(!empty($full_name[1])) $last_name = $full_name[1];
                if(empty($auth->providerData[0]->uid)){
                    $social_id = null;
                }else{
                    $social_id = $auth->providerData[0]->uid;
                }
                if(empty($auth->providerData[0]->email)){
                    $email = '';
                }else{
                    $email = $auth->providerData[0]->email;
                }
                if($auth->providerData[0]->providerId=='password'){
                    $registered_via = 'email';
                }else{
                    $registered_via = $auth->providerData[0]->providerId;
                }

                $user->fill([
                    'social_id' => $social_id,
                    'email' => $email,
                    'profile_picture' => $auth->photoUrl,
                    'registered_via' => str_replace('.com', '', $registered_via),
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $auth->phoneNumber,
                    'access' => 'user'
                ]);
                if (!$user->token) $user->token = sha1(uniqid() . time());
                $user->save();
            }else{
                if($user->account_status=='banned'){
                    return $this->error('Your account has been banned');
                    // return $this->success($user);
                }
            }
        if ($user->wallet_only) $user->today_earning = Mutation::whereWalletId($user->wallet_only->wallet_id)->whereDate('created_at', date('Y-m-d'))->where('amount_in', '>', 0)->whereType('order')->get()->sum('amount_in');
        if($user->access=='vendor'){
            $user = User::with('bookings', 'active_bookings', 'vendor', 'wallet_only')->where('account_status', 'active')->findOrFail($user->user_id);
        }else{
            $user = User::with('bookings', 'active_bookings', 'driver', 'wallet_only')->where('account_status', 'active')->findOrFail($user->user_id);
        }
        return $this->success($user);
        }else{
        return $this->error($auth);
        }
    }

    // public function guide_signin(Request $request)
    // {
    //     $firebase = new Firebase();
    //     $signInResult = $firebase->sign($request->email, $request->password);
        
    //     if($signInResult['status']=='success'){
    //         $user = User::whereEmail($request->email)->first();
    //         if($user){
    //             return $this->success($user);
    //         }else{
    //             return $this->error('User is not found');
    //         }
    //         return $this->success($user);
    //     }else{
    //         return $this->error($signInResult['message']);
    //     }
    // }


    public function guide_signin(Request $request)
    {
        if (filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
            $loginType = "email";
            $checkAccount = User::whereEmail($request->username)->first();
        }else{
            $loginType = "phone";
            $checkAccount = User::wherePhone($request->username)->first();
        }

        if($checkAccount){
            if (Auth::attempt([$loginType => $request->username, 'password' => $request->password])) {
                $user = Auth::user();
                // if ($user->wallet_only) $user->today_earning = Mutation::whereWalletId($user->wallet_only->wallet_id)->whereDate('created_at', date('Y-m-d'))->where('amount_in', '>', 0)->whereType('order')->get()->sum('amount_in');
                if($user->access=='vendor'){
                    $user = User::with('vendor:user_id,vendor_id')->findOrFail($user->user_id);
                }else{
                    $user = User::with('bookings', 'active_bookings', 'driver', 'wallet_only')->where('account_status', 'active')->findOrFail($user->user_id);
                }
                return $this->success($user);
            } else {
                return $this->error('Username and password doesnt match');
            }
        }else{
            return $this->error('Account is not found');
        }
    }

    public function sendOtp(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $checkUser = User::whereEmail($request->email)->first();
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            $additional_data['verification_code'] = mt_rand(1000, 9999);
            $input['additional_data'] = json_encode($additional_data);
            $update = User::find($checkUser->user_id)->update($input);
            $res['user_id'] = $checkUser->user_id;
            $res['email'] = $checkUser->email;
            $res['name'] = $checkUser->first_name;
            $res['verification_code'] = $additional_data['verification_code'];
            Mail::to($checkUser->email)->send(new SendOtp($res));
            unset($res['verification_code']);
            return $this->success($res);
        } else {
            return $this->error('User is not found');
        }
    }



    public function otpVerification(Request $request)
    {
        $checkUser = User::where('additional_data', 'like', '%"verification_code":'.$request->verification_code.'%')->first();
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            $verification_code = '';
            if (!empty($additional_data['verification_code'])) {
                $verification_code = $additional_data['verification_code'];
            }
            if ($verification_code == $request->verification_code) {
                $additional_data['verification_code'] = '';
                $input['additional_data'] = json_encode($additional_data);
                $update = User::find($checkUser->user_id)->update($input);
                return $this->success($checkUser);
            } else {
                return $this->error('Verification code doesnt match');
            }
        } else {
            return $this->error('Verification code doesnt match');
        }
    }

    public function check_existing_email(Request $request)
    {
        $input['email'] = $request->email;
        if (filter_var($input['email'], FILTER_VALIDATE_EMAIL)) {
            $check_user = User::whereEmail($input['email'])->first();
            if (!empty($check_user)) {
                return $this->error('Email is already exist');
            } else {
                return $this->success($input);
            }
        } else {
            return $this->error('Email is not valid');
        }
    }


    public function check_existing_phone(Request $request)
    {
        $input['phone'] = $request->phone;
        $check_user = User::wherePhone($input['phone'])->first();
        if (!empty($check_user)) {
            return $this->success($input);
        } else {
            return $this->error('Please Register with email first');
        }
    }

    public function submit_registration(Request $request)
    {
        $code = 200;
        $input['email'] = $request->email;
        // $input['username'] = $request->username;
        $input['password'] = Hash::make($request->password);
        $input['access'] = 'user';
        $input['phone'] = $request->phone;
        $input['phone_status'] = 'unverified';
        $input['account_status'] = 'active';
        $input['first_name'] = $request->first_name;
        $input['last_name'] = $request->last_name;
        $input['token'] = sha1(uniqid());
        $checkEmail = User::whereEmail($input['email'])->first();
        if (!empty($checkEmail)) {
            return $this->error('Email is already exist');
        }
        $checkPhone = User::wherePhone($input['phone'])->first();
        if (!empty($checkPhone)) {
            return $this->error('Phone is already exist');
        }
        // $checkUsername = User::whereUsername($input['username'])->first();
        // if (!empty($checkUsername)) {
        //     return $this->error('Username is already exist');
        // }
        if ($request->password != $request->confirm_password) {
            return $this->error('Password and confirmation password doesnt match');
        }

        $additional_data['phone_verification_code'] = mt_rand(100000, 999999);
        $input['additional_data'] = json_encode($additional_data);
        $user = User::create($input);
        // $token =  $user->createToken('api_token');
        // $this->sendNotifMessage(890867, $input['phone'], [$additional_data['phone_verification_code']]);
        if ($code == 200) {
            return $this->success($user);
        } else {
            return $this->error($message);
        }
    }


    public function resend_phone_verification(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $checkUser = $request->auth;
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            $additional_data['phone_verification_code'] = mt_rand(100000, 999999);
            $input['additional_data'] = json_encode($additional_data);
            $update = User::find($checkUser->user_id)->update($input);
            // $this->sendNotifMessage(890867, $checkUser->phone, [$additional_data['phone_verification_code']]);
            // $this->sendMessage('Your verification code  is : ' . $additional_data['phone_verification_code'], $checkUser->phone);
            return $this->success($input);
        } else {
            return $this->error('User is not found');
        }
    }


    public function phone_verification(Request $request)
    {
        $verification_code = $request->verification_code;
        $checkUser = $request->auth;
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            $phone_verification_code = '';
            if (!empty($additional_data['phone_verification_code'])) {
                $phone_verification_code = $additional_data['phone_verification_code'];
            }

            if ($verification_code == $phone_verification_code) {
                $input['phone_status'] = 'verified';
                $additional_data['phone_verification_code'] = '';
                $input['additional_data'] = json_encode($additional_data);
                $update = User::find($checkUser->user_id)->update($input);
                $input['user_id'] = $checkUser->user_id;
                return $this->success($input);
            } else {
                return $this->error('Verification code doesnt match');
            }
        } else {
            return $this->error('User is not found');
        }
    }


    public function login(Request $request)
    {
        if (!empty($request->email) and !empty($request->password)) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user();
                if ($user->access == 'vendor') {
                    $vendor = Vendor::where('user_id', $user->user_id)->first();
                    if ($vendor) {
                        if ($vendor->status != 'Active') {
                            return $this->error('Your Vendor is Non Active');
                        }
                    } else {
                        return $this->error('Your Vendor is Not Found');
                    }
                }
                return $this->success($user);
            } else {
                return $this->error('Credentials not match');
            }
        } else {
            return $this->error('Credentials not match');
        }
    }


    public function change_password(Request $request)
    {
        // $checkUser = $request->auth;
        $checkUser = User::whereEmail($request->email)->first();
        $code = 500;
        if (empty($request->old_password) || empty($request->new_password) || empty($request->confirm_new_password)) return $this->error('Please fill in all fields');
        if (strlen(trim($request->new_password)) < 6) return $this->error('Password must be at least 6 characters long');
        if (!empty($checkUser)) {
            $email = $checkUser->email;
            if ($request->new_password == $request->confirm_new_password) {
                if (Auth::attempt(['email' => $email, 'password' => $request->old_password])) {
                    $input['password'] = Hash::make($request->new_password);
                    $update = User::find($checkUser->user_id)->update($input);
                    $code = 200;
                } else {
                    $message = 'Old password is wrong';
                }
            } else {
                $message = 'New password doesnt match';
            }
        } else {
            $message = 'User is not found';
        }
        if ($code == 200) {
            return $this->success($checkUser);
        } else {
            return $this->error($message);
        }
    }

    public function send_forgot_password(Request $request)
    {
        $checkUser = User::whereEmail($request->email)->first();
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            $additional_data['forgotten_password_code'] = mt_rand(100000, 999999);
            $input['additional_data'] = json_encode($additional_data);
            User::find($checkUser->user_id)->update($input);
            // $this->sendMessage('You are receiving this message because we received a password reset request for your account. Your forgotten password code  is : ' . $additional_data['forgotten_password_code'], $checkUser->phone);
            return $this->success(['email' => $request->email]);
        } else {
            return $this->error('email doesn\'t exists');
        }
    }



    public function check_forgot_password_code(Request $request)
    {
        $checkUser = User::whereEmail($request->email)->first();
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            if (!empty($additional_data['forgotten_password_code'])) {
                if ($additional_data['forgotten_password_code'] == $request->forgotten_password_code) {
                    $result['token'] = $checkUser->token;
                    $result['email'] = $checkUser->email;
                    $result['forgotten_password_code'] = $additional_data['forgotten_password_code'];
                    return $this->success($result);
                }
            }
        }
        return $this->error('email and forgotten password code doesnt match');
    }

    public function create_new_password(Request $request)
    {
        $checkUser = $request->auth;
        $code = 500;
        if (!empty($checkUser)) {
            $additional_data = json_decode($checkUser->additional_data, true);
            if ($request->password == $request->confirm_password) {
                if (!empty($additional_data['forgotten_password_code'])) {
                    if ($additional_data['forgotten_password_code'] == $request->forgotten_password_code) {
                        $additional_data['forgotten_password_code'] = '';
                        $input['additional_data'] = json_encode($additional_data);
                        $input['password'] = Hash::make($request->password);
                        $update = User::find($checkUser->user_id)->update($input);
                        $code = 200;
                    } else {
                        $message = 'Forgotten password code doesnt match';
                    }
                } else {
                    $message = 'Forgotten password code doesnt exist';
                }
            } else {
                $message = 'Password and Confirmation password doesnt match';
            }
        } else {
            $message = 'User is not found';
        }
        if ($code == 200) {
            return $this->success($checkUser);
        } else {
            return $this->error($message);
        }
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'Tokens Revoked'
        ];
    }


   

    public function facebook_signin(Request $request)
    {
        if(!empty($request->id)){
            $user = User::whereSocial_id($request->id)->first();
            if(!$user){
                $user = new User();
                $full_name = explode(' ', $request->displayName);
                $first_name='User';
                $last_name='';
                if(!empty($full_name[0])) $first_name = $full_name[0];
                if(!empty($full_name[1])) $last_name = $full_name[1];
                $user->fill([
                    'social_id' => $request->id,
                    'email' => $request->email,
                    'profile_picture' => $request->profile_picture,
                    'registered_via' =>'facebook',
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'access' => 'user'
                ]);
                if (!$user->token) $user->token = sha1(uniqid() . time());
                $user->save();
            }
            if ($user->wallet_only) $user->today_earning = Mutation::whereWalletId($user->wallet_only->wallet_id)->whereDate('created_at', date('Y-m-d'))->where('amount_in', '>', 0)->whereType('order')->get()->sum('amount_in');
            // $user = User::with('bookings', 'active_bookings', 'driver', 'wallet_only')->where('account_status', 'active')->findOrFail($user->user_id);
            return $this->success($user);
        }else{
            return $this->error('ID is  not found');
        }
    }


    public function unauthorized()
    {
        return $this->error('Unauthorized');
    }

    public function firebase_token(Request $request)
    {
        $firebase_token = FirebaseToken::where($request->only('token'))->first() ?? new FirebaseToken();
        $firebase_token->fill([
            'user_id' => $request->auth->user_id,
            'token' => $request->token
        ]);
        $firebase_token->save();
        return response()->json(compact('firebase_token'));
    }

    public function remove_firebase_token(Request $request)
    {
        $firebase_token = FirebaseToken::whereUser_id($request->auth->user_id)->delete();
        return $this->success($request->auth);
    }



    public function check_account(Request $request)
    {
        $username = $request->username;
        $check_user = User::select('user_id', 'phone', 'email', 'access')->wherePhone($username)->orWhere('email',$username)->first();
        if (!empty($check_user)) {
            return $this->success($check_user);
        } else {
            return $this->success(false);
        }
    }
    public function user_registration(Request $request)
    {
        $code = 200;
        $input['email'] = $request->email;
        $input['access'] = 'user';
        $input['phone'] = $request->phone;
        $input['phone_status'] = 'unverified';
        $input['account_status'] = 'active';
        $input['first_name'] = $request->first_name;
        $input['last_name'] = $request->last_name;
        $input['token'] = sha1(uniqid());
        $checkEmail = User::whereEmail($input['email'])->first();
        if (!empty($checkEmail)) {
            return $this->error('Email is already exist');
        }
        $checkPhone = User::wherePhone($input['phone'])->first();
        if (!empty($checkPhone)) {
            return $this->error('Phone is already exist');
        }
        $user = User::create($input);
        if ($code == 200) {
            return $this->success($user);
        } else {
            return $this->error($message);
        }
    }

    public function update_token(Request $request)
    {
        $token = $request->token;
        $phone = $request->phone;
        $checkUser = User::wherePhone($phone)->first();
        if (!empty($checkUser)) {
            $input['token'] = $token;
            $input['phone'] = $phone;
            $update = User::find($checkUser->user_id)->update($input);
            $input['user_id'] = $checkUser->user_id;
            return $this->success($input);
        } else {
            return $this->error('User is not found');
        }
    }

    public function driver_registration(Request $request)
    {
        DB::beginTransaction();

        try {
            // Create User
            $input['first_name'] = $request->first_name;
            $input['last_name'] = $request->last_name;
            $input['phone'] = $request->phone;
            $input['token'] = sha1(uniqid());
            $input['access'] = 'driver';
            $input['phone_status'] = 'unverified';
            $input['account_status'] = 'active';
            $input['registered_via'] = 'phone';
            $input['is_document_verified'] = 'pending';

            $checkPhone = User::wherePhone($input['phone'])->first();
            if (!empty($checkPhone)) {
                return $this->error('Phone is already exist');
            }
            $user = User::create($input);

            // Create Driver
            $driver['vehicle_id'] = $request->vehicle_id;
            $driver['plate_number'] = $request->plate_number;
            $driver['user_id'] = $user->user_id;
            Driver::create($driver);

            // Create Bank
            $bank['bank_id'] = $request->bank_id;
            $bank['account_name'] = $request->account_name;
            $bank['account_number'] = $request->account_number;
            $bank['user_id'] = $user->user_id;
            BankAccount::create($bank);

            // Insert Document
            $file_name = 'CardID_' . uniqid() . '_' . time();
            $file_name = $this->uploadImage($request->card_id, '/public/images/document/', $file_name);
            $document['card_id'] = URL::to('images/document/' . $file_name . '');

            $file_name = 'SIMCARD_' . uniqid() . '_' . time();
            $file_name = $this->uploadImage($request->sim_card, '/public/images/document/', $file_name);
            $document['sim_card'] = URL::to('images/document/' . $file_name . '');

            $file_name = 'AnotherDocument' . uniqid() . '_' . time();
            $file_name = $this->uploadImage($request->another_document, '/public/images/document/', $file_name);
            $document['another_document'] = URL::to('images/document/' . $file_name . '');

            $document['user_id'] = $user->user_id;
            $document['status'] = 'pending';
            Document::create($document);

            DB::commit();
            return $this->success($user);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage());
        }
    }


    // public function append_additional_data(Request $request)
    // {
    //     $checkUser = User::whereToken($request->token)->firstOrFail();
    //     $additional_data = json_decode($checkUser->additional_data, true);
    //     $additional_data[$request->input('key')] = $request->input('value');
    //     $checkUser->additional_data = json_encode($additional_data);
    //     $checkUser->save();
    //     return $this->success($checkUser);
    // }


    // public function resend_login_verification_code(Request $request)
    // {
    //     $checkUser = User::whereToken($request->token)->first();
    //     if (!empty($checkUser)) {
    //         $additional_data = json_decode($checkUser->additional_data, true);
    //         $additional_data['login_verification_code'] = mt_rand(100000, 999999);
    //         $input['additional_data'] = json_encode($additional_data);
    //         $update = User::find($checkUser->user_id)->update($input);
    //         $this->sendNotifMessage(892297, $checkUser->phone, [$additional_data['login_verification_code']]);
    //         return $this->success(['token' => $request->token]);
    //     } else {
    //         return $this->error('User is not found');
    //     }
    // }


    // public function submit_login_verification_code(Request $request)
    // {
    //     $checkUser = User::whereToken($request->token)->first();
    //     if (!empty($checkUser)) {
    //         $additional_data = json_decode($checkUser->additional_data, true);
    //         if (!empty($additional_data['login_verification_code'])) {
    //             if ($additional_data['login_verification_code'] == $request->login_verification_code) {
    //                 $additional_data = json_decode($checkUser->additional_data, true);
    //                 $additional_data['login_verification_code'] = '';
    //                 $checkUser->additional_data = json_encode($additional_data);
    //                 $checkUser->save();
    //                 $result['token'] = $checkUser->token;
    //                 return $this->success($result);
    //             }
    //         }
    //     }
    //     return $this->error('Login verification code doesnt match');
    // }



    // public function check_device(Request $request)
    // {
    //     $checkUser = User::whereToken($request->token)->first();
    //     return $this->success(UniqueDevice::select('granted', 'device_id')->where(['user_id' => $checkUser->user_id])->where($request->only('device_id'))->firstOrFail());
    // }

    // public function verify_new_device(Request $request)
    // {
    //     $checkUser = User::whereToken($request->token)->first();
    //     $device = UniqueDevice::where($request->only('device_id'))->where(['user_id' => $checkUser->user_id])->firstOrFail();
    //     if ($device->otp == $request->input('otp')) {
    //         $device->granted = 'yes';
    //         $device->save();
    //         return $this->success(200);
    //     } else {
    //         return $this->error('Invalid OTP code');
    //     }
    // }

    // public function resend_device_verification(Request $request)
    // {
    //     $checkUser = User::whereToken($request->token)->first();
    //     $otp = mt_rand(100000, 999999);
    //     $device = UniqueDevice::where($request->only('device_id'))->where(['user_id' => $checkUser->user_id])->firstOrFail();
    //     $device->otp = $otp;
    //     $device->save();
    //     $this->sendNotifMessage(892297, $checkUser->phone, [$otp]);
    //     return $this->success(200);
    // }

    // public function forgot_password(Request $request)
    // {
    //     $input = $request->all();
    //     $rules = array(
    //         'email' => "required|email"
    //     );
    //     $validator = Validator::make($input, $rules);
    //     if ($validator->fails()) {
    //         return $this->error($validator->errors()->first());
    //     } else {
    //         try {
    //             $response = Password::sendResetLink($request->only('email'));
    //             switch ($response) {
    //                 case Password::RESET_LINK_SENT:
    //                     return $this->success($request->all());
    //                 case Password::INVALID_USER:
    //                     return $this->error(trans($response));
    //             }
    //         } catch (\Swift_TransportException $ex) {
    //             return $this->error($ex->getMessage());
    //         } catch (Exception $ex) {
    //             return $this->error($ex->getMessage());
    //         }
    //     }
    //     return $this->error('Undefined');
    // }

}
