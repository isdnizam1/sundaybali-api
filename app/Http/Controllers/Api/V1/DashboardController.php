<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\Attraction;
use App\Models\Promotion;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\Vendor;
use App\Models\Car;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;


    public function index(Request $request)
    {
        $checkUser = $request->auth;;
        $endDate=date('Y-m-d H:i:s', strtotime('-0hours'));
        $startDate=date('Y-m-d H:i:s', strtotime($request->date));
        if($checkUser->access=='vendor'){
            $dataVendor = Vendor::whereUser_id($checkUser->user_id)->first();
            $data['total_car'] = Car::whereVendor_id($dataVendor->vendor_id)->count();
            $data['total_order'] = Booking::whereVendor_id($dataVendor->vendor_id)->count();
            $data['order_completed'] = Booking::whereVendor_id($dataVendor->vendor_id)->whereStatus('completed')->count();
            $data['order_pending'] = Booking::whereVendor_id($dataVendor->vendor_id)->whereStatus('pending')->count();
            $data['order_cancel'] = Booking::whereVendor_id($dataVendor->vendor_id)->whereStatus('cancel')->count();
            $data['order_progress'] = Booking::whereVendor_id($dataVendor->vendor_id)->whereIn('status', ['waiting pickup','picked up'])->count();
            $totalBooking = Booking::whereVendor_id($dataVendor->vendor_id)->whereStatus('completed')->whereBetween('created_at',[$startDate,$endDate])->get();
            $data['total_income'] = $totalBooking->sum('subtotal');
        }else{
            $data['total_city_transfer'] = Booking::whereType('CITY TRANSFER')->whereBetween('created_at',[$startDate,$endDate])->count();
            $data['total_car_charter'] = Booking::whereType('CAR CHARTER')->whereBetween('created_at',[$startDate,$endDate])->count();
            $data['total_own_tour'] = Booking::whereType('TOUR')->whereBetween('created_at',[$startDate,$endDate])->count();
            $data['total_users'] = User::whereAccess('user')->whereBetween('created_at',[$startDate,$endDate])->count();
            $data['total_driver'] = User::whereAccess('driver')->whereBetween('created_at',[$startDate,$endDate])->count();
            $data['total_promotion'] = Promotion::whereStatus('active')->whereBetween('created_at',[$startDate,$endDate])->count();
            $data['total_destination'] = Attraction::whereBetween('created_at',[$startDate,$endDate])->count();
            $data['total_vehicle'] = Vehicle::whereBetween('created_at',[$startDate,$endDate])->count();
            $totalBooking = Booking::whereStatus('completed')->whereBetween('created_at',[$startDate,$endDate])->get();
            $data['driver_fee'] = $totalBooking->sum('driver_fee');
            $data['platform_fee'] = $totalBooking->sum('platform_fee');
        }
        return $this->success($data);
    }
}
