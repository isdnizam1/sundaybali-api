<?php

namespace App\Http\Controllers\Api\V1;


use App\Models\User;
use App\Models\Settings;
use App\Traits\GlobalFunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class SettingsController extends Controller
{
    use GlobalFunction;

    public function index(Request $request)
    {
        $setting = Settings::orderBy('setting_id');
        if (!empty($_GET['setting_name'])) {
            $setting_name = $_GET['setting_name'];
            $setting->whereSetting_name($setting_name);
        }
        $data = $setting->first();
        if (!empty($data)) {
            if ($this->isJson($data->value)) {
                $data->value =  json_decode($data->value);
            }

            if($data->setting_name=='car_charter_duration'){
                if($request->duration=='six_hours'){
                    $data = $data->value->six_hours;
                }elseif($request->duration=='ten_hours'){
                    $data = $data->value->ten_hours;
                }else{
                    $data = $data->value;
                }
            }
            return $this->success($data);
        } else {
            return $this->error('Data is not found');
        }
    }



    public function edit_car_charter_price(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $created_at = date("Y-m-d H:i:s");
        $type = $request->type;
        if ($type == 'car_charter') {
            $setting = Settings::whereSetting_name('car_charter_price')->first();
        } elseif ($type == 'city_transfer') {
            $setting = Settings::whereSetting_name('city_transfer_price')->first();
        }
        if (!empty($setting)) {
            $value = json_decode($setting->value, true);
            $id = $request->id;
            $account = $request->account;
            if ($type == 'car_charter') {
                $value[$account][$id - 1]['price_6_hours'] = (int) $request->price_6_hours;
                $value[$account][$id - 1]['price_10_hours'] = (int) $request->price_10_hours;
            } elseif ($type == 'city_transfer') {
                $value[$account][$id - 1]['fee_per_km'] = (int) $request->fee_per_km;
                $value[$account][$id - 1]['minimum_charge'] = (int) $request->minimum_charge;
                $value[$account][$id - 1]['airport_transfer']['fee_per_km'] = (int) $request->fee_per_km_airport;
                $value[$account][$id - 1]['airport_transfer']['minimum_charge'] = (int) $request->minimum_charge_airport;
                $value[$account][$id - 1]['minimum_km'] = (int) $request->minimum_km;
            }

            $input['value'] = json_encode($value);
            $update = Settings::find($setting->setting_id)->update($input);
            return $this->success($input);
        } else {
            return $this->error('Setting is not found');
        }
    }

    public function edit_car_charter_rules(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $created_at = date("Y-m-d H:i:s");
        $setting = Settings::whereSetting_name('car_charter_rules')->first();
        if (!empty($setting)) {
            $value = json_decode($setting->value, true);
            $value[$request->id - 1]['heading'] = $request->heading;
            $value[$request->id - 1]['description'] = $request->description;
            $input['value'] = json_encode($value);
            $update = Settings::find($setting->setting_id)->update($input);
            return $this->success($input);
        } else {
            return $this->error('Setting is not found');
        }
    }

    public function edit_self_drive_price(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $created_at = date("Y-m-d H:i:s");
        $setting = Settings::whereSetting_name('self_drive_price')->first();
        if (!empty($setting)) {
            $value = json_decode($setting->value, true);
            $value[$request->id - 1]['min_km'] = $request->min_km;
            $value[$request->id - 1]['price_per_km'] = $request->price_per_km;
            $input['value'] = json_encode($value);
            $update = Settings::find($setting->setting_id)->update($input);
            return $this->success($input);
        } else {
            return $this->error('Setting is not found');
        }
    }

    public function edit_setting(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $created_at = date("Y-m-d H:i:s");
        $setting = Settings::whereSetting_name($request->setting_name)->first();
        if (!empty($setting)) {
            if($setting->setting_name=='car_charter_duration'){
                $value=json_decode($setting->value,true);
                if($request->action=='edit'){
                    $value[$request->duration][$request->id - 1] = $request->value;
                }else{
                    $value[$request->duration][] = $request->value;
                }
                $input['value']= json_encode($value);
            }elseif($setting->setting_name=='delivery_fee_self_drive'){
                $value=json_decode($setting->value,true);
                if($request->action=='edit'){
                    $value[$request->duration][$request->id - 1] = $request->value;
                }else{
                    $value[$request->duration][] = $request->value;
                }
                $input['value']= json_encode($value);
            }else{
                $input['value']= $request->value;
            }
            $update = Settings::find($setting->setting_id)->update($input);
            return $this->success($input);
        } else {
            return $this->error('Setting is not found');
        }
    }
}
