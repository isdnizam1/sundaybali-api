<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Royryando\Duitku\Facades\Duitku;
use App\Models\BookingStatusLog;

class DuitkuController extends Controller
{
    public function payment_methods(Request $request)
    {
        $payment_methods = Duitku::paymentMethods(100000);
        for($i = 0; $i < count($payment_methods); $i++) {
            if ($payment_methods[$i]['code'] == 'SP') {
                $payment_methods[$i]['name'] = 'QRIS';
                $payment_methods[$i]['image']  = URL::to('images/QRIS.png');
            }
            if ($payment_methods[$i]['code'] == 'OV') {
                $payment_methods[$i]['name'] = 'OVO';
            }
        }

        return response()->json(compact('payment_methods'));
    }

    public function server_return(Request $request)
    {
        $booking = Booking::findOrFail($request->input('merchantOrderId'));
        if ($booking->arr_data['payment_data']['inquiry']['reference'] == $request->input('reference')) {
            $booking->update([
                'payment_status' => ['paid', 'unpaid', 'canceled'][intval($request->input('resultCode'))]
            ]);
        }
        return view('web.closepage');
    }

    public function server_callback(Request $request)
    {
        $booking = Booking::findOrFail($request->input('merchantOrderId'));
        if ($booking->arr_data['payment_data']['inquiry']['reference'] == $request->input('reference'))
            $booking->fill([
                'payment_status' => ['paid', 'failed'][intval($request->input('resultCode'))]
            ]);
        $booking->save();
        if ($booking->payment_status == 'paid') {
            Notification::create([
                'user_id' => $booking->user_id,
                'title' => "Payment Received",
                'message' => 'We have receive your payment, thank you',
                'payload' => json_encode(["booking_id" => $booking->booking_id]),
            ]);
            BookingStatusLog::create([
                'booking_id' => $booking->booking_id,
                'status' => 'pending',
                'description' =>  'Payment Received',
                'notes' => ''
            ]);

            if ($booking->type=='SELF DRIVE') {
                $booking->update([
                    'status' => 'waiting pickup',
                ]);
            }
        }
        return response()->json(200);
    }
}
