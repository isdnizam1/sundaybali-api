<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Traits\GlobalFunction;
use App\Models\User;
use App\Models\Country;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Reviews;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use GlobalFunction;


    public function index(Request $request){
        $limit=200;
        $offset=0;
        if(!empty($_GET['limit'])){
            $limit= $_GET['limit'];
        }
        if(!empty($_GET['offset'])){
            $offset= $_GET['offset'];
        }
        $data =Country::orderBy('country_name')->offset($offset)->limit($limit);
        $data = $data->get();
        if(!empty($data)){ 
            return $this->success($data);
        }else{
            return $this->error('Country is not found');
        }
    }
}