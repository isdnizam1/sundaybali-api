<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\BookingDriver;
use App\Models\Driver;
use App\Models\BookingStatusLog;
use App\Models\Wallet;
use App\Models\Vendor;
use App\Models\Mutation;
use App\Traits\GlobalFunction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class BookingDriverController extends Controller
{
    use GlobalFunction;
    
    public function confirm_order(Request $request)
    {
        $booking_driver = BookingDriver::whereBookingDriverId($request->booking_driver_id)->first();
        $booking_driver->update(['status' => 'upcoming']);
        $booking = Booking::find($booking_driver->booking_id);
        $status = $booking->status;
        if($booking->status=='waiting driver'){
            if($booking->book_type=='later'){
                $status = 'awaiting departure time';
            }elseif($booking->book_type=='now'){
                $status = 'waiting pickup';
            }
        }else{
            $status = 'picked up';
        }
        $booking->update([
            'driver_id' => Driver::whereUserId($request->auth->user_id)->first()->driver_id,
            'status' => $status
        ]);
        Artisan::call('book:upcoming');
        // return response()->json(compact(
        //     'booking_driver'
        // ));
        return response()->json([
            'code' => 200,
            'status' => 'success',
            'booking_driver' => $booking_driver,
        ], 200);
    }
    public function skip_order(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $booking_driver = BookingDriver::whereBookingDriverId($request->booking_driver_id)->first();
            $booking_driver->update(['status' => 'decline', 'reason' => $request->reason]);
            $booking = Booking::find($booking_driver->booking_id);
            $booking->fill([
                'status' => 'pending',
                'driver_id' => null,
            ]);
            $booking->save();
            // return response()->json(compact(
            //     'booking_driver'
            // ));
            return response()->json([
                'code' => 200,
                'status' => 'success',
                'booking_driver' => $booking_driver,
            ], 200);
        });
    }
    public function picked_up(Request $request)
    {
        $booking = Booking::find($request->booking_id);
        if($booking) {
            DB::beginTransaction();
            // DB::update('update module_booking set status = "picked up" where booking_id = ?', [$booking->booking_id]);
            if ($booking->type=='SELF DRIVE') {
                $booking->fill([
                    'status' => 'picked up',
                    'driver_id' => null,
                ]);
            } else {
                $booking->fill([
                    'status' => 'picked up',
                ]);
            }
            $booking->save();
            BookingStatusLog::create([
                'booking_id' => $booking->booking_id,
                'status' => 'picked up',
                'description' => $booking->booking_status,
                'notes' => $request->notes
            ]);
            DB::commit();
            // $booking->update(['status' => 'picked up']);
            // $booking->save();
            // return response()->json(compact('booking'));
            return response()->json([
                'code' => 200,
                'status' => 'success',
                'booking' => $booking,
            ], 200);
        }
        return $this->error('Booking is not found');
    }
    public function finish_order(Request $request)
    {
        $booking = Booking::find($request->booking_id);
        $now = Carbon::now();
        if($booking) {
            // Add Mutation Wallet
            if($booking->type=='SELF DRIVE'){
                $wallet_id = Wallet::whereUserId(Vendor::find($booking->vendor_id)->user_id)->first()->wallet_id;
                $amount = $booking->subtotal-$booking->platform_fee;

                $additional_data = json_decode($booking->additional_data, true);
                $additional_data['return_location']['date'] = $now->toDateString();
                $additional_data['return_location']['time'] = $now->toTimeString();
                $booking->update([
                    'additional_data' => json_encode($additional_data)
                ]);
            }else{
                $wallet_id = Wallet::whereUserId(Driver::find($booking->driver_id)->user_id)->first()->wallet_id;
                $amount = $booking->driver_fee;
            }

            $booking->update([
                'status' => 'completed',
            ]);

            // Write change status log
            BookingStatusLog::create([
                'booking_id' => $booking->booking_id,
                'status' => 'completed',
                'description' => $booking->booking_status,
                'notes' => $request->notes
            ]);
            
            Mutation::create([
                'type' => 'order',
                'wallet_id' => $wallet_id,
                'amount_in' => $amount,
                'status' => 'pending',
                'amount_out' => 0,
                'remark' => 'Fee transaction ' . $booking->booking_id,
            ]);

            // return response()->json(compact('booking'));
            return response()->json([
                'code' => 200,
                'status' => 'success',
                'booking' => $booking,
            ], 200);
        }
        return $this->error('Booking is not found');
    }
    public function history(Request $request)
    {
        $booking_ids = BookingDriver::whereDriverId($request->auth->driver->driver_id)->whereIn('status', ['confirm', 'upcoming'])->pluck('booking_id') ?? [0];
        $bookings = Booking::whereIn('booking_id', $booking_ids)->orderBy('created_at', 'desc')->get();
        return $this->success($bookings);
    }
}
