<?php

namespace App\Console\Commands;

use App\Libs\Firebase;
use App\Models\Notification;
use Illuminate\Console\Command;

class PushSenderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:sender';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $firebase = new Firebase();
        foreach (Notification::whereStatus('pending')->limit(10)->get() as $notification) {
            if($notification->type=='push'){
                $firebase->send_notification($notification->id);
            }else{
                $payload = json_decode($notification->payload);
                if(!empty($payload->redirect)){
                    $url = url($payload->redirect);
                    // force string on url -
                    $url = str_replace('allsunday','featour',$url);
                    $url = str_replace('api.featour','backoffice.featour',$url);
                }else{
                    $url = url('dashboard');
                }
                $message = $notification->message;
                $content = array(
                "en" => $message
                );
                
                $fields = array(
                'app_id' => "882b1a7e-1db7-4036-987e-2f2f04b017c8",
                'included_segments' => array('All'),
                'data' => array('body' => $message),
                'contents' => $content,
                'url' => $url
                );
                
                $fields = json_encode($fields);
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Basic ZjYwNGZjNzEtMmQ0MC00M2FhLWE0M2YtNWE5Mjg1NjhlM2Vm'
                ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                
                $response = curl_exec($ch);
                curl_close($ch);
                $input['status'] = 'sent';
                Notification::find($notification->id)->update($input);
            }
        }
        return 0;
    }
}
