<?php

namespace App\Console\Commands;

use App\Models\Booking;
use App\Models\BookingDriver;
use App\Models\Notification;
use App\Models\Mutation;
use App\Models\Wallet;
use App\Models\Report;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BookingExpirationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'book:expiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    
    public function handle()
    {
        // date_default_timezone_set('Asia/Jakarta');
        foreach (Booking::where('created_at', '<', date('Y-m-d H:i:s', strtotime('-10minutes')))->whereType('SELF DRIVE')->wherePaymentStatus('unpaid')->get() as $book) {
            $book->update(['payment_status' => 'expired', 'status' => 'cancel']);
            Notification::create([
                'user_id' => $book->user_id,
                'title' => "Your booking " . $book->booking_id . " has been expired",
                'message' => 'Your booking has been expired due no payment have been made within 10 MINUTES',
                'payload' => json_encode(["booking_id" => $book->booking_id]),
            ]);
        }

        $dateTime = date('Y-m-d H:i:s');
        foreach (Booking::where('created_at', '<', date('Y-m-d H:i:s', strtotime('-1hours')))->whereReminderSent(false)->wherePaymentStatus('unpaid')->get() as $book) {
            $book->update(['reminder_sent' => 1]);
            Notification::create([
                'user_id' => $book->user_id,
                'title' => "Your booking " . $book->booking_id . " will expire in next 1 hour",
                'message' => 'Please make payment immediately for your booking within the next hour',
                'payload' => json_encode(["booking_id" => $book->booking_id]),
            ]);
        }
        foreach (Booking::where('created_at', '<', date('Y-m-d H:i:s', strtotime('-2hours')))->wherePaymentStatus('unpaid')->get() as $book) {
            $book->update(['payment_status' => 'expired', 'status' => 'cancel']);
            Notification::create([
                'user_id' => $book->user_id,
                'title' => "Your booking " . $book->booking_id . " has been expired",
                'message' => 'Your booking has been expired due no payment have been made within two hours',
                'payload' => json_encode(["booking_id" => $book->booking_id]),
            ]);
        }
        foreach (BookingDriver::with('driver')->where('deadline_confirmation', '<', $dateTime)->whereStatus('pending')->get() as $bookDriver) {
            $bookDriver->update(['status' => 'expired']);
            $title =  "Booking ID " . $bookDriver->booking_id . " has been expired";
            $message =  'This booking has been expired due no confirmation have been made within two minutes.';
            Notification::create([
                'user_id' => $bookDriver->driver->user_id,
                'title' => $title,
                'message' => $message,
                'payload' => json_encode(["booking_id" => $bookDriver->booking_id, 'date' => $dateTime]),
            ]);
            Booking::whereBooking_id($bookDriver->booking_id)->update(['driver_id' => NULL, 'status' => 'pending']);
        }
        
        // Reminder -1 days
        $status=["waiting pickup"];
        $booking = Booking::with('driver')->whereIn('status', $status)->whereBetween(DB::raw('CONCAT(pickup_date,  " ",pickup_time)'), [date('Y-m-d H:i:s', strtotime('+24hours')),date('Y-m-d H:i:s', strtotime('+25hours'))])->get();
        foreach ($booking as $key) {
            $additional_data= json_decode($key->additional_data,TRUE);
            if(empty($additional_data['reminder_notif'])){
                Notification::create([
                    'user_id' => $key->driver->user_id,
                    'title' => 'Booking Update',
                    'message' => 'A gentle reminder that you have a reservation for tomorrow',
                    'payload' => json_encode(["booking_id" => $key->booking_id]),
                ]);
                
                Notification::create([
                    'user_id' => $key->user_id,
                    'title' => 'Job Order Update',
                    'message' => 'A gentle reminder that you have a job order for tomorrow',
                    'payload' => json_encode(["booking_id" => $key->booking_id]),
                ]);

                $additional_data['reminder_notif'] =1;
                $form['additional_data'] =  json_encode($additional_data);
                Booking::whereBooking_id($key->booking_id)->update($form);
            }
        }
        // End Reminder -2 days
        

          


          
        // Reminder Hari H-4hours Pickup
        $status=["waiting pickup"];
        $booking = Booking::with('vendor')->whereType('SELF DRIVE')->whereIn('status', $status)->whereBetween(DB::raw('CONCAT(pickup_date,  " ",pickup_time)'), [date('Y-m-d H:i:s', strtotime('+4hours')),date('Y-m-d H:i:s', strtotime('+250minutes'))])->get();
        foreach ($booking as $key) {
            $additional_data= json_decode($key->additional_data,TRUE);
            if(empty($additional_data['reminder_notif_4hours'])){
                if($key->type=='SELF DRIVE'){ 
                    Notification::create([
                        'user_id' => $key->vendor->user_id,
                        'title' => 'Booking Update',
                        'message' => 'A gentle reminder that you have a reservation for 4 hours, Please cleaning your car',
                        'payload' => json_encode(["booking_id" => $key->booking_id,'category'=> 'cleaning car']),
                    ]);
                }
                $additional_data['reminder_notif_4hours'] =1;
                $form['additional_data'] =  json_encode($additional_data);
                Booking::whereBooking_id($key->booking_id)->update($form);
            }
        }
        // End Reminder Hari H Pickup





        // Reminder -2 hours
        $status=["waiting pickup","awaiting departure time"];
        $booking = Booking::with('driver')->with('vendor')->whereIn('status', $status)->whereBetween(DB::raw('CONCAT(pickup_date,  " ",pickup_time)'), [date('Y-m-d H:i:s', strtotime('+2hours')),date('Y-m-d H:i:s', strtotime('+130minutes'))])->get();
        foreach ($booking as $key) {
            $additional_data= json_decode($key->additional_data,TRUE);
            if(empty($additional_data['reminder_notif_2hours'])){
                if($key->type=='SELF DRIVE'){ 
                    Notification::create([
                        'user_id' => $key->vendor->user_id,
                        'title' => 'Booking Update',
                        'message' => 'A gentle reminder that you have a reservation for 2 hours, please dropp off your car to the customer ',
                        'payload' => json_encode(["booking_id" => $key->booking_id,'category'=> 'uploadPhoto']),
                    ]);
    
                    Notification::create([
                        'user_id' => $key->user_id,
                        'title' => 'Job Order Update',
                        'message' => 'A gentle reminder that you have a job order for 2 hours',
                        'payload' => json_encode(["booking_id" => $key->booking_id]),
                    ]);
                }else{
                    Notification::create([
                        'user_id' => $key->driver->user_id,
                        'title' => 'Booking Update',
                        'message' => 'A gentle reminder that you have a reservation for 2 hours',
                        'payload' => json_encode(["booking_id" => $key->booking_id]),
                    ]);
    
                    Notification::create([
                        'user_id' => $key->user_id,
                        'title' => 'Job Order Update',
                        'message' => 'A gentle reminder that you have a job order for 2 hours',
                        'payload' => json_encode(["booking_id" => $key->booking_id]),
                    ]);
                }
              

                $additional_data['reminder_notif_2hours'] =1;
                $form['additional_data'] =  json_encode($additional_data);
                if($key->status=='awaiting departure time'){
                    $form['status'] = 'waiting pickup';
                }
                Booking::whereBooking_id($key->booking_id)->update($form);
                if($key->type!='SELF DRIVE'){
                    $booking_driver = BookingDriver::whereBooking_id($key->booking_id)->whereDriver_id($key->driver_id)->whereStatus('upcoming')->first();
                    $booking_driver->update(['status' => 'confirm']);
                }

            }
        }
        // End Reminder -2 days





          
        // Reminder Hari H Pickup
        $status=["waiting pickup"];
        $booking = Booking::with('vendor')->whereType('SELF DRIVE')->whereIn('status', $status)->whereBetween(DB::raw('CONCAT(pickup_date,  " ",pickup_time)'), [date('Y-m-d H:i:s', strtotime('+1minutes')),date('Y-m-d H:i:s', strtotime('+7minutes'))])->get();
        foreach ($booking as $key) {
            $additional_data= json_decode($key->additional_data,TRUE);
            if(empty($additional_data['reminder_notif_pickup'])){
                if($key->type=='SELF DRIVE'){ 
                    Notification::create([
                        'user_id' => $key->vendor->user_id,
                        'title' => 'Booking Update',
                        'message' => 'a Gentle reminder that you have a reservation for now, please upload your car condition ',
                        'payload' => json_encode(["booking_id" => $key->booking_id,'category'=> 'uploadPhoto']),
                    ]);
                }
                $additional_data['reminder_notif_pickup'] =1;
                $form['additional_data'] =  json_encode($additional_data);
                Booking::whereBooking_id($key->booking_id)->update($form);
            }
        }
        // End Reminder Hari H Pickup




          
        // Send balance if don have report
        // $status=["completed"];
        // $booking = Booking::with('driver')->whereIn('status', $status)->whereBetween(DB::raw('CONCAT(pickup_date,  " ",pickup_time)'), [date('Y-m-d H:i:s', strtotime('-26hours')),date('Y-m-d H:i:s', strtotime('-25hours'))])->get();
        // foreach ($booking as $key) {
        //     $checkReport =  Report::whereBooking_id($key->booking_id)->first();
        //     $remark = 'Fee transaction '.$key->booking_id;
        //     $mutation = Mutation::whereRemark($remark)->whereStatus('pending')->first();
        //     if(empty($checkReport)){
        //         if($mutation){
        //             $updateMutation = Mutation::find($mutation->mutation_id)->update(['status' => 'completed']);
        //             $wallet = Wallet::findOrFail($mutation->wallet_id);
        //             $mutations = Mutation::whereWalletId($wallet->wallet_id)->get();
        //             $completedMutations = Mutation::whereWalletId($wallet->wallet_id)->whereStatus('completed')->get();
        //             $wallet->fill([
        //                 'balance' => $mutations->sum('amount_in') - $mutations->sum('amount_out'),
        //                 'balance_withdraw' =>  $completedMutations->sum('amount_in') - $completedMutations->sum('amount_out')
        //             ]);  
        //             $wallet->save();
        //             Notification::create([
        //                 'user_id' => $wallet->user_id,
        //                 'title' => "Featour e-Wallet",
        //                 // 'title' => ($mutation->amount_in ?? 0) > 0 ? "Your wallet balance increases" : "Your wallet balance is reduced",
        //                 'message' => sprintf('(%s%s) %s', ($mutation->amount_in ?? 0) > 0 ? '+' : '-', number_format(($mutation->amount_in ?? 0) > 0 ? $mutation->amount_in : $mutation->amount_out), $mutation->remark),
        //                 'payload' => json_encode(["mutation_id" => $mutation->mutation_id]),
        //             ]);
        //             Artisan::call('push:sender');
        //         }
        //     }
        // }
        // End Send balance if don have report
        
        return 0;
    }
}
