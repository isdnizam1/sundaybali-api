<?php

namespace App\Console\Commands;

use App\Models\Mutation;
use Illuminate\Console\Command;

class TestMutationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:mutation {type} {wallet_id} {amount_in} {amount_out} {remark}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Mutation::create([
            'type' => $this->argument('type'),
            'wallet_id' => $this->argument('wallet_id'),
            'amount_in' => $this->argument('amount_in'),
            'amount_out' => $this->argument('amount_out'),
            'remark' => $this->argument('remark')
        ]);
        return 0;
    }
}
