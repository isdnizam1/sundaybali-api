<?php

namespace App\Console\Commands;

use App\Models\Booking;
use App\Models\BookingDriver;
use Illuminate\Console\Command;

class UpcomingBookingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'book:upcoming';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (BookingDriver::whereStatus('upcoming')->where('pickup_time', '<', date('Y-m-d H:i:s', strtotime('1hour')))->get() as $bookingDriver) {
            $bookingDriver->fill(['status' => 'confirm']);
            $bookingDriver->save();
        }
        return 0;
    }
}
