<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Booking;
use App\Models\BookingStatusLog;
use App\Models\Mutation;
use App\Models\Wallet;
use App\Models\Report;
use App\Models\Driver;
use App\Models\Vendor;
use Carbon\Carbon;

class BookingCompletionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'book:completion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Auto complete is working fine!");

        $booking = Booking::where('status', 'picked up')->get();
        $now = Carbon::now();
        $formatted_now = $now->toDateString(). " ". $now->toTimeString();
        foreach ($booking as $key) {
            $additional_data= json_decode($key->additional_data,TRUE);

            if (!array_key_exists('return_location', $additional_data)) continue;
            if (!array_key_exists('date', $additional_data['return_location'])) continue;

            if ($formatted_now >= ($additional_data['return_location']['date'] . " " . $additional_data['return_location']['time'])) {
                $key->update([
                    'status' => 'completed',
                ]);
                BookingStatusLog::create([
                    'booking_id' => $key->booking_id,
                    'status' => 'completed',
                    'description' => $key->booking_status,
                    'notes' => "Completed automatically by the system"
                ]);

                // Add Mutation Wallet
                if($key->type=='SELF DRIVE'){
                    $wallet_id = Wallet::whereUserId(Vendor::find($key->vendor_id)->user_id)->first()->wallet_id;
                    $amount = $key->subtotal-$key->platform_fee;
                }else{
                    $wallet_id = Wallet::whereUserId(Driver::find($key->driver_id)->user_id)->first()->wallet_id;
                    $amount = $key->driver_fee;
                }
                Mutation::create([
                    'type' => 'order',
                    'wallet_id' => $wallet_id,
                    'amount_in' => $amount,
                    'status' => 'pending',
                    'amount_out' => 0,
                    'remark' => 'Fee transaction ' . $key->booking_id,
                ]);
            }
        }

        // Send balance if don have report
        \Log::info("Wallet sync is working fine!");
        $status=["completed"];
        $booking = Booking::with('driver')->whereIn('status', $status)->whereBetween('updated_at', [date('Y-m-d H:i:s', strtotime('-25hours')),date('Y-m-d H:i:s', strtotime('-24hours'))])->get();
        foreach ($booking as $key) {
            $checkReport =  Report::whereBooking_id($key->booking_id)->first();
            $remark = 'Fee transaction '.$key->booking_id;
            $mutation = Mutation::whereRemark($remark)->whereStatus('pending')->first();
            if(empty($checkReport)){
                if($mutation){
                    $updateMutation = Mutation::find($mutation->mutation_id)->update(['status' => 'completed']);
                    $wallet = Wallet::findOrFail($mutation->wallet_id);
                    $mutations = Mutation::whereWalletId($wallet->wallet_id)->whereStatus('pending')->get();
                    $completedMutations = Mutation::whereWalletId($wallet->wallet_id)->whereStatus('completed')->get();
                    $wallet->fill([
                        'balance' => $mutations->sum('amount_in') - $mutations->sum('amount_out'),
                        'balance_withdraw' =>  $completedMutations->sum('amount_in') - $completedMutations->sum('amount_out')
                    ]);  
                    $wallet->save();
                    // Notification::create([
                    //     'user_id' => $wallet->user_id,
                    //     'title' => "Featour e-Wallet",
                    //     // 'title' => ($mutation->amount_in ?? 0) > 0 ? "Your wallet balance increases" : "Your wallet balance is reduced",
                    //     'message' => sprintf('(%s%s) %s', ($mutation->amount_in ?? 0) > 0 ? '+' : '-', number_format(($mutation->amount_in ?? 0) > 0 ? $mutation->amount_in : $mutation->amount_out), $mutation->remark),
                    //     'payload' => json_encode(["mutation_id" => $mutation->mutation_id]),
                    // ]);
                    // Artisan::call('push:sender');
                }
            }
        }
        // End Send balance if don have report

        return 0;
    }
}
