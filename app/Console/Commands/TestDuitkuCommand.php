<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Royryando\Duitku\Facades\Duitku;

class TestDuitkuCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:duitku';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $payment_method = Duitku::paymentMethods(100000);
        $inquiry = Duitku::createInvoice('ORDER_ID', 100000, 'M1', 'Product Name', 'John Doe', 'john@example.com', 120);
        print_r(compact('payment_method', 'inquiry'));
        return 0;
    }
}
