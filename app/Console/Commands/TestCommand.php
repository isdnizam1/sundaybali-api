<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Booking;
use App\Models\Mutation;
use App\Models\Wallet;
use App\Models\Report;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Test Command for Sync Wallet, just once
        $status=["completed"];
        $booking = Booking::with('driver')->whereIn('status', $status)->where('updated_at', '<', date('Y-m-d H:i:s', strtotime('-24hours')))->get();
        \Log::info(count($booking));
        foreach ($booking as $key) {
            $checkReport =  Report::whereBooking_id($key->booking_id)->first();
            $remark = 'Fee transaction '.$key->booking_id;
            $mutation = Mutation::whereRemark($remark)->whereStatus('pending')->first();
            if(empty($checkReport)){
                if($mutation){
                    $updateMutation = Mutation::find($mutation->mutation_id)->update(['status' => 'completed']);
                    $wallet = Wallet::findOrFail($mutation->wallet_id);
                    $mutations = Mutation::whereWalletId($wallet->wallet_id)->whereStatus('pending')->get();
                    $completedMutations = Mutation::whereWalletId($wallet->wallet_id)->whereStatus('completed')->get();
                    $wallet->fill([
                        'balance' => $mutations->sum('amount_in') - $mutations->sum('amount_out'),
                        'balance_withdraw' =>  $completedMutations->sum('amount_in') - $completedMutations->sum('amount_out')
                    ]);  
                    $wallet->save();
                }
            }
        }
        \Log::info("Test Command is working fine!");

        return 0;
    }
}
