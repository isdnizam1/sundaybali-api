<?php

namespace App\Libs;

use Illuminate\Support\Facades\Http;

class Distance
{
  protected $gmapApiKey = "AIzaSyAG34SjeOwP20uH3G-h593VhjtAP4-PXtk";

  public function getDistance(string $origin, string $destination)
  {
    $request = Http::get('https://maps.googleapis.com/maps/api/distancematrix/json', [
      'origins' => $origin,
      'destinations' => $destination,
      'mode' => 'driving',
      'language' => 'en-US',
      'key' => $this->gmapApiKey
    ]);
    return $request->json()['rows'][0]['elements'][0];
  }
}
