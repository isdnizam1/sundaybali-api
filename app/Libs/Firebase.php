<?php

namespace App\Libs;

use App\Models\Notification;
use Firebase\Auth\Token\Exception\InvalidToken;
use Illuminate\Support\Facades\Storage;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\RawMessageFromArray;

class Firebase
{
  public $factory;

  function __construct()
  {
    $this->factory = (new Factory)->withServiceAccount(base_path('sunday-c3e2c-92873788ad67.json')); // filenya nanti aku kasih terpisah mas
  }

  public function verifyIdToken(string $idTokenString)
  {
    $auth = $this->factory->createAuth();
    $uid = null;
    $success = false;
    try {
      $verifiedIdToken = $auth->verifyIdToken($idTokenString);
      $uid = $verifiedIdToken->claims()->get('sub');
      $success = true;
    } catch (InvalidToken $e) {
      $message = 'The token is invalid: ' . $e->getMessage();
    } catch (\InvalidArgumentException $e) {
      $message =  'The token could not be parsed: ' . $e->getMessage();
    }

    if($success){
      return   $auth->getUser($uid);
    }else{
      return   $message;
    }
  }

  public function sign(string $email, string $pass)
  {
    $auth = $this->factory->createAuth();
    $uid = null;
    $data['status'] = 'failed';
    try {
      // $ccc =$auth->createUserWithEmailAndPassword($email, $pass);

      $ccc =$auth->signInWithEmailAndPassword($email, $pass);
      $data['status'] = 'success';
    } catch (InvalidToken $e) {
      $message = 'The token is invalid: ' . $e->getMessage();
      $data['message'] = $e->getMessage();
    } catch (\InvalidArgumentException $e) {
      $message =  'The token could not be parsed: ' . $e->getMessage();
      $data['message'] = $e->getMessage();
    } catch (\Kreait\Firebase\Exception\Auth\InvalidPassword $e) {
      $data['message'] = 'Email and password doesnt match';
    } catch (\Kreait\Firebase\Exception\InvalidArgumentException $e) {
        $data['message'] = $e->getMessage();
    } catch (\Kreait\Firebase\Auth\SignIn\FailedToSignIn $e) {
        $data['message'] = $e->getMessage();
    } 
    return   $data;
  }


  public function signToken(string $email)
  {
    $auth = $this->factory->createAuth();
    $uid = null;
    $success = false;
    try {
      $ccc =$auth->signInAsUser($email);
      // $ccc =$auth->signInWithEmailAndPassword($email, $pass);
      $success = true;
      // $uid = $ccc->getClaim('sub');
    } catch (InvalidToken $e) {
      $message = 'The token is invalid: ' . $e->getMessage();
    } catch (\InvalidArgumentException $e) {
      $message =  'The token could not be parsed: ' . $e->getMessage();
    }

    if($success){
      $data['uid']= $ccc;
      $data['auth']= $auth;
      return   $data;
    }else{
      return   $message;
    }
  }



  public function signUp(string $email)
  {
    $auth = $this->factory->createAuth();
    $uid = null;
    $success = false;
    try {
      $customToken = $auth->createCustomToken($email);
      $success = true;
      // $uid = $ccc->getClaim('sub');
    } catch (InvalidToken $e) {
      $message = 'The token is invalid: ' . $e->getMessage();
    } catch (\InvalidArgumentException $e) {
      $message =  'The token could not be parsed: ' . $e->getMessage();
    }

    if($success){
      $data['uid']= $customToken->toString();
      return   $data;
    }else{
      return   $message;
    }
  }
  public function send_notification(int $notification_id)
  {
    $messaging = $this->factory->createMessaging();
    $n = Notification::with('user')->findOrFail($notification_id);
    $n->update(['status' => 'sent']);
    $notification = [
      'title' => $n->title,
      'body' => $n->message,
      'image' => 'https://firebasestorage.googleapis.com/v0/b/all-sunday.appspot.com/o/featours-small.png?alt=media',
    ];
    $data = @json_decode($n->payload, true) ?? [];
    foreach ($data as $key => $value) {
      $data[$key] = strval($value);
    }
    $message = new RawMessageFromArray([
      'data' => $data,
      'notification' => $notification,
      'android' => [
        // https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#androidconfig
        'ttl' => '3600s',
        'priority' => 'high',
        'notification' => $notification,
      ]
    ]);
    foreach ($n->user->firebase_tokens as $fb_token) {
      $sendReport = $messaging->sendMulticast($message, [$fb_token->token]);
    }
    // ob_start();
    // print_r($sendReport);
    // Storage::put('log/push-sender.log', ob_get_clean());
  }
}
