<?php

namespace App\Libs;

use App\Models\Booking;
use App\Models\User;

class Slack
{

    public static function bookUpdate(string $booking_id)
    {
        ob_start();
        $book = Booking::with('user')->findOrFail($booking_id);
        if(!empty($book->arr_data['additional_data']['flight_number'])){
            $fligh_number = $book->arr_data['additional_data']['flight_number'];
        }else{
            $fligh_number = '-';
        }
        $text = sprintf(
            "Booking Type: *%s*\nBooking ID: *%s*\nCustomer: *%s*\nPax: *%s*\nPickup Time: *%s*\nPickup Location: *%s*\nFlight Number: *%s*\nGuide Language: *%s*\n%s: *%s*\nSubtotal: *%s*\nPayment Status: *%s*",
            $book->type,
            $book->booking_id,
            $book->user->name ?? ($book->user->first_name . ' ' . $book->user->last_name),
            $book->pax_info,
            date('M d, Y H:i', strtotime(implode(' ', [$book->pickup_date, $book->pickup_time]))),
            $book->arr_data['additional_data']['pickup_location']['address'],
            $fligh_number,
            isset($book->arr_data['additional_data']['preferred_language']) ? $book->arr_data['additional_data']['preferred_language'] : '',
            $book->type == 'CAR CHARTER' ? 'Duration' : 'Destination',
            $book->type == 'CAR CHARTER' ? $book->duration : isset($book->arr_data['additional_data']['destination']) ? $book->arr_data['additional_data']['destination']['address'] : '',
            sprintf('Rp %s', number_format($book->subtotal)),
            strtoupper($book->payment_status)
        );
        $ch = curl_init("https://hooks.slack.com/services/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");
        $payload = json_encode(compact('text'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_exec($ch);
        curl_close($ch);
        ob_clean();
    }
}
