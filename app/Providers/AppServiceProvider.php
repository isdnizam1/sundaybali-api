<?php

namespace App\Providers;

use App\Models\Booking;
use App\Models\BookingDriver;
use App\Models\FirebaseToken;
use App\Models\Mutation;
use App\Models\Notification;
use App\Models\PositionLog;
use App\Observers\BookingObserver;
use App\Models\User;
use App\Observers\BookingDriverObserver;
use App\Observers\UserObserver;
use App\Observers\FirebaseTokenObserver;
use App\Observers\MutationObserver;
use App\Observers\NotificationObserver;
use Illuminate\Support\ServiceProvider;
use App\Models\Withdrawal;
use App\Observers\PositionLogObserver;
use App\Observers\WithdrawalObserver;
use Illuminate\Support\Facades\Schema; 


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Booking::observe(BookingObserver::class);
        FirebaseToken::observe(FirebaseTokenObserver::class);
        Notification::observe(NotificationObserver::class);
        User::observe(UserObserver::class);
        Mutation::observe(MutationObserver::class);
        BookingDriver::observe(BookingDriverObserver::class);
        Withdrawal::observe(WithdrawalObserver::class);
        PositionLog::observe(PositionLogObserver::class);
        Schema::defaultStringLength(191);
    }
}
