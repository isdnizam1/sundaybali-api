<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;
    protected $primaryKey = 'document_id';
    protected $guarded = ['document_id'];
    protected $table = 'module_document';
    public $incrementing = false;

}
