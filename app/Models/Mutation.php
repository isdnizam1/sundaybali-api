<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mutation extends Model
{
    use HasFactory;
    protected $primaryKey = 'mutation_id';
    protected $guarded = ['mutation_id'];
    protected $table = 'module_mutations';
    public $appends = ['created_time'];

    public function getCreatedTimeAttribute()
    {
        return str_replace(
            sprintf(' %s', date('Y')),
            '',
            date('M d, Y H:i', strtotime($this->created_at ?? 'now'))
        );
    }
}
