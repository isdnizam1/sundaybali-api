<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected $primaryKey = 'booking_id';
    protected $guarded = [];
    protected $table = 'module_booking';
    public $incrementing = false;
    public $appends = ['arr_data', 'pax_info', 'booking_status', 'name'];
    public $keyType = 'string';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }


    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id', 'car_id')->with('vehicle');
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class, 'driver_id', 'driver_id')->with('user', 'vehicle');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'vendor_id');
    }

    public function images()
    {
        return $this->hasMany(BookingImage::class, 'booking_id', 'booking_id');
    }

    public function status_logs()
    {
        return $this->hasMany(BookingStatusLog::class, 'booking_id', 'booking_id')->orderByDesc('created_at');
    }

    public function picked_up_images()
    {
        return $this->images()->where('type','=', 'PICKED_UP');
    }

    public function completed_images()
    {
        return $this->images()->where('type','=', 'COMPLETED');
    }

    public function getArrDataAttribute()
    {
        if ($this->additional_data || $this->payment_data) {
            return [
                'additional_data' => @json_decode($this->additional_data, true),
                'payment_data' => @json_decode($this->payment_data, true),
            ];
        }
    }

    public function getNameAttribute()
    {
        return implode(' ', [$this->first_name, $this->last_name]);
    }

    public function getPaxInfoAttribute()
    {
        if ($this->adult) {
            $result = ($this->adult + $this->child) . " Pax";
            $qty = [];
            if ($this->adult > 0) $qty[] = "{$this->adult} Adults";
            if ($this->child > 0) $qty[] = "{$this->child} Childs";
            if ($this->infant > 0) $qty[] = "{$this->infant} Infants";
            $result = sprintf('%s (%s)', $result, trim(implode(' - ', $qty)));
            return $result;
        }
    }


    
    public function getBookingStatusAttribute()
    {
        $result = "";
        if ($this->status) {
            $result = [
                'pending' => 'Waiting for payment',
                'awaiting departure time' => 'Awaiting departure time',
                'waiting driver' => 'Searching for a guide',
                'waiting pickup' => 'Waiting for pickup',
                'picked up' => 'Guide met customer',
                'cancel' => 'Canceled',
                'completed' => 'Completed'
            ][$this->status];
        }
        if ($this->status == 'pending' && $this->payment_status == 'paid') $result = "Searching for a guide";
        return $result;
    }
    public function report()
    {
        return $this->hasOne(Report::class, 'booking_id', 'booking_id');
    }
}
