<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{

    use HasFactory;
    protected $primaryKey = 'driver_id';
    protected $guarded = ['driver_id'];
    protected $table = 'module_driver';

    public function user()
    {
        return $this->hasOne(User::class, 'user_id', 'user_id');
    }

    public function last_location()
    {
        return $this->hasOne(PositionLog::class, 'id', 'last_position_id');
    }

    public function vehicle()
    {
        return $this->hasOne(Vehicle::class, 'vehicle_id', 'vehicle_id');
    }

    public function pending_booking()
    {
        return $this->hasOne(BookingDriver::class, 'driver_id', 'driver_id')->with('booking')->whereStatus('pending');
    }

    public function active_booking()
    {
        return $this->hasOne(BookingDriver::class, 'driver_id', 'driver_id')->with('booking', 'active_booking')->has('active_booking')->whereStatus('confirm');
    }
}
