<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'user_id';
    protected $guarded = ['user_id'];
    protected $table = 'system_users';
    public $appends = ['full_name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function driver()
    {
        return $this->hasOne(Driver::class, 'user_id', 'user_id')->with('pending_booking', 'active_booking', 'vehicle', 'last_location');
        // return $this->hasOne(Driver::class, 'user_id', 'user_id')->with('vehicle');
    }


    public function vendor()
    {
        return $this->hasOne(Vendor::class, 'user_id', 'user_id');
    }

    public function bankAccount()
    {
        return $this->hasOne(BankAccount::class, 'user_id', 'user_id')->with('bank');
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class, 'user_id', 'user_id')->with('mutation');
    }

    public function wallet_only()
    {
        return $this->hasOne(Wallet::class, 'user_id', 'user_id');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class, 'user_id', 'user_id')->orderBy('created_at', 'desc');
    }

    public function active_bookings()
    {
        return $this->hasMany(Booking::class, 'user_id', 'user_id')->whereIn('payment_status', ['unpaid', 'paid'])->whereIn('status', ['pending','awaiting departure time','waiting driver', 'waiting pickup','picked up'])->whereDate('pickup_date', date('Y-m-d'))->orderBy('created_at', 'desc');;
    }

    public function getFullNameAttribute()
    {
        return implode(" ", [$this->first_name, $this->last_name]);
    }

    public function firebase_tokens()
    {
        return $this->hasMany(FirebaseToken::class, 'user_id', 'user_id');
    }
}
