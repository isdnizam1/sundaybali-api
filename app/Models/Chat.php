<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;
    protected $primaryKey = 'chat_id';
    protected $guarded = ['chat_id'];
    protected $table = 'module_chat';
    public function sender()
    {
        return $this->hasOne(User::class, 'user_id', 'sender_id');
    }
    public function participant()
    {
        return $this->hasOne(User::class, 'user_id', 'participant_id');
    }
}
