<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;
    protected $primaryKey = 'bank_id';
    protected $guarded = ['bank_id'];
    protected $table = 'module_banks';

}
