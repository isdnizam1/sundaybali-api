<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Bank;

class BankAccount extends Model
{
    use HasFactory;
    protected $primaryKey = 'bank_account_id';
    protected $guarded = ['bank_account_id'];
    protected $table = 'module_bank_account';

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'bank_id');
    }
}
