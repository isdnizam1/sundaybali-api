<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PositionLog extends Model
{
    use HasFactory;

    protected $table = 'module_position_logs';
    protected $guarded = [];
}
