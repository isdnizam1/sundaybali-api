<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingImage extends Model
{
    use HasFactory;
    protected $table = 'module_booking_images';
    protected $fillable = ['image', 'booking_id', 'type'];
}
