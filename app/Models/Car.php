<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    protected $primaryKey = 'car_id';
    protected $guarded = ['car_id'];
    protected $table = 'module_cars';
    public function vehicle()
    {
        return $this->hasOne(Vehicle::class, 'vehicle_id', 'vehicle_id')->with('vehicleType');
    }
    public function vendor()
    {
        return $this->hasOne(Vendor::class, 'vendor_id', 'vendor_id');
    }
}
