<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    use HasFactory;
    protected $primaryKey = 'review_id';
    protected $guarded = ['review_id'];
    protected $table = 'module_reviews';

    public function user()
    {
        return $this->hasOne(User::class, 'user_id', 'user_id');
    }
}
