<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    use HasFactory;
    protected $primaryKey = 'promotion_id';
    protected $guarded = ['promotion_id'];
    protected $table = 'module_promotion';

}
