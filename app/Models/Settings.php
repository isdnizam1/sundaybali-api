<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use HasFactory;
    protected $primaryKey = 'setting_id';
    protected $guarded = ['setting_id'];
    protected $table = 'system_settings';

    public static function getValueByName(string $setting_name)
    {
        $setting = Settings::where(compact('setting_name'))->first();
        if ($json = json_decode($setting->value, true)) {
            $setting->value = $json;
        }
        return $setting->value;
    }
}
