<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarImage extends Model
{
    use HasFactory;
    protected $primaryKey = 'car_image_id';
    protected $guarded = ['car_image_id'];
    protected $table = 'module_cars_image';
    public function car()
    {
        return $this->hasOne(Car::class, 'car_id', 'car_id');
    }
}
