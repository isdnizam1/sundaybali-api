<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;
    protected $primaryKey = 'report_id';
    protected $guarded = ['report_id'];
    protected $table = 'module_report';
    public $timestamps = false;
    public function booking()
    {
        return $this->hasOne(Booking::class, 'booking_id', 'booking_id')->with('user');
    }
}
