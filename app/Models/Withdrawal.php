<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    use HasFactory;
    protected $primaryKey = 'withdrawal_id';
    protected $guarded = ['withdrawal_id'];
    protected $table = 'module_withdrawal';
    public $appends = ['created_time', 'formated_status'];

    public function getCreatedTimeAttribute()
    {
        return str_replace(
            sprintf(' %s', date('Y')),
            '',
            date('M d, Y H:i', strtotime($this->created_at ?? 'now'))
        );
    }

    public function getFormatedStatusAttribute()
    {
        if ($this->status) return [
            'pending' => 'Being processed',
            'paid' => 'Already paid (transferred)',
            'decline' => 'Declined, balance returned'
        ][$this->status];
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class, 'wallet_id', 'wallet_id')->with('user');
    }
}
