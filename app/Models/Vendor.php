<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;
    protected $primaryKey = 'vendor_id';
    protected $guarded = ['vendor_id'];
    protected $table = 'module_vendor';
    public $appends = ['arr_data'];
    public function user()
    {
        return $this->hasOne(User::class, 'user_id','user_id')->with('bankAccount');
    }

    public function getArrDataAttribute()
    {
        if ($this->additional_data) {
            return [
                'additional_data' => @json_decode($this->additional_data, true),
            ];
        }
    }
}
