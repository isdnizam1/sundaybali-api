<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    use HasFactory;
    protected $primaryKey = 'vehicle_type_id';
    protected $guarded = ['vehicle_type_id'];
    protected $table = 'module_vehicle_type';

}
