<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingDriver extends Model
{
    use HasFactory;
    protected $primaryKey = 'booking_driver_id';
    protected $guarded = [];
    protected $table = 'module_booking_driver';

    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'booking_id')->with('user');
    }

    public function active_booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'booking_id')->whereIn('status', ['awaiting departure time','waiting driver', 'waiting pickup','picked up']);
    }

    public function driver()
    {
        return $this->hasOne(Driver::class, 'driver_id', 'driver_id');
    }
}
