<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attraction extends Model
{
    use HasFactory;
    protected $primaryKey = 'attraction_id';
    protected $guarded = ['attraction_id'];
    protected $table = 'module_attraction';

}
