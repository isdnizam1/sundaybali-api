<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    use HasFactory;
    protected $primaryKey = 'wallet_id';
    protected $guarded = ['wallet_id'];
    protected $table = 'module_wallets';
    public function mutation()
    {
        return $this->hasMany(Mutation::class, 'wallet_id','wallet_id');
    }
    public function user()
    {
        return $this->hasOne(User::class, 'user_id','user_id');
    }
}
