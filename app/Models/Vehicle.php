<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;
    protected $primaryKey = 'vehicle_id';
    protected $guarded = ['vehicle_id'];
    protected $table = 'module_vehicle';

    public function vehicleType()
    {
        return $this->hasOne(VehicleType::class, 'vehicle_type_id', 'vehicle_type_id');
    }

}
