<?php

namespace App\Traits;


use \DateTime;
use App\Models\User;
use App\Models\Notif;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Api Responser Trait
|--------------------------------------------------------------------------
|
| This trait will be used for any response we sent to clients.
|
*/

trait GlobalFunction
{
   /**
    * Return a success JSON response.
    *
    * @param  array|string  $data
    * @param  string  $message
    * @param  int|null  $code
    * @return \Illuminate\Http\JsonResponse
    */
   protected function success($data,  int $code = 200)
   {
      return response()->json([
         'code' => 200,
         'status' => 'success',
         'result' => $data
      ], $code);
   }

   /**
    * Return an error JSON response.
    *
    * @param  string  $message
    * @param  int  $code
    * @param  array|string|null  $data
    * @return \Illuminate\Http\JsonResponse
    */
   protected function error(string $message = null, int $code = 200)
   {
      return response()->json([
         'code' => 500,
         'status' => 'failed',
         'message' => $message
      ], $code);
   }

   protected  function isJson(string $string)
   {
      json_decode($string);
      return (json_last_error() == JSON_ERROR_NONE);
   }

  
   protected function add_date_days($date, $days)
   {
      return date("Y-m-d", strtotime(date("Y-m-d", strtotime($date)) . " " . $days));
   }

   protected function uploadImage($imageBase64, $path, $file_name)
   {
      $replace_string =  array("data:application/octet-stream;", "data:image/png;", 'data:image/jpeg;', 'data:image/jpg;', 'base64,');
      $img = str_replace($replace_string, '', $imageBase64);
      $img = str_replace(' ', '+', $img);
      $typeImage = substr($img, 0, 1);
      if ($typeImage == 'i') {
         $typeImage = '.png';
      } else {
         $typeImage = '.jpg';
      }
      $data2 = base64_decode($img);
      $upload_directory = dirname(__FILE__) . $path;
      $upload_directory = str_replace('app/Traits', '', $upload_directory);
      $im = imageCreateFromString($data2);
      $imageUploaded = imagejpeg($im, $upload_directory . '' . $file_name . '' . $typeImage, 75);
      imagedestroy($im);
      if ($imageUploaded) {
         return $file_name . '' . $typeImage;
      } else {
         return false;
      }
   }

   protected function get_between_date($start_date = false, $end_date, $type = false)
   {
      if (!$start_date) {
         $start_date = date("Y/m/d");
      }
      $start_date = str_replace('-', '/', $start_date);
      $end_date = str_replace('-', '/', $end_date);
      $datetime1 = new DateTime($start_date);
      $datetime2 = new DateTime($end_date);
      $difference = $datetime1->diff($datetime2);
      if ($type == 'month') {
         $interval = $difference->m;
      } elseif ($type == 'week') {
         $interval = floor($difference->d / 7);
      } elseif ($type == 'hour') {
         $interval = $difference->h;
      } elseif ($type == 'minute') {
         $interval = $difference->i;
      } elseif ($type == 'year') {
         $interval = $difference->y;
      } else {
         if($difference->h != 0){
            $interval = $difference->days+1;
         }else{
            $interval = $difference->days;
         }
      }
      return $interval;
   }


   function add_datetime($date, $type){
      return date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($date)) . " " . $type));
   }

   protected function set_notif($user_id, $title = '', $message, $payload = false, $type = 'push', $status = 'pending')
   {
      if($user_id=='admin'){
         $listAdmin = User::whereAccess('admin')->get();
         foreach($listAdmin as $key){
            Notif::create([
               'user_id' => $key->user_id,
               'title' => $title,
               'message' => $message,
               'status' => 'pending',
               'type' => 'in_app',
               'payload' => json_encode($payload)
            ]);
         }
      }else{
         Notif::create([
            'user_id' => $user_id,
            'title' => $title,
            'message' => $message,
            'status' => 'pending',
            'type' => $type,
            'payload' => json_encode($payload)
         ]);

         if($type=='push'){
           Artisan::call('push:sender');
         }
      }
   }
}
