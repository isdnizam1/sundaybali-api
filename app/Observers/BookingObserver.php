<?php

namespace App\Observers;

use App\Libs\Slack;
use App\Models\Booking;
use App\Models\Driver;
use App\Models\Vendor;
use App\Models\Mutation;
use App\Models\Notification;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Support\Facades\Artisan;
use App\Traits\GlobalFunction;

class BookingObserver
{
    /**
     * Handle the Booking "created" event.
     *
     * @param  \App\Models\Booking  $booking
     * @return void
     */
    use GlobalFunction;

    public function created(Booking $booking)
    {
        if($booking->type=='SELF DRIVE'){
            $user_id = Vendor::find($booking->vendor_id)->user_id;
            // $this->set_notif($user_id,'New Order','Yeay.. you got a new  order #' . $booking->booking_id, ["booking_id" => $booking->booking_id, 'redirect' => '/admin/bookingDetail/' . $booking->booking_id],'push');
            $this->set_notif('admin', 'New ' . ucwords(strtolower($booking->type)) . ' order', 'New ' . ucwords(strtolower($booking->type)) . ' order #' . $booking->booking_id, ["booking_id" => $booking->booking_id, 'redirect' => '/admin/bookingDetail/' . $booking->booking_id]);
            Artisan::call('push:sender');
        }
    }

    /**
     * Handle the Booking "updated" event.
     *
     * @param  \App\Models\Booking  $booking
     * @return void
     */
    public function updated(Booking $booking)
    {
        if ($booking->getOriginal('payment_status') != $booking->payment_status) {
            if ($booking->payment_status == 'paid'){
                Slack::bookUpdate($booking->booking_id);
                if($booking->type=='SELF DRIVE'){
                    $user_id = Vendor::find($booking->vendor_id)->user_id;
                    $this->set_notif($user_id, 'New ' . ucwords(strtolower($booking->type)) . ' order', 'Yeay.. you got a new   order #' . $booking->booking_id, ["booking_id" => $booking->booking_id, 'redirect' => '/admin/bookingDetail/' . $booking->booking_id]);
                    $booking->update(['status' => 'waiting pickup']);
                }else{
                    $this->set_notif('admin', 'New ' . ucwords(strtolower($booking->type)) . ' order', 'New ' . ucwords(strtolower($booking->type)) . ' order #' . $booking->booking_id, ["booking_id" => $booking->booking_id, 'redirect' => '/admin/bookingDetail/' . $booking->booking_id]);
                }
            }
            if ($booking->payment_status == 'canceled') $booking->update(['payment_status' => 'expired', 'status' => 'cancel']);
            if ($booking->payment_status == 'failed') $booking->update(['payment_status' => 'expired', 'status' => 'cancel']);
            if ($booking->payment_status == 'refunded'){
                Notification::create([
                    'user_id' => $booking->user_id,
                    'title' => "Book Status Updated",
                    'message' => "Your order has been refunded",
                    'payload' => json_encode(["booking_id" => $booking->booking_id]),
                ]);
            }
        }

        if ($booking->getOriginal('status') != $booking->status) {
        
            if($booking->type=='SELF DRIVE'){
                Notification::create([
                    'user_id' => $booking->user_id,
                    'title' => 'Book Status Updated',
                    'message' => [
                        'awaiting departure time' => 'You got a car, awaiting for departure time.',
                        'waiting pickup' => 'You got a car, awaiting for departure time..',
                        'picked up' => 'The vendor has picked you up.',
                        'cancel' => 'Your order has been cancelled',
                        'completed' => 'Your order has been completed',
                        'pending' => 'Waiting for confirmation',
                    ][$booking->status],
                    'payload' => json_encode(['booking_id' => $booking->booking_id]),
                    'type' => 'push'
                ]);
            }else{
                Notification::create([
                    'user_id' => $booking->user_id,
                    'title' => 'Book Status Updated',
                    'message' => [
                        'waiting driver' => 'Getting a guide, waiting for confirmation.',
                        'awaiting departure time' => 'You got a guide, awaiting for departure time.',
                        'waiting pickup' => 'The guide has confirmed and is on his way to pick you up.',
                        'picked up' => 'The guide has picked you up.',
                        'cancel' => 'Your order has been cancelled',
                        'completed' => 'Your order has been completed',
                        'pending' => 'Searching for a guide',
                    ][$booking->status],
                    'payload' => json_encode(['booking_id' => $booking->booking_id]),
                    'type' => 'push'
                ]);
            }
            if ($booking->status == 'completed') {
                if($booking->type=='SELF DRIVE'){
                    $wallet_id = Wallet::whereUserId(Vendor::find($booking->vendor_id)->user_id)->first()->wallet_id;
                    $amount = $booking->subtotal-$booking->platform_fee;
                }else{
                    $wallet_id = Wallet::whereUserId(Driver::find($booking->driver_id)->user_id)->first()->wallet_id;
                    $amount = $booking->driver_fee;
                }
                Mutation::create([
                    'type' => 'order',
                    'wallet_id' => $wallet_id,
                    'amount_in' => $amount,
                    'status' => 'pending',
                    'amount_out' => 0,
                    'remark' => 'Fee transaction ' . $booking->booking_id,
                ]);
            } elseif ($booking->status == 'cancel' and $booking->getOriginal('payment_status') == 'paid') {
                // Mutation::create([
                //     'type' => 'refund',
                //     'wallet_id' => Wallet::whereUserId($booking->user_id)->first()->wallet_id,
                //     'amount_in' => $booking->subtotal,
                //     'amount_out' => 0,
                //     'remark' => 'Refund transaction ' . $booking->booking_id,
                // ]);
            }
        }
        Artisan::call('push:sender');
    }

    /**
     * Handle the Booking "deleted" event.
     *
     * @param  \App\Models\Booking  $booking
     * @return void
     */
    public function deleted(Booking $booking)
    {
        //
    }

    /**
     * Handle the Booking "restored" event.
     *
     * @param  \App\Models\Booking  $booking
     * @return void
     */
    public function restored(Booking $booking)
    {
        //
    }

    /**
     * Handle the Booking "force deleted" event.
     *
     * @param  \App\Models\Booking  $booking
     * @return void
     */
    public function forceDeleted(Booking $booking)
    {
        //
    }
}
