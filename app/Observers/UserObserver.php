<?php

namespace App\Observers;

use App\Models\User;
use App\Models\Wallet;
use App\Models\Notification;
use Illuminate\Support\Facades\Artisan;
class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        Wallet::create([
            'user_id' => $user->user_id,
            'balance' => 0,
        ]);
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $User
     * @return void
     */
    public function updated(User $user)
    {
        if ($user->getOriginal('account_status') != $user->account_status) {
            $additional_data = json_decode($user->additional_data);
            $status ='';
            if ($user->account_status=='banned') {
                $status = 'banned';
            }elseif($user->account_status=='active') {
                $status = 'activated';
            }

            if(!empty($additional_data->banned_reason)){
                $message = 'Your account has been '.$status.' with reason : '.$additional_data->banned_reason;
            }else{
                $message = 'Your account has been '.$status.' with no reason';
            }

            Notification::create([
                'user_id' => $user->user_id,
                'title' => "Account Status",
                'message' => $message,
                'payload' => json_encode(["user_id" => $user->user_id]),
            ]);
            Artisan::call('push:sender');
        }
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $User
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $User
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $User
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
