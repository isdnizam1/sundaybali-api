<?php

namespace App\Observers;

use App\Models\Booking;
use App\Models\BookingDriver;
use App\Models\Driver;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use App\Traits\GlobalFunction;

class BookingDriverObserver
{
    /**
     * Handle the BookingDriver "created" event.
     *
     * @param  \App\Models\BookingDriver  $bookingDriver
     * @return void
     */
    use GlobalFunction;

    public function creating(BookingDriver $bookingDriver){
        $booking = Booking::find($bookingDriver->booking_id);
        $bookingDriver->pickup_time = sprintf('%s %s', $booking->pickup_date, $booking->pickup_time);
    }

    public function created(BookingDriver $bookingDriver)
    {
        try {
            Notification::create([
                'user_id' => Driver::find($bookingDriver->driver_id)->user_id,
                'title' => 'Yeaay.. You got an order',
                'message' => 'Please confirm the order immediately',
                'payload' => json_encode(['booking_id' => $bookingDriver->booking_id]),
                'type' => 'push'
            ]);
            Artisan::call('push:sender');
            Artisan::call('book:upcoming');
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Handle the BookingDriver "updated" event.
     *
     * @param  \App\Models\BookingDriver  $bookingDriver
     * @return void
     */
    public function updated(BookingDriver $bookingDriver)
    {
        if ($bookingDriver->getOriginal('status') != $bookingDriver->status) {
            Notification::create([
                'user_id' => Driver::find($bookingDriver->driver_id)->user_id,
                'title' => 'Book Status Updated',
                'message' => [
                    'confirm' => 'Please pick up consumers immediately',
                    'upcoming' => 'You have successfully confirmed the order',
                    'decline' => 'You have canceled the order',
                    'expired' => 'Your order is cancelled',
                ][$bookingDriver->status],
                'payload' => json_encode(['booking_id' => $bookingDriver->booking_id]),
                'type' => 'push'
            ]);
            if ($bookingDriver->status == 'confirm') {
                Booking::find($bookingDriver->booking_id)->update(['status' => 'waiting pickup']);
                $this->set_notif('admin', 'Book Status Updated', 'Guide successfully confirmed the order #' . $bookingDriver->booking_id, ["booking_id" => $bookingDriver->booking_id, 'redirect' => '/admin/bookingDetail/' . $bookingDriver->booking_id]);
            } elseif ($bookingDriver->status == 'decline') {
                $this->set_notif('admin', 'Book Status Updated', 'Guide canceled the order #' . $bookingDriver->booking_id, ["booking_id" => $bookingDriver->booking_id, 'redirect' => '/admin/bookingDetail/' . $bookingDriver->booking_id]);
            } elseif ($bookingDriver->status == 'upcoming') {
                $this->set_notif('admin', 'awaiting departure time', 'Guide successfully confirmed the order #' . $bookingDriver->booking_id, ["booking_id" => $bookingDriver->booking_id, 'redirect' => '/admin/bookingDetail/' . $bookingDriver->booking_id]);
            } elseif ($bookingDriver->status == 'expired') {
                $this->set_notif('admin', 'Book Status Updated', 'Guide no confirmation the order #' . $bookingDriver->booking_id, ["booking_id" => $bookingDriver->booking_id, 'redirect' => '/admin/bookingDetail/' . $bookingDriver->booking_id]);
            }
        }
        Artisan::call('push:sender');
    }

    /**
     * Handle the BookingDriver "deleted" event.
     *
     * @param  \App\Models\BookingDriver  $bookingDriver
     * @return void
     */
    public function deleted(BookingDriver $bookingDriver)
    {
        //
    }

    /**
     * Handle the BookingDriver "restored" event.
     *
     * @param  \App\Models\BookingDriver  $bookingDriver
     * @return void
     */
    public function restored(BookingDriver $bookingDriver)
    {
        //
    }

    /**
     * Handle the BookingDriver "force deleted" event.
     *
     * @param  \App\Models\BookingDriver  $bookingDriver
     * @return void
     */
    public function forceDeleted(BookingDriver $bookingDriver)
    {
        //
    }
}
