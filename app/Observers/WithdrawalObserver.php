<?php

namespace App\Observers;

use App\Models\Withdrawal;
use App\Models\Mutation;
use App\Models\Notification;
use App\Models\Wallet;
use Illuminate\Support\Facades\Artisan;
use App\Traits\GlobalFunction;
class WithdrawalObserver
{
    /**
     * Handle the Withdrawal "created" event.
     *
     * @param  \App\Models\Withdrawal  $withdrawal
     * @return void
     */
    use GlobalFunction;

    public function created(Withdrawal $withdrawal)
    {
        $wallet = Wallet::find($withdrawal->wallet_id);
        $this->set_notif('admin', 'Withdrawal request', 'You have a new withdrawal request', ["withdrawal_id" => $withdrawal->withdrawal_id,'redirect'=>'/admin/withdrawal']);
        Mutation::create([
            'type' => 'refund',
            'wallet_id' => $withdrawal->wallet_id,
            'amount_in' => 0,
            'amount_out' => $withdrawal->amount,
            'remark' => 'Withdrawal request',
        ]);
        Artisan::call('push:sender');
    }
    /**
     * Handle the Withdrawal "updated" event.
     *
     * @param  \App\Models\Withdrawal  $withdrawal
     * @return void
     */
    public function updated(Withdrawal $withdrawal)
    {
        $wallet = Wallet::find($withdrawal->wallet_id);
        if ($withdrawal->getOriginal('status') != $withdrawal->status) {
            if ($withdrawal->status == 'decline') {
                Mutation::create([
                    'type' => 'refund',
                    'wallet_id' => $withdrawal->wallet_id,
                    'amount_in' => $withdrawal->amount,
                    'amount_out' => 0,
                    'remark' => 'Refund from decline withdrawal',
                ]);
                Notification::create([
                    'user_id' => $wallet->user_id,
                    'title' => "Featour e-Wallet",
                    'message' => 'Your withdrawal has been declined',
                    'payload' => json_encode(["withdrawal_id" => $withdrawal->withdrawal_id]),
                ]);
            }elseif ($withdrawal->status == 'paid') {
                Notification::create([
                    'user_id' => $wallet->user_id,
                    'title' => "Featour e-Wallet",
                    'message' => 'Your withdrawal has been paid',
                    'payload' => json_encode(["withdrawal_id" => $withdrawal->withdrawal_id]),
                ]);
            }
        }
        Artisan::call('push:sender');
    }

    /**
     * Handle the Withdrawal "deleted" event.
     *
     * @param  \App\Models\Withdrawal  $withdrawal
     * @return void
     */
    public function deleted(Withdrawal $withdrawal)
    {
        //
    }

    /**
     * Handle the Withdrawal "restored" event.
     *
     * @param  \App\Models\Withdrawal  $withdrawal
     * @return void
     */
    public function restored(Withdrawal $withdrawal)
    {
        //
    }

    /**
     * Handle the Withdrawal "force deleted" event.
     *
     * @param  \App\Models\Withdrawal  $withdrawal
     * @return void
     */
    public function forceDeleted(Withdrawal $withdrawal)
    {
        //
    }
}
