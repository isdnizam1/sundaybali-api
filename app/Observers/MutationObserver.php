<?php

namespace App\Observers;

use App\Models\Mutation;
use App\Models\Notification;
use App\Models\Wallet;
use Illuminate\Support\Facades\Artisan;

class MutationObserver
{
    /**
     * Handle the Mutation "created" event.
     *
     * @param  \App\Models\Mutation  $mutation
     * @return void
     */
    public function created(Mutation $mutation)
    {
        $wallet = Wallet::findOrFail($mutation->wallet_id);
        // $mutations = Mutation::whereWalletId($wallet->wallet_id)->whereIn('status',['pending','completed'])->get();
        $mutations = Mutation::whereWalletId($wallet->wallet_id)->whereStatus('pending')->get();
        $completedMutations = Mutation::whereWalletId($wallet->wallet_id)->whereStatus('completed')->get();
        $wallet->fill([
            'balance' => $mutations->sum('amount_in') - $mutations->sum('amount_out'),
            'balance_withdraw' =>  $completedMutations->sum('amount_in') - $completedMutations->sum('amount_out')
        ]);
        $wallet->save();
        Notification::create([
            'user_id' => $wallet->user_id,
            'title' => "Featour e-Wallet",
            // 'title' => ($mutation->amount_in ?? 0) > 0 ? "Your wallet balance increases" : "Your wallet balance is reduced",
            'message' => sprintf('(%s%s) %s', ($mutation->amount_in ?? 0) > 0 ? '+' : '-', number_format(($mutation->amount_in ?? 0) > 0 ? $mutation->amount_in : $mutation->amount_out), $mutation->remark),
            'payload' => json_encode(["mutation_id" => $mutation->mutation_id]),
        ]);
        Artisan::call('push:sender');
        try {
        } catch (\Throwable $th) {
            // Just keep silent
        }
    }

    /**
     * Handle the Mutation "updated" event.
     *
     * @param  \App\Models\Mutation  $mutation
     * @return void
     */
    public function updated(Mutation $mutation)
    {
        //
            $wallet = Wallet::findOrFail($mutation->wallet_id);
            $mutations = Mutation::whereWalletId($wallet->wallet_id)->whereIn('status',['pending','completed'])->get();
            $completedMutations = Mutation::whereWalletId($wallet->wallet_id)->whereStatus('completed')->get();
            $wallet->fill([
                'balance' => $mutations->sum('amount_in') - $mutations->sum('amount_out'),
                'balance_withdraw' =>  $completedMutations->sum('amount_in') - $completedMutations->sum('amount_out')
            ]);  
            $wallet->save();
            Notification::create([
                'user_id' => $wallet->user_id,
                'title' => "Featour e-Wallet",
                // 'title' => ($mutation->amount_in ?? 0) > 0 ? "Your wallet balance increases" : "Your wallet balance is reduced",
                'message' => sprintf('(%s%s) %s', ($mutation->amount_in ?? 0) > 0 ? '+' : '-', number_format(($mutation->amount_in ?? 0) > 0 ? $mutation->amount_in : $mutation->amount_out), $mutation->remark),
                'payload' => json_encode(["mutation_id" => $mutation->mutation_id]),
            ]);
            Artisan::call('push:sender');
            try {
            } catch (\Throwable $th) {
                // Just keep silent
            }
    }

    /**
     * Handle the Mutation "deleted" event.
     *
     * @param  \App\Models\Mutation  $mutation
     * @return void
     */
    public function deleted(Mutation $mutation)
    {
        //
    }

    /**
     * Handle the Mutation "restored" event.
     *
     * @param  \App\Models\Mutation  $mutation
     * @return void
     */
    public function restored(Mutation $mutation)
    {
        //
    }

    /**
     * Handle the Mutation "force deleted" event.
     *
     * @param  \App\Models\Mutation  $mutation
     * @return void
     */
    public function forceDeleted(Mutation $mutation)
    {
        //
    }
}
