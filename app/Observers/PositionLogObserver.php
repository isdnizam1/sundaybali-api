<?php

namespace App\Observers;

use App\Models\Driver;
use App\Models\PositionLog;

class PositionLogObserver
{
    /**
     * Handle the PositionLog "created" event.
     *
     * @param  \App\Models\PositionLog  $positionLog
     * @return void
     */
    public function created(PositionLog $positionLog)
    {
        Driver::find($positionLog->driver_id)->update([
            'last_position_id' => $positionLog->id
        ]);
    }

    /**
     * Handle the PositionLog "updated" event.
     *
     * @param  \App\Models\PositionLog  $positionLog
     * @return void
     */
    public function updated(PositionLog $positionLog)
    {
        //
    }

    /**
     * Handle the PositionLog "deleted" event.
     *
     * @param  \App\Models\PositionLog  $positionLog
     * @return void
     */
    public function deleted(PositionLog $positionLog)
    {
        //
    }

    /**
     * Handle the PositionLog "restored" event.
     *
     * @param  \App\Models\PositionLog  $positionLog
     * @return void
     */
    public function restored(PositionLog $positionLog)
    {
        //
    }

    /**
     * Handle the PositionLog "force deleted" event.
     *
     * @param  \App\Models\PositionLog  $positionLog
     * @return void
     */
    public function forceDeleted(PositionLog $positionLog)
    {
        //
    }
}
