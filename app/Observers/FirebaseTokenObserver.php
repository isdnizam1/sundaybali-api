<?php

namespace App\Observers;

use App\Models\FirebaseToken;
use App\Models\Notification;
use Illuminate\Support\Facades\Artisan;

class FirebaseTokenObserver
{
    /**
     * Handle the FirebaseToken "created" event.
     *
     * @param  \App\Models\FirebaseToken  $firebaseToken
     * @return void
     */
    public function created(FirebaseToken $firebaseToken)
    {
        Notification::create([
            'user_id' => $firebaseToken->user_id,
            'title' => "Welcome to Featours",
            'message' => 'We are here to make your vacation easier and more enjoyable',
            'payload' => json_encode(["welcome" => "1"]),
        ]);
        // Artisan::call('push:sender');
    }

    /**
     * Handle the FirebaseToken "updated" event.
     *
     * @param  \App\Models\FirebaseToken  $firebaseToken
     * @return void
     */
    public function updated(FirebaseToken $firebaseToken)
    {
        //
    }

    /**
     * Handle the FirebaseToken "deleted" event.
     *
     * @param  \App\Models\FirebaseToken  $firebaseToken
     * @return void
     */
    public function deleted(FirebaseToken $firebaseToken)
    {
        //
    }

    /**
     * Handle the FirebaseToken "restored" event.
     *
     * @param  \App\Models\FirebaseToken  $firebaseToken
     * @return void
     */
    public function restored(FirebaseToken $firebaseToken)
    {
        //
    }

    /**
     * Handle the FirebaseToken "force deleted" event.
     *
     * @param  \App\Models\FirebaseToken  $firebaseToken
     * @return void
     */
    public function forceDeleted(FirebaseToken $firebaseToken)
    {
        //
    }
}
