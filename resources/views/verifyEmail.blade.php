Dear {{$name}},
<br><br>
Please verify your email with bellow link: 
<a href="{{ $url }}">Verify Email</a>
<br><br>
If you didn’t ask to verify this address, you can ignore this email.
<br><br>
Thanks,