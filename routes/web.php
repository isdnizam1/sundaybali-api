<?php

use App\Http\Controllers\Api\V1\DuitkuController;
use App\Http\Controllers\Api\V1\VendorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('duitku/return', [DuitkuController::class, 'server_return']);
Route::post('duitku/callback', [DuitkuController::class, 'server_callback']);
Route::get('vendor/verify', [VendorController::class, 'verify_vendor']);
