<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\UserController;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\NewsController;
use App\Http\Controllers\Api\V1\PromotionController;
use App\Http\Controllers\Api\V1\BookingController;
use App\Http\Controllers\Api\V1\AttractionController;
use App\Http\Controllers\Api\V1\DuitkuController;
use App\Http\Controllers\Api\V1\SettingsController;
use App\Http\Controllers\Api\V1\VehicleController;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Api\V1\BankController;
use App\Http\Controllers\Api\V1\BookingDriverController;
use App\Http\Controllers\Api\V1\DashboardController;
use App\Http\Controllers\Api\V1\DriverController;
use App\Http\Controllers\Api\V1\WalletController;
use App\Http\Controllers\Api\V1\WithdrawalController;
use App\Http\Controllers\Api\V1\NotifController;
use App\Http\Controllers\Api\V1\PositionController;
use App\Http\Controllers\Api\V1\VendorController;
use App\Http\Controllers\Api\V1\CarsController;
use App\Http\Controllers\Api\V1\ReviewsController;
use App\Http\Controllers\Api\V1\ReportController;
use App\Http\Controllers\Api\V1\CountryController;
use App\Http\Controllers\Api\V1\DocumentController;
use App\Http\Controllers\Api\V1\ChatController;
use App\Http\Controllers\Api\V1\TestingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// User
Route::post('v1/auth/change_password', [AuthController::class, 'change_password']);

Route::group(['middleware' => ['api_auth']], function () {
    Route::post('v1/auth/phone_verification', [AuthController::class, 'phone_verification']);
    Route::post('v1/auth/resend_phone_verification', [AuthController::class, 'resend_phone_verification']);
    Route::post('v1/auth/create_new_password', [AuthController::class, 'create_new_password']);
    Route::post('v1/auth/firebase-token', [AuthController::class, 'firebase_token']);
    Route::get('v1/user', [UserController::class, 'index']);
    Route::post('v1/user/add_driver', [UserController::class, 'add_driver']);

    Route::get('v1/user/detail', [UserController::class, 'detail']);
    Route::post('v1/user/set_status_user', [UserController::class, 'set_status_user']);
    Route::post('v1/user/update_profile', [UserController::class, 'update_profile']);
    Route::get('v1/user/driver', [UserController::class, 'driver']);
    Route::post('v1/user/delete_account', [UserController::class, 'delete_account']);

    Route::post('v1/news/add', [NewsController::class, 'add']);

    Route::post('v1/booking/submit', [BookingController::class, 'submit']);
    Route::get('v1/booking/detail-new', [BookingController::class, 'detail_new']);
    Route::post('v1/booking/book-city-transfer', [BookingController::class, 'book_city_transfer']);
    Route::post('v1/booking/book-car-charter', [BookingController::class, 'book_car_charter']);
    Route::post('v1/booking/book-self-drive', [BookingController::class, 'book_self_drive']);
    Route::get('v1/booking', [BookingController::class, 'index']);
    Route::get('v1/booking/self_drive', [BookingController::class, 'self_drive']);
    Route::get('v1/booking/detail', [BookingController::class, 'detail']);
    Route::post('v1/booking/selectDriver', [BookingController::class, 'selectDriver']);
    Route::post('v1/booking/declineBooking', [BookingController::class, 'declineBooking']);
    Route::post('v1/booking/confirmBooking', [BookingController::class, 'confirmBooking']);
    Route::post('v1/booking/refundBooking', [BookingController::class, 'refundBooking']);
    Route::post('v1/booking/sendFeeTranscation', [BookingController::class, 'sendFeeTranscation']);
    Route::post('v1/booking/paidBooking', [BookingController::class, 'paidBooking']);
    Route::get('v1/booking/on_rent', [BookingController::class, 'on_rent']);
    Route::post('v1/booking/confirmCleaningCar', [BookingController::class, 'confirmCleaningCar']);
    Route::post('v1/booking/uploadPhoto', [BookingController::class, 'uploadPhoto']);
    Route::post('v1/booking/deletePhoto', [BookingController::class, 'deletePhoto']);
    // Route::get('v1/booking/my_rent', [BookingController::class, 'my_rent']);
    Route::get('v1/booking/status-logs', [BookingController::class, 'statusLogs']);

    Route::get('v1/attraction', [AttractionController::class, 'index']);
    Route::post('v1/vehicle/add_vehicle', [VehicleController::class, 'add_vehicle']);

    Route::post('v1/attraction/store', [AttractionController::class, 'store']);
    Route::post('v1/news/store', [NewsController::class, 'store']);
    Route::post('v1/promotion/add', [PromotionController::class, 'add']);
    Route::post('v1/promotion/edit', [PromotionController::class, 'edit']);

    Route::post('v1/settings/edit_car_charter_price', [SettingsController::class, 'edit_car_charter_price']);
    Route::post('v1/settings/edit_car_charter_rules', [SettingsController::class, 'edit_car_charter_rules']);
    Route::post('v1/settings/edit_setting', [SettingsController::class, 'edit_setting']);
    Route::post('v1/settings/edit_self_drive_price', [SettingsController::class, 'edit_self_drive_price']);

    Route::get('v1/duitku/payment-methods', [DuitkuController::class, 'payment_methods']);

    Route::get('v1/dashboard', [DashboardController::class, 'index']);
    Route::post('v1/driver/switch-status', [DriverController::class, 'switch_status']);
    Route::get('v1/driver/last_location', [DriverController::class, 'last_location']);
    Route::get('v1/bank/account', [BankController::class, 'account']);
    Route::post('v1/bank/save', [BankController::class, 'save']);


    Route::get('v1/withdrawal', [WithdrawalController::class, 'index']);
    Route::get('v1/withdrawal/personal', [WithdrawalController::class, 'personal_withdrawal_history']);
    Route::post('v1/withdrawal/inquiry', [WithdrawalController::class, 'inquiry']);
    Route::post('v1/withdrawal/set_status_withdraw', [WithdrawalController::class, 'set_status_withdraw']);
    Route::post('v1/booking-driver/confirm-order', [BookingDriverController::class, 'confirm_order']);
    Route::post('v1/booking-driver/skip-order', [BookingDriverController::class, 'skip_order']);
    Route::post('v1/booking-driver/picked-up', [BookingDriverController::class, 'picked_up']);
    Route::post('v1/booking-driver/finish-order', [BookingDriverController::class, 'finish_order']);
    Route::get('v1/booking-driver/history', [BookingDriverController::class, 'history']);
    Route::get('v1/wallet/mutations', [WalletController::class, 'mutations']);

    Route::get('v1/notification', [NotifController::class, 'index']);
    Route::post('v1/notification/set_status_notif', [NotifController::class, 'set_status_notif']);
    Route::post('v1/position/push-log', [PositionController::class, 'push_log']);

    Route::get('v1/vendor', [VendorController::class, 'index']);
    Route::post('v1/vendor/update_location', [VendorController::class, 'update_location']);
    Route::post('v1/vendor/update_profile', [VendorController::class, 'update_profile']);
    Route::post('v1/vendor/add_vendor', [VendorController::class, 'add_vendor']);
    Route::post('v1/vendor/update_free_delivery', [VendorController::class, 'update_free_delivery']);
    Route::post('v1/car/add_car', [CarsController::class, 'add_car']);
    Route::post('v1/car/set_status', [CarsController::class, 'set_status']);
    Route::post('v1/car/add_image', [CarsController::class, 'add_image']);
    Route::post('v1/car/remove_image', [CarsController::class, 'remove_image']);
    Route::post('v1/reviews/add_review', [ReviewsController::class, 'addReview']);

    Route::get('v1/report', [ReportController::class, 'index']);
    Route::post('v1/report/add_report', [ReportController::class, 'addReport']);
    Route::post('v1/auth/remove_firebase_token', [AuthController::class, 'remove_firebase_token']);

    Route::post('v1/document/insert_document', [DocumentController::class, 'insert_document']);
    Route::post('v1/document/set_status_document', [DocumentController::class, 'set_status_document']);

    Route::get('v1/chat', [ChatController::class, 'index']);
    Route::post('v1/chat/send', [ChatController::class, 'send']);
    Route::post('v1/chat/delete', [ChatController::class, 'delete']);
});

// // Authentication
Route::get('v1/car/', [CarsController::class, 'index']);
Route::get('v1/reviews', [ReviewsController::class, 'index']);
Route::get('v1/car/detail', [CarsController::class, 'detail']);
Route::get('v1/car/search', [CarsController::class, 'search']);
Route::post('v1/auth/otpVerification', [AuthController::class, 'otpVerification']);
Route::post('v1/auth/sendOtp', [AuthController::class, 'sendOtp']);
Route::get('v1/booking/get-price', [BookingController::class, 'get_price']);
Route::post('v1/auth/update_token', [AuthController::class, 'update_token']);
Route::post('v1/auth/check_account', [AuthController::class, 'check_account']);
Route::post('v1/notif/send_notif', [NotifController::class, 'send_notif']);
Route::post('v1/auth/check_existing_email', [AuthController::class, 'check_existing_email']);
Route::post('v1/auth/check_existing_phone', [AuthController::class, 'check_existing_phone']);
Route::post('v1/auth/user_registration', [AuthController::class, 'user_registration']);
Route::post('v1/auth/submit_registration', [AuthController::class, 'submit_registration']);
Route::post('v1/auth/login', [AuthController::class, 'login']);
Route::post('v1/auth/firebase_signin', [AuthController::class, 'firebase_signin']);
Route::post('v1/auth/facebook_signin', [AuthController::class, 'facebook_signin']);
Route::post('v1/auth/forgot_password', [AuthController::class, 'send_forgot_password']);
Route::post('v1/auth/check_forgot_password_code', [AuthController::class, 'check_forgot_password_code']);
Route::post('v1/auth/guide_signin', [AuthController::class, 'guide_signin']);
Route::post('v1/auth/driver_registration', [AuthController::class, 'driver_registration']);

Route::get('v1/settings', [SettingsController::class, 'index']);
Route::get('v1/bank', [BankController::class, 'index']);
Route::get('v1/news', [NewsController::class, 'index']);
Route::get('v1/promotion', [PromotionController::class, 'index']);
Route::get('v1/country', [CountryController::class, 'index']);
Route::get('v1/vehicle', [VehicleController::class, 'index']);
Route::get('v1/vehicle/type', [VehicleController::class, 'type']);
Route::post('v1/test/email', [TestingController::class, 'send_email']);
