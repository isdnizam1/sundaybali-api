# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.13-MariaDB)
# Database: sundayBali
# Generation Time: 2021-04-15 14:29:12 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table module_attraction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_attraction`;

CREATE TABLE `module_attraction` (
  `attraction_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `attraction_name` varchar(255) DEFAULT NULL,
  `overview` longtext DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`attraction_id`),
  UNIQUE KEY `module_attraction_index_9` (`attraction_id`),
  KEY `user_id` (`user_id`),
  KEY `module_attraction_index_8` (`attraction_id`),
  CONSTRAINT `module_attraction_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `system_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table module_attraction_gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_attraction_gallery`;

CREATE TABLE `module_attraction_gallery` (
  `attraction_gallery_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attraction_id` bigint(20) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`attraction_gallery_id`),
  UNIQUE KEY `module_attraction_gallery_index_11` (`attraction_gallery_id`),
  KEY `module_attraction_gallery_index_10` (`attraction_id`),
  CONSTRAINT `module_attraction_gallery_ibfk_1` FOREIGN KEY (`attraction_id`) REFERENCES `module_attraction` (`attraction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table module_booking
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_booking`;

CREATE TABLE `module_booking` (
  `booking_id` varchar(255) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `driver_id` bigint(20) DEFAULT NULL,
  `type` enum('tour','transfer') DEFAULT 'tour',
  `status` enum('pending','approved','decline') DEFAULT NULL,
  `additional_data` longtext DEFAULT NULL COMMENT 'date,pickup_time,pax,location,note,duration',
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`booking_id`),
  KEY `user_id` (`user_id`),
  KEY `driver_id` (`driver_id`),
  CONSTRAINT `module_booking_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `system_users` (`user_id`),
  CONSTRAINT `module_booking_ibfk_2` FOREIGN KEY (`driver_id`) REFERENCES `module_driver` (`driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table module_driver
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_driver`;

CREATE TABLE `module_driver` (
  `driver_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `vehicle_id` bigint(20) DEFAULT NULL,
  `plate_number` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`driver_id`),
  UNIQUE KEY `module_driver_index_19` (`driver_id`),
  KEY `module_driver_index_18` (`user_id`,`driver_id`),
  CONSTRAINT `module_driver_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `system_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table module_news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_news`;

CREATE TABLE `module_news` (
  `news_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` enum('pending','sent','read') DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`news_id`),
  UNIQUE KEY `module_news_index_5` (`news_id`),
  KEY `user_id` (`user_id`),
  KEY `module_news_index_4` (`news_id`,`user_id`,`status`),
  CONSTRAINT `module_news_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `system_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table module_notification
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_notification`;

CREATE TABLE `module_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `payload` longtext DEFAULT NULL,
  `type` enum('push','in_app') DEFAULT NULL,
  `status` enum('pending','sent','read') DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `module_notification_index_3` (`id`),
  KEY `user_id` (`user_id`),
  KEY `module_notification_index_2` (`id`,`user_id`,`status`,`type`),
  CONSTRAINT `module_notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `system_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table module_payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_payment`;

CREATE TABLE `module_payment` (
  `payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `booking_id` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `payment_type` enum('bank_transfer','credit_card') DEFAULT NULL,
  `additional_data` longtext DEFAULT NULL,
  `payment_status` enum('unpaid','pending','setlement','expire','cancel') DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`payment_id`),
  UNIQUE KEY `module_payment_index_21` (`payment_id`),
  KEY `module_payment_index_20` (`booking_id`,`payment_id`),
  CONSTRAINT `module_payment_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `module_booking` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table module_promotion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_promotion`;

CREATE TABLE `module_promotion` (
  `promotion_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `promotion_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` longtext DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`promotion_id`),
  UNIQUE KEY `module_promotion_index_13` (`promotion_id`),
  KEY `user_id` (`user_id`),
  KEY `module_promotion_index_12` (`promotion_id`),
  CONSTRAINT `module_promotion_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `system_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table module_vehicle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_vehicle`;

CREATE TABLE `module_vehicle` (
  `vehicle_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vehicle_type_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `seats` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`vehicle_id`),
  UNIQUE KEY `module_vehicle_index_7` (`vehicle_id`),
  KEY `module_vehicle_index_6` (`vehicle_type_id`),
  CONSTRAINT `module_vehicle_ibfk_1` FOREIGN KEY (`vehicle_type_id`) REFERENCES `module_vehicle_type` (`vehicle_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table module_vehicle_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `module_vehicle_type`;

CREATE TABLE `module_vehicle_type` (
  `vehicle_type_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`vehicle_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table system_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_settings`;

CREATE TABLE `system_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(255) DEFAULT NULL,
  `value` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `system_settings_index_15` (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table system_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_users`;

CREATE TABLE `system_users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `social_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `phone_status` enum('unverified','verified') DEFAULT NULL,
  `access` enum('user','driver','admin') DEFAULT NULL,
  `account_status` enum('pending','approved','decline') DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `additional_data` longtext DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `registered_via` enum('email','google','facebook') DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `system_users_index_1` (`user_id`),
  KEY `system_users_index_0` (`user_id`,`email`,`access`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `system_users` WRITE;
/*!40000 ALTER TABLE `system_users` DISABLE KEYS */;

INSERT INTO `system_users` (`user_id`, `social_id`, `first_name`, `last_name`, `email`, `username`, `password`, `phone`, `phone_status`, `access`, `account_status`, `profile_picture`, `additional_data`, `token`, `registered_via`, `created_at`, `updated_at`)
VALUES
	(3,NULL,'John','Doe','ad1min@mailinator.com','admin96','$2y$10$JXlH9UwUuN973yVwO.jqxeYqeQmxE5KkTq7QRfv8EDvQCWA5Rsave','+62856966403232','verified','user','pending',NULL,'{\"phone_verification_code\":923134,\"forgotten_password_code\":\"\"}','621faadd9b96656c6c85578afd338838b57f0b24',NULL,'2021-04-15 00:32:34','2021-04-15 01:06:00');

/*!40000 ALTER TABLE `system_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
