<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleBookingDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_booking_driver', function (Blueprint $table) {
            $table->id('booking_driver_id');
            $table->string('booking_id');
            $table->string('driver_id');
            $table->dateTime('deadline_confirmation');
            $table->enum('status', ['pending', 'confirm','decline','expired'])->default('pending');
            $table->string('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_booking_driver');
    }
}
