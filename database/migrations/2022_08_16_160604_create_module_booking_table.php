<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_booking', function (Blueprint $table) {
            $table->id();
            $table->string('booking_id');
            $table->enum('payment_status',['unpaid','paid','canceled','expired','failed','refunded'])->default('unpaid');
            $table->enum('status',['pending','awaiting departure time','waiting driver','waiting pickup','picked up','cancel','completed'])->default('waiting driver');
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('driver_id')->nullable();
            $table->integer('car_id')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->enum('type',['TOUR','CITY TRANSFER','CAR CHARTER','SELF DRIVE'])->default('tour')->nullable();
            $table->enum('book_type',['now','tomorrow','later'])->default('now')->nullable();
            $table->date('pickup_date')->nullable();
            $table->time('pickup_time')->nullable();
            $table->string('duration')->nullable();
            $table->integer('adult')->nullable();
            $table->integer('child')->nullable();
            $table->integer('infant')->nullable();
            $table->double('distance')->nullable();
            $table->double('rate_per_km')->nullable();
            $table->double('car_price')->nullable();
            $table->double('entrance_fee')->nullable();
            $table->double('driver_fee')->nullable();
            $table->double('payment_fee')->nullable();
            $table->double('platform_fee')->nullable();
            $table->double('subtotal')->nullable();
            $table->longText('additional_data')->nullable();
            $table->timestamps();
            $table->string('car_type')->nullable();
            $table->longText('destination')->nullable();
            $table->string('payment_provider')->nullable();
            $table->text('payment_data')->nullable();
            $table->boolean('reminder_sent')->nullable();
            $table->boolean('is_airport_transfer')->nullable();
            $table->boolean('is_refund')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_booking');
    }
}
