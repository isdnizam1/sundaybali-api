<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_report', function (Blueprint $table) {
            $table->increments('report_id');
            $table->string('booking_id')->nullable();
            $table->string('coment')->nullable();
            $table->string('reason')->nullable();
            $table->enum('status',['on-progress','solved','confirm','decline','costumer-win','driver-win'])->default('on-progress');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_report');
    }
}
