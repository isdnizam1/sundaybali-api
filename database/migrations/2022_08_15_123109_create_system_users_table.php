<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('social_id')->nullable();
            $table->string('first_id')->nullable();
            $table->string('last_id')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('about_me')->nullable();
            $table->string('phone')->nullable();
            $table->enum('phone_status',['unverified','verified','facebook','phone','password','apple'])->nullable();
            $table->enum('access',['user','driver','admin','vendor'])->nullable();
            $table->string('profile_picture')->nullable();
            $table->longText('additional_data')->nullable();
            $table->string('token')->nullable();
            $table->enum('registered_via',['email','google','facebook','phone','password','apple'])->nullable();
            $table->string('prefered_leanguage')->nullable();           
            $table->timestamps();
            $table->enum('account_status',['active','banned'])->dafault('active');
            $table->enum('is_document_verified',['pending','verified','unverified'])->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_users');
    }
}
