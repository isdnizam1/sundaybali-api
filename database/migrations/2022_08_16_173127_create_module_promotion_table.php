<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulePromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_promotion', function (Blueprint $table) {
            $table->increments('promotion_id');
            $table->bigInteger('user_id')->nullable();
            $table->string('promotion_name')->nullable();
            $table->longText('description')->nullable();
            $table->longText('image')->nullable();
            $table->string('voucher_code');
            $table->string('discount');
            $table->enum('status',['active','inactive'])->nullable()->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_promotion');
    }
}
