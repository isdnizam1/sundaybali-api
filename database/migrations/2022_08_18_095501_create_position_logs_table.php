<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('driver_id');
            $table->string('street_name');
            $table->string('country');
            $table->string('province');
            $table->string('district');
            $table->string('subdistrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_logs');
    }
}
