<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_cars', function (Blueprint $table) {
            $table->increments('car_id');
            $table->integer('vendor_id');
            $table->integer('vehicle_id')->nullable();
            $table->integer('decription')->nullable();
            $table->integer('year');
            $table->enum('transmission', ['Manual','Matic'])->default('Manual');
            $table->string('plate_number');
            $table->integer('rental_price');
            $table->string('image');
            $table->string('latitude');
            $table->string('longitude')->nullable();
            $table->string('location_name')->nullable();
            $table->enum('status', ['Active','Non Active'])->default('Active');
            $table->integer('seats')->nullable();
            $table->string('fuel')->nullable();
            $table->string('class')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_cars');
    }
}
