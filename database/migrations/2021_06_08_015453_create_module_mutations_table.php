<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_mutations', function (Blueprint $table) {
            $table->id('mutation_id');
            $table->enum('type', ['topup', 'order','withdraw','refund'])->default('topup');
            $table->integer('wallet_id');
            $table->integer('balance');
            $table->integer('amount_in');
            $table->integer('amount_out');
            $table->string('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_mutations');
    }
}
