<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_vehicle', function (Blueprint $table) {
            $table->increments('vehicle_id');
            $table->string('vehicle_type_id')->nullable();
            $table->string('name')->nullable();
            $table->integer('seats')->nullable();
            $table->integer('price')->nullable();
            $table->timestamps();
            $table->integer('suitcases')->nullable();
            $table->text('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_vehicle');
    }
}
