<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_reviews', function (Blueprint $table) {
            $table->increments('review_id');
            $table->integer('user_id');
            $table->integer('star');
            $table->string('comment')->nullable();
            $table->longText('service')->nullable();;
            $table->integer('booking_id')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_reviews');
    }
}
