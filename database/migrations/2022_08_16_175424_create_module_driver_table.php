<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_driver', function (Blueprint $table) {
            $table->increments('driver_id');
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('vehicle_id')->nullable();
            $table->bigInteger('last_position_id')->nullable();
            $table->string('plate_number')->nullable();
            $table->timestamps();
            $table->enum('status',['Online','Ofline','Blocked'])->default('Ofline');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_driver');
    }
}
