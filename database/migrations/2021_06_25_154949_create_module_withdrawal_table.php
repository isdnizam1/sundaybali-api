<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleWithdrawalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_withdrawal', function (Blueprint $table) {
            $table->id('withdrawal_id');
            $table->integer('amount');
            $table->integer('wallet_id');
            $table->longText('additional_data')->nullable();
            $table->enum('status', ['pending', 'paid', 'decline'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_withdrawal');
    }
}
