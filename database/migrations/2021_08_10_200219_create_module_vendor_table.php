<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_vendor', function (Blueprint $table) {
            $table->id('vendor_id')->unique();
            $table->integer('user_id');
            $table->string('vendor_name')->nullable();
            $table->integer('service_fee');
            $table->enum('service_fee_type', ['percentase','nominal'])->default('percentase');
            $table->longText('additional_data')->nullable();
            $table->enum('status', ['Active','Non Active'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_vendor');
    }
}
