<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_document', function (Blueprint $table) {
            $table->increments('document_id');
            $table->integer('user_id')->nullable();
            $table->string('card_id')->nullable();
            $table->string('card_id_selfie')->nullable();
            $table->string('sim_card')->nullable();
            $table->string('another_document')->nullable();
            $table->enum('status',['pending','approved','decline'])->dafault('pending');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_document');
    }
}
