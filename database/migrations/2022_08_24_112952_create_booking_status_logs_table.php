<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingStatusLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_status_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booking_id');
            $table->enum('status',['pending','awaiting departure time','waiting driver','waiting pickup','pickedup','cancel','completed']);
            $table->string('description')->nullable();
            $table->timestamps();
            $table->text('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_status_logs');
    }
}
