<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleBookingImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_booking_images', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->string('booking_id');
            $table->enum('type', ['PICKED_UP', 'COMPLETED'])->default('PICKED_UP');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_booking_images');
    }
}
