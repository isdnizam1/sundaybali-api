import store from "../store";
import Cookies from 'js-cookie'
export default function auth({ next, router }) {
  if (!store.getters.isAuthenticated) {
      return router.push({ name: "Login" });
  }else{
    let access= Cookies.get('access');
    console.log(access);
    if(access!='admin'){
      window.location.href = '/dashboard';
    }
  }

  return next();
}
