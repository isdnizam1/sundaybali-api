import store from "../store";
import Cookies from 'js-cookie'
export default function auth({ next, router }) {
  if (!store.getters.isAuthenticated) {
      return router.push({ name: "Login" });
  }

  return next();
}
