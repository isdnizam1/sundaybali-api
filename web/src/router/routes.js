import DashboardLayout from "@/layout/DashboardLayout.vue";
import AuthLayout from "@/pages/Dashboard/Pages/AuthLayout.vue";

// Dashboard pages
import Dashboard from "@/pages/Dashboard/Dashboard.vue";
// Profile
import UserProfile from "@/pages/Dashboard/Examples/UserProfile.vue";

// User Management
import ListUserPage from "@/pages/Dashboard/Examples/UserManagement/ListUserPage.vue";

// Pages
import RtlSupport from "@/pages/Dashboard/Pages/RtlSupport.vue";
import Login from "@/pages/Login/index.vue";
import Register from "@/pages/Dashboard/Pages/Register.vue";

// Components pages
import Notifications from "@/pages/Dashboard/Components/Notifications.vue";
import Icons from "@/pages/Dashboard/Components/Icons.vue";
import Typography from "@/pages/Dashboard/Components/Typography.vue";

// TableList pages
import RegularTables from "@/pages/Dashboard/Tables/RegularTables.vue";

// Maps pages
import FullScreenMap from "@/pages/Dashboard/Maps/FullScreenMap.vue";

//import middleware
import auth from "@/middleware/auth";
import vendorAuth from "@/middleware/vendor-auth";
import guest from "@/middleware/guest";
import Booking from "@/pages/Booking/index.vue";
import BookingDetail from "@/pages/Booking/detail.vue";
import BookingDetailVendor from "@/pages/BookingVendor/detail.vue";

import UserDetail from "@/pages/User/detail.vue";
import User from "@/pages/User/index.vue";

import Withdrawal from "@/pages/Withdrawal/index.vue";
import vendorWithdrawal from "@/pages/Withdrawal/vendor_withdrawal.vue";
import vendorMutation from "@/pages/Withdrawal/vendor_mutation.vue";
import vendorRentCar from "@/pages/Booking/vendor_rent_car.vue";
import vendorDashboard from "@/pages/Dashboard/vendor_dashboard.vue";

import BookingVendor from "@/pages/BookingVendor/index.vue";

import updateProfile from "@/pages/User/updateProfile.vue";
import Driver from "@/pages/Driver/index.vue";
import addDriver from "@/pages/Driver/addDriver.vue";

import Attraction from "@/pages/Attraction/index.vue";
import Report from "@/pages/Report/index.vue";
import News from "@/pages/News/index.vue";
import addNews from "@/pages/News/addNews.vue";
import editNews from "@/pages/News/addNews.vue";
import Promotion from "@/pages/Promotion/index.vue";
import addPromotion from "@/pages/Promotion/addPromotion.vue";
import editPromotion from "@/pages/Promotion/addPromotion.vue";
import CarsType from "@/pages/Cars/carsType.vue";
import listCars from "@/pages/Cars/listCars.vue";
import addCar from "@/pages/Cars/addCar.vue";
import carCharter from "@/pages/Settings/carCharter.vue";
import carCharterRules from "@/pages/Settings/carCharterRules.vue";
import welcomeMessage from "@/pages/Settings/welcomeMessage.vue";
import deliverySelfDrive from "@/pages/Settings/deliverySelfDrive.vue";
import termConditionBooking from "@/pages/Settings/termConditionBooking.vue";
import selfDriveTermCondition from "@/pages/Settings/selfDriveTermCondition.vue";
import cityTransfer from "@/pages/Settings/cityTransfer.vue";
import coverageArea from "@/pages/Settings/coverageArea.vue";
import selfDrive from "@/pages/Settings/selfDrive.vue";

import Vendor from "@/pages/Vendor/index.vue";
import addVendor from "@/pages/Vendor/addVendor.vue";

let componentsMenu = {
  path: "/components",
  component: DashboardLayout,
  redirect: "/components/notification",
  name: "Components",
  children: [
    {
      path: "table",
      name: "Table",
      components: { default: RegularTables },
      meta: { middleware: auth }
    },
    {
      path: "typography",
      name: "Typography",
      components: { default: Typography },
      meta: { middleware: auth }
    },
    {
      path: "icons",
      name: "Icons",
      components: { default: Icons },
      meta: { middleware: auth }
    },
    {
      path: "maps",
      name: "Maps",
      meta: {
        hideContent: true,
        hideFooter: true,
        navbarAbsolute: true,
        middleware: vendorAuth
      },
      components: { default: FullScreenMap }
    },
    {
      path: "notifications",
      name: "Notifications",
      components: { default: Notifications },
      meta: { middleware: auth }
    },
   
    {
      path: "rtl",
      name: "وحة القيادة",
      meta: {
        rtlActive: true,
        middleware: auth
      },
      components: { default: RtlSupport }
    }
  ]
};

let examplesMenu = {
  path: "/examples",
  component: DashboardLayout,
  name: "Examples",
  children: [
    {
      path: "user-profile",
      name: "User Profile",
      components: { default: UserProfile },
      meta: { middleware: auth }
    },
    {
      path: "user-management/list-users",
      name: "List Users",
      components: { default: ListUserPage },
      meta: { middleware: auth }
    }
  ]
};

let authPages = {
  path: "/",
  component: AuthLayout,
  name: "Authentication",
  children: [
    {
      path: "/login",
      name: "Login",
      component: Login,
      meta: { middleware: guest }
    },
    {
      path: "/register",
      name: "Register",
      component: Register,
      meta: { middleware: guest }
    }
  ]
};

const routes = [
  {
    path: "/",
    redirect: "/dashboard",
    meta: { middleware: vendorAuth },
    name: "Home"
  },
  {
    path: "/",
    component: DashboardLayout,
    meta: { middleware: vendorAuth },

    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        components: { default: Dashboard },
        meta: { middleware: vendorAuth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/booking/:status",
        name: "Booking",
        components: { default: Booking },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/bookingDetail/:id",
        name: "BookingDetail",
        components: { default: BookingDetail },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "vendor/booking/:status",
        name: "Rent Car",
        components: { default: BookingVendor },
        meta: { middleware: vendorAuth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "vendor/bookingDetail/:id",
        name: "Booking Detail",
        components: { default: BookingDetailVendor },
        meta: { middleware: vendorAuth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/user/detail/:id",
        name: "Detail",
        components: { default: UserDetail },
        meta: { middleware: auth }
      }
    ]
  },
  
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/users",
        name: "User",
        components: { default: User },
        meta: { middleware: auth }
      }
    ]
  }, 
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/driver",
        name: "Guide",
        components: { default: Driver },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/vendor",
        name: "Vendor",
        components: { default: Vendor },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/attraction",
        name: "Attraction",
        components: { default: Attraction },
        meta: { middleware: auth }
      }
    ]
  },  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/report",
        name: "Report",
        components: { default: Report },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/news",
        name: "News",
        components: { default: News },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/news/add",
        name: "Add News",
        components: { default: addNews },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/news/edit/:id",
        name: "Edit News",
        components: { default: editNews },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/driver/add",
        name: "Add Guide",
        components: { default: addDriver },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/vendor/add",
        name: "Add Vendor",
        components: { default: addVendor },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/driver/edit/:id",
        name: "Edit Guide",
        components: { default: addDriver },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/vendor/edit/:id",
        name: "Edit Vendor",
        components: { default: addVendor },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/promotion",
        name: "Promotion",
        components: { default: Promotion },
        meta: { middleware: auth }
      }
    ]
  }, {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/promotion/add",
        name: "Add Promotion",
        components: { default: addPromotion },
        meta: { middleware: auth }
      }
    ]
  }, {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/promotion/edit/:id",
        name: "Edit Promotion",
        components: { default: editPromotion },
        meta: { middleware: auth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/settings/car_charter_price",
        name: "Car Charter Price",
        components: { default: carCharter },
        meta: { middleware: auth }
      }
    ]
  }, 
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/settings/car_charter_rules",
        name: "Car Charter Rules",
        components: { default: carCharterRules },
        meta: { middleware: auth }
      }
    ]
  }, 
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/settings/welcomeMessage",
        name: "Welcome Message",
        components: { default: welcomeMessage },
        meta: { middleware: auth }
      }
    ]
  }, 
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/settings/delivery_self_drive",
        name: "Delivery Self Drive",
        components: { default: deliverySelfDrive },
        meta: { middleware: auth }
      }
    ]
  }, 
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/settings/termConditionBooking",
        name: "Terms Condition Booking",
        components: { default: termConditionBooking },
        meta: { middleware: auth }
      }
    ]
  }, 
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/settings/selfDriveTermCondition",
        name: "Self Drive Terms Condition",
        components: { default: selfDriveTermCondition },
        meta: { middleware: auth }
      }
    ]
  }, 
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/settings/coverageArea",
        name: "Coverage Area",
        components: { default: coverageArea },
        meta: { middleware: auth }
      }
    ]
  }, 
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/settings/city_transfer_price",
        name: "City Transfer Price",
        components: { default: cityTransfer },
        meta: { middleware: auth }
      }
    ]
  }, {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/cars_type",
        name: "CarsType",
        components: { default: CarsType },
        meta: { middleware: auth }
      }
    ]
  }, {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/cars",
        name: "List Car",
        components: { default: listCars },
        meta: { middleware: vendorAuth }
      }
    ]
  },{
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/cars/add",
        name: "Add Car",
        components: { default: addCar },
        meta: { middleware: vendorAuth }
      },
      {
        path: "admin/car/edit/:id",
        name: "Edit Car",
        components: { default: addCar },
        meta: { middleware: vendorAuth }
      },
      {
        path: "admin/car/view/:id",
        name: "Car Detail",
        components: { default: addCar },
        meta: { middleware: vendorAuth }
      }
    ]
  },{
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/withdrawal",
        name: "List ",
        components: { default: Withdrawal },
        meta: { middleware: auth }
      },
      {
        path: "vendor/withdrawal",
        name: "List Withdrawal ",
        components: { default: vendorWithdrawal },
        meta: { middleware: vendorAuth }
      },
      {
        path: "vendor/mutation",
        name: "List Mutation",
        components: { default: vendorMutation },
        meta: { middleware: vendorAuth }
      },
      {
        path: "vendor/rent_car",
        name: "Rent Car",
        components: { default: vendorRentCar },
        meta: { middleware: vendorAuth }
      },
      {
        path: "vendor/dashboard",
        name: "Vendor Dashboard",
        components: { default: vendorDashboard },
        meta: { middleware: vendorAuth }
      }
    ]
  }, {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/profile",
        name: "Update Profile",
        components: { default: updateProfile },
        meta: { middleware: vendorAuth }
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "admin/settings/self_drive_price",
        name: "Self Drive Price",
        components: { default: selfDrive },
        meta: { middleware: auth }
      }
    ]
  }, 
  componentsMenu,
  examplesMenu,
  authPages
];

export default routes;
