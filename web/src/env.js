export const IS_DEV = process.env.NODE_ENV === "development";
export const IS_PROD = window.location.hostname === "backoffice.featour.com";

export const API_ENDPOINT = IS_PROD
  ? "https://api.featour.com/api"
  : IS_DEV
  ? "http://127.0.0.1:8000/api"
  : "https://api-dev.featour.com/api";
