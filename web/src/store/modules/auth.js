import Vue from "vue";
import router from "@/router";
import { VueAuthenticate } from "vue-authenticate";

import axios from "axios";
import VueAxios from "vue-axios";
import { API_ENDPOINT } from "../../env";
Vue.use(VueAxios, axios);

const vueAuth = new VueAuthenticate(Vue.prototype.$http, {
  baseUrl: API_ENDPOINT,
  tokenName: "access_token",
  loginUrl: "/v1/auth/login",
  registerUrl: "/register"
});

export default {
  state: {
    isAuthenticated:
      localStorage.getItem("vue-authenticate.vueauth_access_token") !== null
  },

  getters: {
    isAuthenticated(state) {
      return state.isAuthenticated;
    }
  },

  mutations: {
    isAuthenticated(state, payload) {
      state.isAuthenticated = payload.isAuthenticated;
    }
  },

  actions: {
    login(context, payload) {
      console.log({ payload });
      return vueAuth.login(payload).then(response => {
        if (response.data.code == 200) {
          //   // context.commit("isAuthenticated", {
          //   //   isAuthenticated: vueAuth.isAuthenticated()
          //   // });
          console.log(response.data.result.token);
          localStorage.setItem(
            "userInfo",
            JSON.stringify(response.data.result)
          );
          localStorage.setItem(
            "vue-authenticate.vueauth_access_token",
            JSON.stringify(response.data.result)
          );
        }
        // router.push({name: "Home"});
        return response;
      });
    },

    register(context, payload) {
      return vueAuth
        .register(payload.user, payload.requestOptions)
        .then(response => {
          context.commit("isAuthenticated", {
            isAuthenticated: vueAuth.isAuthenticated()
          });
          router.push({ name: "Home" });
        });
    },

    logout(context, payload) {
      return vueAuth.logout().then(response => {
        context.commit("isAuthenticated", {
          isAuthenticated: vueAuth.isAuthenticated()
        });
        router.push({ name: "Login" });
      });
    }
  }
};
