import request from "@/utils/request";

export function getVehicle(params) {
  return request({
    url: "/v1/vehicle",
    method: "get",
    params
  });
}
export function getVehicleType(params) {
  return request({
    url: "/v1/vehicle/type",
    method: "get",
    params
  });
}

export function addVehicle(data) {
  return request({
    url: `/v1/vehicle/add_vehicle`,
    method: "post",
    data
  });
}
