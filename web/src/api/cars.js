import { API_ENDPOINT } from "@/env";
import request from "@/utils/request";


export function getCar(params) {
  return request({
    url: "/v1/cars",
    method: "get",
    params
  });
}

export function addCar(data) {
  return request({
    url: `/v1/cars/add_car`,
    method: "post",
    data
  });
}
