import { API_ENDPOINT } from "@/env";
import request from "@/utils/request";


export function getUser(params) {
  return request({
    url: "/v1/user",
    method: "get",
    params
  });
}

export function getDriver(params) {
  return request({
    url: "/v1/user/driver",
    method: "get",
    params
  });
}

export function getUserLoggediIn(params) {
  return request({
    url: "/v1/user/detail",
    method: "get",
    params
  });
}


export function addDriver(data) {
  return request({
    url: `/v1/user/add_driver`,
    method: "post",
    data
  });
}
export function changePassword(data) {
  return request({
    url: `/v1/auth/change_password`,
    method: "post",
    data
  });
}
export function login(data) {
  return request({
    url: `/v1/auth/login`,
    method: "post",
    data
  });
}
export function getInfo(token) {
  return request({
    url: `/v1/user/detail`,
    method: "get",
    params: {}
  });
}
export function setStatusUser(data) {
  return request({
    url: "/v1/user/set_status_user",
    method: "post",
    data: data
  });
}
export function logout() {
  return request({
    url: "/v1/auth/logout",
    method: "post"
  });
}

export function updateProfile(data) {
  return request({
    url: "/v1/user/update_profile",
    method: "post",
    data: data
  });
}






export function removeUser(data) {
  return request({
    url: "/v1/user/remove",
    method: "post",
    data: data
  });
}

export function createUser(data) {
  return request({
    url: "/v1/user/add",
    method: "post",
    data: data
  });
}

export function updateUserRole({ user_id, role_id }) {
  return request({
    url: "/v1/role/update_role_user",
    method: "post",
    data: { user_id, role_id }
  });
}

export function getUsers(params) {
  return request({
    url: "/v1/role/user",
    method: "get",
    params
  });
}


export function deleteUser(data) {
  return request({
    url: `/v1/user/delete_account`,
    method: "post",
    data
  });
}


export function setStatusDocument(data) {
  return request({
    url: `/v1/document/set_status_document`,
    method: "post",
    data
  });
}