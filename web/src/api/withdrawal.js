import { API_ENDPOINT } from "@/env";
import request from "@/utils/request";


export function getWithdraw(params) {
  return request({
    url: "/v1/withdrawal",
    method: "get",
    params
  });
}

export function getMutation(params) {
  return request({
    url: "/v1/wallet/mutations",
    method: "get",
    params
  });
}
export function setStatusWithdraw(data) {
  return request({
    url: "/v1/withdrawal/set_status_withdraw",
    method: "post",
    data: data
  });
}