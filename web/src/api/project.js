import request from "@/utils/request";

export function getProjects(params) {
  return request({
    url: "/v1/project",
    method: "get",
    params
  });
}

export function saveProject(data) {
  return request({
    url: `/v1/project/${data.project_id ? "edit" : "add"}`,
    method: "post",
    data
  });
}
