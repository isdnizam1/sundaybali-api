import request from "@/utils/request";

export function getPromotion(params) {
  return request({
    url: "/v1/promotion",
    method: "get",
    params
  });
}



export function addPromotion(data) {
  return request({
    url: `/v1/promotion/add`,
    method: "post",
    data
  });
}

export function editPromotion(data) {
  return request({
    url: `/v1/promotion/edit`,
    method: "post",
    data
  });
}