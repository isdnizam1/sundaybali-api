import request from "@/utils/request";

export function getNotif(params) {
  return request({
    url: "/v1/notification",
    method: "get",
    params
  });
}


export function setStatus(data) {
  return request({
    url: `/v1/notification/set_status_notif`,
    method: "post",
    data
  });
}
