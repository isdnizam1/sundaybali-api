import request from "@/utils/request";

export function getSetting(params) {
  return request({
    url: "/v1/settings",
    method: "get",
    params
  });
}
export function editSetting(data) {
  return request({
    url: `/v1/settings/edit_setting`,
    method: "post",
    data
  });
}
export function editSettingPrice(data) {
  return request({
    url: `/v1/settings/edit_car_charter_price`,
    method: "post",
    data
  });
}
export function editCarCharterRules(data) {
  return request({
    url: `/v1/settings/edit_car_charter_rules`,
    method: "post",
    data
  });
}
export function editSelfDrivePrice(data) {
  return request({
    url: `/v1/settings/edit_self_drive_price`,
    method: "post",
    data
  });
}