import { API_ENDPOINT } from "@/env";
import request from "@/utils/request";


export function getCountry(params) {
  return request({
    url: "/v1/country",
    method: "get",
    params
  });
}