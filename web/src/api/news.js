import request from "@/utils/request";

export function getNews(params) {
  return request({
    url: "/v1/news",
    method: "get",
    params
  });
}




export function addNews(data) {
  return request({
    url: `/v1/news/add`,
    method: "post",
    data
  });
}
