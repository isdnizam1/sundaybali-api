import { API_ENDPOINT } from "@/env";
import request from "@/utils/request";


export function getVendor(params) {
  return request({
    url: "/v1/vendor",
    method: "get",
    params
  });
}

export function addVendor(data) {
  return request({
    url: `/v1/vendor/add_vendor`,
    method: "post",
    data
  });
}
