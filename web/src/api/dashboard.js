import request from "@/utils/request";

export function getDashboard(params) {
  return request({
    url: "/v1/dashboard",
    method: "get",
    params
  });
}
