import request from "@/utils/request";

export function getClients(params) {
  return request({
    url: "/v1/client",
    method: "get",
    params
  });
}
