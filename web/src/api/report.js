import request from "@/utils/request";

export function getReport(params) {
  return request({
    url: "/v1/report",
    method: "get",
    params
  });
}



