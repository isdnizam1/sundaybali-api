import request from "@/utils/request";

export function getBooking(params) {
  return request({
    url: "/v1/booking",
    method: "get",
    params
  });
}

export function getBookingSelfDrive(params) {
  return request({
    url: "/v1/booking/self_drive",
    method: "get",
    params
  });
}



export function getDetailBooking(params) {
  return request({
    url: "/v1/booking/detail",
    method: "get",
    params
  });
}

export function getReport(params) {
  return request({
    url: "/v1/booking/report",
    method: "get",
    params
  });
}

export function selectDriver(data) {
  return request({
    url: `/v1/booking/selectDriver`,
    method: "post",
    data
  });
}



export function declinedBooking(data) {
  return request({
    url: `/v1/booking/declineBooking`,
    method: "post",
    data
  });
}



export function confirmBooking(data) {
  return request({
    url: `/v1/booking/confirmBooking`,
    method: "post",
    data
  });
}


export function refundBooking(data) {
  return request({
    url: `/v1/booking/refundBooking`,
    method: "post",
    data
  });
}



export function confirmFeeBooking(data) {
  return request({
    url: `/v1/booking/sendFeeTranscation`,
    method: "post",
    data
  });
}

export function paidBooking(data) {
  return request({
    url: `/v1/booking/paidBooking`,
    method: "post",
    data
  });
}
