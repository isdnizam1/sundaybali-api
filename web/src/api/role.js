import request from "@/utils/request";

export function getRole(params) {
  return request({
    url: "/v1/role",
    method: "get",
    params
  });
}

export function saveRole(data) {
  return request({
    url: `/v1/role/${data.role_id ? "edit" : "add"}`,
    method: "post",
    data
  });
}
