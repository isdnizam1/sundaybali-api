import { API_ENDPOINT } from "@/env";
import request from "@/utils/request";


export function getLastLocation(params) {
  return request({
    url: "/v1/driver/last_location",
    method: "get",
    params
  });
}
