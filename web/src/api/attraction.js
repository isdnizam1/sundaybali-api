import request from "@/utils/request";

export function getAttraction(params) {
  return request({
    url: "/v1/attraction",
    method: "get",
    params
  });
}
