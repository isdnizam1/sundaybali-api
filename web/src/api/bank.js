import request from "@/utils/request";

export function getBank(params) {
  return request({
    url: "/v1/bank",
    method: "get",
    params
  });
}
